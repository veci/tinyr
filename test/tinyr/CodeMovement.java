package tinyr;

import org.junit.*;
import tinyr.bytecode.instructions.*;
import tinyr.bytecode.*;

/**
 */
public class CodeMovement extends VersioningBase {
/*
    @Test
    public void singleStemUpMovementInsideBlock() {
        Instruction[] original = new Instruction[] {
                new LOAD_CONST(0, 1),
                new LOAD_CONST(1, 1),
                new RET(1)
        };
        Code c = new Code(original);
        Code.View cv = c.toExecutable();
        Code.Patch p = cv.openForEditing();
        cv.moveUp(1);
        assertPatchAndRollback(cv, p, original,
                new LOAD_CONST(1, 1),
                new LOAD_CONST(0, 1),
                new RET(1)
        );
    }

    @Test
    public void moveUpToPreviousSingleBlock() {
        Instruction[] original = new Instruction[] {
                new LOAD_CONST(0, 1),
                new BR(1),
                new LABEL(1),
                new LOAD_CONST(1, 1),
                new RET(1)
        };
        Code cc = new Code(original);
        Code.View c = cc.toExecutable();
        Code.Patch p = c.openForEditing();
        c.moveUp(3);
        assertPatchAndRollback(c, p, original,
                new LOAD_CONST(0, 1),
                new LOAD_CONST(1, 1),
                new BR(1),
                new LABEL(1),
                new RET(1)
        );
    }

    @Test
    public void moveUpToMorePreviousBlocks() {
        Instruction[] original = new Instruction[] {
                new LOAD_CONST(0, 1),
                new BR(2),
                new LABEL(1),
                new LOAD_CONST(1, 1),
                new BR(2),
                new LABEL(2),
                new LOAD_CONST(3, 3),
                new RET(1)
        };
        Code cc = new Code(original);
        Code.View c = cc.toExecutable();
        Code.Patch p = c.openForEditing();
        c.moveUp(6);
        assertPatchAndRollback(c, p, original,
                new LOAD_CONST(0, 1),
                new LOAD_CONST(3, 3), // duplicated
                new BR(2),
                new LABEL(1),
                new LOAD_CONST(1, 1),
                new LOAD_CONST(3, 3), // duplicated
                new BR(2),
                new LABEL(2),
                new RET(1)
        );
    }

    @Test
    public void moveUpToPreviousBlockWithMoreChildren() {
        Instruction[] original = new Instruction[] {
                new LOAD_CONST(0, 1),
                new CBR(0, 1, 2),
                new LABEL(1),
                new LOAD_CONST(3, 3),
                new LOAD_CONST(1, 1),
                new BR(3),
                new LABEL(2),
                new LOAD_CONST(2, 2),
                new LOAD_CONST(3, 3),
                new BR(3),
                new LABEL(3),
                new RET(1)
        };
        Code cc = new Code(original);
        Code.View c = cc.toExecutable();
        Code.Patch p = c.openForEditing();
        c.moveUp(3);
        assertPatchAndRollback(c, p, original,
                new LOAD_CONST(0, 1),
                new LOAD_CONST(3, 3),
                new CBR(0, 1, 2),
                new LABEL(1),
                new LOAD_CONST(1, 1),
                new BR(3),
                new LABEL(2),
                new LOAD_CONST(2, 2),
                new BR(3),
                new LABEL(3),
                new RET(1)
        );
    }

    @Test
    public void moveUpToPreviousBlockWithMoreChildren2() {
        Instruction[] original = new Instruction[] {
                new LOAD_CONST(0, 1),
                new CBR(0, 1, 2),
                new LABEL(1),
                new LOAD_CONST(3, 3),
                new LOAD_CONST(1, 1),
                new BR(3),
                new LABEL(2),
                new LOAD_CONST(2, 2),
                new LOAD_CONST(1, 1),
                new BR(3),
                new LABEL(3),
                new RET(1)
        };
        Code cc = new Code(original);
        Code.View c = cc.toExecutable();
        Code.Patch p = c.openForEditing();
        c.moveUp(4, 1);
        assertPatchAndRollback(c, p, original,
                new LOAD_CONST(0, 1),
                new LOAD_CONST(1, 1),
                new CBR(0, 1, 2),
                new LABEL(1),
                new LOAD_CONST(3, 3),
                new BR(3),
                new LABEL(2),
                new LOAD_CONST(2, 2),
                new BR(3),
                new LABEL(3),
                new RET(1)
        );
    } */

}
