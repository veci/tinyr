package tinyr;

import org.junit.*;
import tinyr.bytecode.*;
import tinyr.bytecode.instructions.*;
import tinyr.runtime.Label;

import java.util.HashSet;

/**
 */
public class Versioning extends VersioningBase {


    @Test
    public void simpleRemovalPatchFirst() {
        Instruction[] original = new Instruction[] {
            new ADD(0, 0, 0),
            new ADD(1, 1, 1),
            new ADD(2, 2, 2),
            new ADD(3, 3, 3),
            new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.remove(0, p);
        assertPatchAndRollback(c, p, original,
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new ADD(3, 3, 3),
                new RET(0)
        );
    }

    @Test
    public void simpleRemovalPatchLast() {
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new ADD(3, 3, 3),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.remove(3, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new RET(0)
        );
    }

    @Test
    public void simpleRemovalPatchMiddle() {
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new ADD(3, 3, 3),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.remove(1, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new ADD(2, 2, 2),
                new ADD(3, 3, 3),
                new RET(0)
        );
    }

    @Test
    public void simpleRemovalPatchMiddleConcat() {
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new ADD(3, 3, 3),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.remove(1, p);
        c.remove(1, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new ADD(3, 3, 3),
                new RET(0)
        );
    }

    @Test
    public void simpleRemovalPatchMiddleConcat2() {
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new ADD(3, 3, 3),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.remove(2, p);
        c.remove(1, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new ADD(3, 3, 3),
                new RET(0)
        );
    }


    @Test
    public void simpleRemovalPatch2() {
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new ADD(3, 3, 3),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.remove(1, p);
        c.remove(2, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new ADD(2, 2, 2),
                new RET(0)
        );
    }

    @Test
    public void simpleInsertionPatchFirst() {
        Instruction[] original = new Instruction[] {
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new ADD(3, 3, 3),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.insert(new ADD(0, 0, 0), 0, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new ADD(3, 3, 3),
                new RET(0)
        );
    }

    @Test
    public void simpleInsertionPatchLast() {
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.insert(new ADD(3, 3, 3), 3, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new ADD(3, 3, 3),
                new RET(0)
        );
    }

    @Test
    public void simpleInsertionPatchMiddle() {
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new ADD(3, 3, 3),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.insert(new ADD(2, 2, 2), 2, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new ADD(3, 3, 3),
                new RET(0)
        );
    }

    @Test
    public void simpleInsertionPatchMiddleConcat() {
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new ADD(3, 3, 3),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.insert(new ADD(1, 1, 1), 1, p);
        c.insert(new ADD(2, 2, 2), 2, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new ADD(3, 3, 3),
                new RET(0)
        );
    }

    @Test
    public void simpleInsertionPatchMiddleConcat2() {
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new ADD(3, 3, 3),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.insert(new ADD(2, 2, 2), 1, p);
        c.insert(new ADD(1, 1, 1), 1, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new ADD(3, 3, 3),
                new RET(0)
        );
    }

    @Test
    public void simpleInsertionPatchMiddle2() {
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new ADD(2, 2, 2),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.insert(new ADD(1, 1, 1), 1, p);
        c.insert(new ADD(3, 3, 3), 3, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new ADD(3, 3, 3),
                new RET(0)
        );
    }

    // Not allowed in current code, throws an assertion when patch tries to delete instruction it has inserted
/*    @Test
    public void removalOfInsertion() {
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new ADD(2, 2, 2),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.insert(new ADD(1, 1, 1), 1, p);
        c.remove(1, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new ADD(2, 2, 2),
                new RET(0)
        );
    } */

    @Test
    public void removalAndInsertion() {
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new ADD(2, 2, 2),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.remove(0, p);
        c.insert(new ADD(1, 1, 1), 1, p);
        assertPatchAndRollback(c, p, original,
                new ADD(2, 2, 2),
                new ADD(1, 1, 1),
                new RET(0)
        );
    }

    @Test
    public void removalAndInsertionConcat() {
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new ADD(2, 2, 2),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.remove(0, p);
        c.insert(new ADD(1, 1, 1), 0 , p);
        assertPatchAndRollback(c, p, original,
                new ADD(1, 1, 1),
                new ADD(2, 2, 2),
                new RET(0)
        );
    }

    @Test
    public void basicBlocksSurviveEditableWithNoChanges() {
        Label l = new Label();
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new BR(l),
                new LABEL(l),
                new ADD(1,1,1),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new BR(l),
                new LABEL(l),
                new ADD(1,1,1),
                new RET(0)
        );
    }

    @Test
    public void basicBlocksSurviveRemoval() {
        Label l = new Label();
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new BR(l),
                new LABEL(l),
                new ADD(1,1,1),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.remove(0, p);
        assertPatchAndRollback(c, p, original,
                new BR(l),
                new LABEL(l),
                new ADD(1,1,1),
                new RET(0)
        );
    }

    @Test
    public void basicBlocksSurviveInsertion() {
        Label l = new Label();
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new BR(l),
                new LABEL(l),
                new ADD(1,1,1),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.insert(new ADD(1, 1, 1), 1, p);
        c.insert(new ADD(3, 3, 3), 4, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new ADD(1, 1, 1),
                new BR(l),
                new LABEL(l),
                new ADD(3, 3, 3),
                new ADD(1, 1, 1),
                new RET(0)
        );
    }

    @Test
    public void basicBlockCanBeDeleted() {
        Label l = new Label();
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new BR(l),
                new LABEL(l),
                new ADD(1,1,1),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.remove(1, p);
        c.insert(new RET(23), 1, p);
        c.remove(2, p);
        c.remove(2, p);
        c.remove(2, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new RET(23)
        );
    }

    @Test
    public void basicBlockCanBeInserted() {
        Label l = new Label();
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new RET(0)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.insert(new LABEL(l), 2, p);
        c.insert(new RET(1), 3, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new RET(0),
                new LABEL(l),
                new RET(1)
        );
    }

    @Test
    public void basicBlockCanBeInsertedInBetween() {
        Label l = new Label();
        Label l2 = new Label();
        Instruction[] original = new Instruction[] {
                new ADD(0, 0, 0),
                new RET(0),
                new LABEL(l),
                new RET(1)
        };
        Code cc = new Code(original);
        Code.View c = cc.getView();
        Code.Patch p = c.openNewPatch();
        c.insert(new LABEL(l2), 2, p );
        c.insert(new RET(2), 3, p);
        assertPatchAndRollback(c, p, original,
                new ADD(0, 0, 0),
                new RET(0),
                new LABEL(l),
                new RET(2),
                new LABEL(l2),
                new RET(1)
        );
    }

    
}
