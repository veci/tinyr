package tinyr;

import org.junit.*;
import tinyr.bytecode.*;
import tinyr.bytecode.instructions.*;

import java.util.HashSet;


/**
 */
@SuppressWarnings("deprecation")
public class CodeAndVersioning {

    static Instruction[] code(int... idxs) {
        Instruction[] result = new Instruction[idxs.length + 1];
        for ( int i = 0; i < idxs.length; ++i )
            result[i] = new IS_CLOSURE(idxs[i], idxs[i]);
        result[result.length - 1] = new RET(0);
        return result;
    }

    static void AssertEquals(Instruction[] expected, Instruction[] got) {
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        for ( Instruction i : expected )
            sb1.append(i.toString());
        for ( Instruction i : got )
            sb2.append(i.toString());
        Assert.assertEquals(sb1.toString(), sb2.toString());
    }

    @Test
    public void simpleLinearization() {
        Instruction[] original = code(1,2,3,4);
        Code ch = new Code(original);
        Instruction[] l = ch.getView().insnsAsArray();
        AssertEquals(original, l);
    }

    @Test
    public void simpleInsertion() {
        Instruction[] original = code(1,2,3,4);
        Code cc = new Code(original);
        Code.View ch = cc.getView();
        Code.Patch p = ch.openNewPatch();
        ch.insert(new IS_CLOSURE(100, 100), 0, p);
        ch.close(p);
        Instruction[] l = ch.codeObject().getView().insnsAsArray();
        AssertEquals(original, l);
        Instruction[] l2 = code(100, 1, 2, 3, 4);
        HashSet<Code.Patch> hs = new HashSet<>();
        hs.add(p);
        AssertEquals(l2, ch.codeObject().getView(hs).insnsAsArray());
    }

    @Test
    public void simpleRemoval() {
        Instruction[] original = code(1,2,3,4);
        Code cc = new Code(original);
        Code.View ch = cc.getView();
        Code.Patch p = ch.openNewPatch();
        ch.remove(2, p);
        ch.close(p);
        Instruction[] l = ch.codeObject().getView().insnsAsArray();
        AssertEquals(original, l);
        Instruction[] l2 = code(1, 2, 4);
        HashSet<Code.Patch> hs = new HashSet<>();
        hs.add(p);
        AssertEquals(l2, ch.codeObject().getView(hs).insnsAsArray());
    }

    @Test
    public void simpleInsertionAndRemoval() {
        Instruction[] original = code(1,2,3,4);
        Code cc = new Code(original);
        Code.View ch = cc.getView();
        Code.Patch p = ch.openNewPatch();
        ch.insert(new IS_CLOSURE(100, 100), 0, p);
        ch.remove(1, p);
        ch.insert(new IS_CLOSURE(200, 200), 1, p);
        ch.close(p);
        Instruction[] l = ch.codeObject().getView().insnsAsArray();
        AssertEquals(original, l);
        Instruction[] l2 = code(100, 200, 2, 3, 4);
        HashSet<Code.Patch> hs = new HashSet<>();
        hs.add(p);
        AssertEquals(l2, ch.codeObject().getView(hs).insnsAsArray());
    }

    @Test
    public void multiplePatches() {
        Instruction[] original = code(1,2,3,4);
        Code cc = new Code(original);
        Code.View ch = cc.getView();
        Code.Patch p = ch.openNewPatch();
        ch.insert(new IS_CLOSURE(100, 100), 0, p);
        ch.insert(new IS_CLOSURE(500, 500), 5, p);
        ch.close(p);
        Code.Patch p2 = ch.openNewPatch();
        ch.remove(1, p2);
        ch.insert(new IS_CLOSURE(200, 200), 1, p2);
        ch.close(p2);
        Code.Patch p3 = ch.openNewPatch();
        ch.remove(0, p3);
        ch.remove(2, p3);
        ch.insert(new IS_CLOSURE(0,0), 0, p3);
        Instruction[] l2 = code(0, 200, 2, 4, 500);
        Instruction[] l = ch.codeObject().getView().insnsAsArray();
        AssertEquals(original, l);
        HashSet<Code.Patch> hs = new HashSet<>();
        hs.add(p);
        AssertEquals(code(100, 1, 2, 3, 4, 500), ch.codeObject().getView(hs).insnsAsArray());
        hs.remove(p);
        hs.add(p2);
        AssertEquals(code(200, 2, 3, 4), ch.codeObject().getView(hs).insnsAsArray());
    }

}
