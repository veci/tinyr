package tinyr;

import java.util.*;

import org.junit.*;
import tinyr.ast.Node;
import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class ExecTest {

    public Value execute(String code) {
        Parser p = new Parser();
        Lowering l = new Lowering();
        Node ast = p.parse(code);
        try {
            Code b = l.lower(ast);
            Code.View be = b.getView();
            Value v = be.execute(new Frame(be));
            return v;
        } catch (Exception e) {
            Assert.assertTrue("No errors expected, but " + e.getMessage(), false);
        }
        return null;
    }

    public void execute(String code, int expected) {
        Value v = execute(code);
        if (v instanceof Primitive) {
            Assert.assertTrue("Expected result " + expected + " but " + v + " found", ((Primitive) v).value == expected);
        } else {
            Assert.assertTrue("Expected result " + expected + " but " + v + " found", false);
        }
    }

}
