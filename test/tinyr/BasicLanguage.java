package tinyr;

import org.junit.Test;

/**
 */
public class BasicLanguage extends ExecTest {

    @Test
    public void constants() {
        execute("1", 1);
        execute("2", 2);
    }

    @Test
    public void variables() {
        execute("a = 1", 1);
        execute("a = 2 a", 2);
    }

    @Test
    public void unaryOperator() {
        execute("-1", -1);
    }

    @Test
    public void arithmeticOperators() {
        execute("1 + 2", 3);
        execute("a = 1 a + a", 2);
        execute("1 - 2", -1);
        execute("a = 1 b = 2 a > b", 0);
        execute("a = 2 b = 2 a >= b", 1);
        execute("a = 1 b = 0 a <= b", 0);
        execute("a = 1 b = 0 a == b+1", 1);
    }

    @Test
    public void relationalOperators() {
        execute("1 == 1", 1);
        execute("1 != 1", 0);
        execute("1 == function() { 1 } ", 0);
        execute("2 != function() { 1 } ", 1);
    }

    @Test
    public void functions() {
        execute("a = function() { 1 } a()", 1);
        execute("a = function() { b } b = 2 a()", 2);
        execute("a = function() { b + c} b = 1 c = 3 a()", 4);        
    }

    @Test
    public void ifStatement() {
        execute("if (1) 2 else 3", 2);
        execute("if (0) 2 else 3", 3);
        execute("if (1) { 2 } else { 3 }", 2);
        execute("a = 1 if (a) 2 else b", 2);
        execute("a = 0 if (a) a else a + 1", 1);
        execute("a = 1 b = 3 if (a) { if (b) 1 else 2 } else { 3 }", 1);
        execute("a = 1 b = 0 if (a) { if (b) 1 else 2 } else { 3 }", 2);
        execute("a = 0 b = 3 if (a) { if (b) 1 else 2 } else { 3 }", 3);
    }

    @Test
    public void variableRedefinition() {
        execute("a = 1 a = 2 a", 2);
        execute("a = function() { 1 } a = 2 a", 2);
    }

    @Test
    public void functionDoesNotCapture() {
        execute("a = 1 b = function() { a } a = 2 b()", 2);
    }
    
    @Test
    public void WhileLoop() {
        execute("a = 1 i = 1 while (i < 5) {a = a + 1 i = i + 1} a", 5);
        execute("a = 1 b = 5 while (b > a) {a = a + 2} a", 5);
        execute("a = 1 b = 5 while (b > a) {a = a + 2} a", 5);
        execute("b = 1 a = 4 while (1) { while (b < 100) { b = b + 1 if (b < 10) continue else break } a = a + a break } a", 8);        
        execute("b = 1 a = 4 while (1) { while (b < 100) { b = b + 1 if (b < 10) continue else break } a = a + a break } b", 10);           
    }
    
    @Test public void FunctionWhile() {
        execute("a = function() { b = 2 i = 2 while (i < 10) { b = b + 1 i = i + 1} RET b} a()", 10);
        execute("a = function() { b = 2 i = 10 while (i >= b) { b = b + 2} RET b} a()",12);
        execute("x = function() { b = 1 a = 4 while (1) { while (b < 100) { b = b + 1 if (b < 10) continue else break } a = a + a break } RET a } x()", 8);
    }
}
