package tinyr;

import java.util.Vector;

import org.junit.*;

import tinyr.Parser.*;
import tinyr.ast.*;

/**
 */
public class ParserTests {

    @Test
    public void ADD() {
        Parser p = new Parser();
        Node n = p.parse("register a register b ADD a b a");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new RegisterDeclaration("b"))
                .addNode(new ADD(new Register("a"), new Register("b"), new Register("a")))));
    }

    @Test
    public void ADD_Exprs() {
        Parser p = new Parser();
        Node n = p.parse("register a ADD a 2 3");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new ADD(new Register("a"), new Literal(2), new Literal(3)))));
    }

    @Test
    public void Assignment() {
        Parser p = new Parser();
        Node n = p.parse("a = 5");
        Assert.assertTrue(n.equals(new Assignment("a", new Literal(5))));
    }

    @Test
    public void BinaryOperatorAdd() {
        Parser p = new Parser();
        Node n = p.parse("2 + 3");
        Assert.assertTrue(n.equals(new BinaryOperator(new Literal(2), new Literal(3), Parser.Token.Type.opAdd)));
    }

    @Test
    public void BinaryOperatorEq() {
        Parser p = new Parser();
        Node n = p.parse("2 == 3");
        Assert.assertTrue(n.equals(new BinaryOperator(new Literal(2), new Literal(3), Parser.Token.Type.opEq)));
    }

    @Test
    public void BinaryOperatorNeq() {
        Parser p = new Parser();
        Node n = p.parse("2 != 3");
        Assert.assertTrue(n.equals(new BinaryOperator(new Literal(2), new Literal(3), Parser.Token.Type.opNeq)));
    }

    @Test
    public void BinaryOperatorSub() {
        Parser p = new Parser();
        Node n = p.parse("2 - 3");
        Assert.assertTrue(n.equals(new BinaryOperator(new Literal(2), new Literal(3), Parser.Token.Type.opSub)));
    }
    
    @Test
    public void BinaryOperatorGT() {
        Parser p = new Parser();
        Node n = p.parse("3 > 2");
        Assert.assertTrue(n.equals(new BinaryOperator(new Literal(3), new Literal(2), Parser.Token.Type.opGreater)));
    }
    
    @Test
    public void BinaryOperatorGTFalse() {
        Parser p = new Parser();
        Node n = p.parse("3 < 2");
        Assert.assertFalse(n.equals(new BinaryOperator(new Literal(3), new Literal(2), Parser.Token.Type.opGreater)));
    }
    
    @Test
    public void BinaryOperatorLT() {
        Parser p = new Parser();
        Node n = p.parse("3 < 2");
        Assert.assertFalse(n.equals(new BinaryOperator(new Literal(2), new Literal(3), Parser.Token.Type.opLesser)));
    }
    
    @Test
    public void BinaryOperatorGTE() {
        Parser p = new Parser();
        Node n = p.parse("3 >= 2");
        Assert.assertTrue(n.equals(new BinaryOperator(new Literal(3), new Literal(2), Parser.Token.Type.opGreaterEquals)));
    }
    
    @Test
    public void BinaryOperatorLTE() {
        Parser p = new Parser();
        Node n = p.parse("2 <= 3");
        Assert.assertTrue(n.equals(new BinaryOperator(new Literal(2), new Literal(3), Parser.Token.Type.opLesserEquals)));
    }

    @Test
    public void BinaryOperatorRecursiveness() {
        Parser p = new Parser();
        Node n = p.parse("1 - 2 - 3");
        Assert.assertTrue(n.equals(new BinaryOperator(new BinaryOperator(new Literal(1), new Literal(2), Parser.Token.Type.opSub), new Literal(3), Parser.Token.Type.opSub)));
    }

    @Test
    public void BinaryOperatorPriority1() {
        Parser p = new Parser();
        Node n = p.parse("1 - 2 == 3");
        Assert.assertTrue(n.equals(new BinaryOperator(new BinaryOperator(new Literal(1), new Literal(2), Parser.Token.Type.opSub), new Literal(3), Parser.Token.Type.opEq)));
    }

    @Test
    public void BinaryOperatorPriority2() {
        Parser p = new Parser();
        Node n = p.parse("1 == 2 - 3");
        Assert.assertTrue(n.equals(new BinaryOperator(new Literal(1), new BinaryOperator(new Literal(2), new Literal(3), Parser.Token.Type.opSub), Parser.Token.Type.opEq)));
    }

    @Test
    public void BinaryOperatorParentheses() {
        Parser p = new Parser();
        Node n = p.parse("1 - (2 == 3)");
        Assert.assertTrue(n.equals(new BinaryOperator(new Literal(1), new BinaryOperator(new Literal(2), new Literal(3), Parser.Token.Type.opEq), Parser.Token.Type.opSub)));
    }

    @Test
    public void Block() {
        Parser p = new Parser();
        Node n = p.parse("1 2 3");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new Literal(1))
                .addNode(new Literal(2))
                .addNode(new Literal(3))));
    }

    @Test
    public void CALL() {
        Parser p = new Parser();
        Node n = p.parse("register a register b CALL a b []");        
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new RegisterDeclaration("b"))
                .addNode(new CALL(new Register("a"), new Register("b"), new Vector<Node>()))));
    }

    @Test
    public void CALL_Exprs() {
        Vector<Node> args = new Vector<Node>();
        args.add(new Literal(1));
        args.add(new Literal(2));
        Parser p = new Parser();
        Node n = p.parse("register a CALL a b [1, 2]");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new CALL(new Register("a"), new Variable("b"), args))));
    }

    @Test
    public void EQ() {
        Parser p = new Parser();
        Node n = p.parse("register a register b EQ a b a");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new RegisterDeclaration("b"))
                .addNode(new EQ(new Register("a"), new Register("b"), new Register("a")))));
    }

    @Test
    public void EQ_Exprs() {
        Parser p = new Parser();
        Node n = p.parse("register a EQ a 2 3");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new EQ(new Register("a"), new Literal(2), new Literal(3)))));
    }

    @Test
    public void FunctionCall() {
        Parser p = new Parser();
        Node n = p.parse("a()");
        Assert.assertTrue(n.equals(new FunctionCall(new Variable("a"), null )));
    }

    @Test
    public void FunctionCallArgs() {
        Parser p = new Parser();
        Node n = p.parse("a(1,2,3)");
        System.out.println(n.toString());
        Vector<Node> args = new Vector<>();
        args.add(new Literal(1));
        args.add(new Literal(2));
        args.add(new Literal(3));
        Assert.assertTrue(n.equals(new FunctionCall(new Variable("a"), args)));
    }

    @Test
    public void FunctionDefinition() {
        Parser p = new Parser();
        Node n = p.parse("function() a");
        Assert.assertTrue(n.equals(new FunctionDefinition(new Variable("a"))));
    }

    @Test
    public void FunctionDefinitionBlockIgnored() {
        Parser p = new Parser();
        Node n = p.parse("function() { a }");
        Assert.assertTrue(n.equals(new FunctionDefinition(new Variable("a"))));
    }

    @Test
    public void FunctionDefinitionBlock() {
        Parser p = new Parser();
        Node n = p.parse("function() { a b }");
        Assert.assertTrue(n.equals(new FunctionDefinition(new Block().addNode(new Variable("a")).addNode(new Variable("b")))));
    }

    @Test
    public void GT() {
        Parser p = new Parser();
        Node n = p.parse("register a register b GT a b a");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new RegisterDeclaration("b"))
                .addNode(new GT(new Register("a"), new Register("b"), new Register("a")))));
    }

    @Test
    public void GT_Exprs() {
        Parser p = new Parser();
        Node n = p.parse("register a GT a 3 2");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new GT(new Register("a"), new Literal(3), new Literal(2)))));
    }
    
    @Test
    public void GTE() {
        Parser p = new Parser();
        Node n = p.parse("register a register b GTE a b a");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new RegisterDeclaration("b"))
                .addNode(new GTE(new Register("a"), new Register("b"), new Register("a")))));
    }

    @Test
    public void GTE_Exprs() {
        Parser p = new Parser();
        Node n = p.parse("register a GTE a 3 2");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new GTE(new Register("a"), new Literal(3), new Literal(2)))));
    }
    
    @Test
    public void IfStatementTrueEmpty() {
        Parser p = new Parser();
        Node n = p.parse("if (1) { }");
        Assert.assertTrue(n.equals(new IfStatement(new Literal(1), new Block(), null)));
    }
    @Test
    public void IfStatementTrue() {
        Parser p = new Parser();
        Node n = p.parse("if (1) 2 ");
        Assert.assertTrue(n.equals(new IfStatement(new Literal(1), new Literal(2), null)));
    }

    @Test
    public void IfStatementTrueBlockIgnored() {
        Parser p = new Parser();
        Node n = p.parse("if (1) { 2 }");        
        Assert.assertTrue(n.equals(new IfStatement(new Literal(1), new Literal(2), null)));
    }

    @Test
    public void IfStatementTrueBlock() {
        Parser p = new Parser();
        Node n = p.parse("if (1) { 2 3 }");
        Assert.assertTrue(n.equals(new IfStatement(new Literal(1), new Block().addNode(new Literal(2)).addNode(new Literal(3)), null)));
    }

    @Test
    public void IfStatementTrueEmptyFalse() {
        Parser p = new Parser();
        Node n = p.parse("if (1) { } else 2");
        Assert.assertTrue(n.equals(new IfStatement(new Literal(1), new Block(), new Literal(2))));
    }

    @Test
    public void IfStatementTrueEmptyFalseBlockIgnored() {
        Parser p = new Parser();
        Node n = p.parse("if (1) { } else { 2 }");
        Assert.assertTrue(n.equals(new IfStatement(new Literal(1), new Block(), new Literal(2))));
    }

    @Test
    public void IfStatementTrueEmptyFalseBlock() {
        Parser p = new Parser();
        Node n = p.parse("if (1) { } else { 2 1 }");
        Assert.assertTrue(n.equals(new IfStatement(new Literal(1), new Block(), new Block().addNode(new Literal(2)).addNode(new Literal(1)))));
    }

    @Test
    public void IfStatementTrueEmptyFalseEmpty() {
        Parser p = new Parser();
        Node n = p.parse("if (1) { } else { }");
        Assert.assertTrue(n.equals(new IfStatement(new Literal(1), new Block(), null)));
    }

    @Test
    public void IfStatementTrueFalse() {
        Parser p = new Parser();
        Node n = p.parse("if (1) { 45 } else 2");
        Assert.assertTrue(n.equals(new IfStatement(new Literal(1), new Literal(45), new Literal(2))));
    }

    @Test
    public void IS_CLOSURE() {
        Parser p = new Parser();
        Node n = p.parse("register a register b IS_CLOSURE a b");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new RegisterDeclaration("b"))
                .addNode(new IS_CLOSURE(new Register("a"), new Register("b")))));
    }

    @Test
    public void IS_CLOSURE_Exprs() {
        Parser p = new Parser();
        Node n = p.parse("register a IS_CLOSURE a b");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new IS_CLOSURE(new Register("a"), new Variable("b")))));
    }

    @Test
    public void Literal() {
        Parser p = new Parser();
        Node n = p.parse("1");
        Assert.assertTrue(n.equals(new Literal(1)));
    }

    @Test
    public void LOAD_REG() {
        Parser p = new Parser();
        Node n = p.parse("register a register b LOAD_REG a b");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new RegisterDeclaration("b"))
                .addNode(new LOAD_REG(new Register("a"), new Register("b")))));
    }

    @Test
    public void LOAD_VAR() {
        Parser p = new Parser();
        Node n = p.parse("register a LOAD_VAR a b");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new LOAD_VAR(new Register("a"), "b"))));
    }
    
    @Test
    public void LT() {
        Parser p = new Parser();
        Node n = p.parse("register a register b LT a b a");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new RegisterDeclaration("b"))
                .addNode(new LT(new Register("a"), new Register("b"), new Register("a")))));
    }

    @Test
    public void LT_Exprs() {
        Parser p = new Parser();
        Node n = p.parse("register a LT a 2 3");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new LT(new Register("a"), new Literal(2), new Literal(3)))));
    }
    
    @Test
    public void LTE() {
        Parser p = new Parser();
        Node n = p.parse("register a register b LTE a b a");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new RegisterDeclaration("b"))
                .addNode(new LTE(new Register("a"), new Register("b"), new Register("a")))));
    }

    @Test
    public void LTE_Exprs() {
        Parser p = new Parser();
        Node n = p.parse("register a LTE a 2 3");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new LTE(new Register("a"), new Literal(2), new Literal(3)))));
    }

    @Test
    public void NEQ() {
        Parser p = new Parser();
        Node n = p.parse("register a register b NEQ a b a");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new RegisterDeclaration("b"))
                .addNode(new NEQ(new Register("a"), new Register("b"), new Register("a")))));
    }

    @Test
    public void NEQ_Exprs() {
        Parser p = new Parser();
        Node n = p.parse("register a NEQ a 2 3");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new NEQ(new Register("a"), new Literal(2), new Literal(3)))));
    }

    @Test
    public void NOP() {
        Parser p = new Parser();
        Node n = p.parse("NOP NOP");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new NOP())
                .addNode(new NOP())));
    }

    @Test
    public void Register() {
        Parser p = new Parser();
        Node n = p.parse("register a a");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new Register("a"))));
    }

    @Test
    public void RegisterAssignment() {
        Parser p = new Parser();
        Node n = p.parse("register a a = 1");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new RegisterAssignment("a", new Literal(1)))));
    }

    @Test
    public void RegisterDeclaration() {
        Parser p = new Parser();
        Node n = p.parse("register a");
        Assert.assertTrue(n.equals(new RegisterDeclaration("a")));
    }

    @Test
    public void RANDOM() {
        Parser p = new Parser();
        Node n = p.parse("register a RANDOM a 0.5");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new RANDOM(new Register("a"), new Literal(0.5)))));
    }

    @Test
    public void RET() {
        Parser p = new Parser();
        Node n = p.parse("RET 1");
        Assert.assertTrue(n.equals(new RET(new Literal(1))));
    }

    @Test
    public void STORE_VAR() {
        Parser p = new Parser();
        Node n = p.parse("register b STORE_VAR b a");
        Assert.assertTrue(n.equals(new Block().addNode(new RegisterDeclaration("b")).addNode(new STORE_VAR("a", new Register("b")))));
    }

    @Test
    public void SUB() {
        Parser p = new Parser();
        Node n = p.parse("register a register b SUB a b a");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new RegisterDeclaration("b"))
                .addNode(new SUB(new Register("a"), new Register("b"), new Register("a")))));
    }

    @Test
    public void SUB_Exprs() {
        Parser p = new Parser();
        Node n = p.parse("register a SUB a 2 3");
        Assert.assertTrue(n.equals(new Block()
                .addNode(new RegisterDeclaration("a"))
                .addNode(new SUB(new Register("a"), new Literal(2), new Literal(3)))));
    }

    @Test
    public void Variable() {
        Parser p = new Parser();
        Node n = p.parse("abc");
        Assert.assertTrue(n.equals(new Variable("abc")));
    }
    
    @Test
    public void whileTest() {
      Parser p = new Parser();
      Node n = p.parse("b = 1 while (b < 10) { b = b + 1 }");      
      Assert.assertTrue(n.equals(new Block(new tinyr.ast.Assignment("b", new Literal(1)), 
              new WhileLoop(new BinaryOperator(new Variable("b"), new Literal(10), Token.Type.opLesser), 
              new tinyr.ast.Assignment("b", new BinaryOperator( new Variable("b"), new Literal(1), Token.Type.opAdd))))));
    }
    
    @Test
    public void whileBreak() {
      Parser p = new Parser();
      Node n = p.parse("b = 1 a = 10 while (b < 10) { b = b + 1 break a = a + 1}");      
      Assert.assertTrue(n.equals(new Block(new tinyr.ast.Assignment("b", new Literal(1)), new tinyr.ast.Assignment("a", new Literal(10)), 
              new WhileLoop(new BinaryOperator(new Variable("b"), new Literal(10), Token.Type.opLesser), 
              new Block(new tinyr.ast.Assignment("b", new BinaryOperator( new Variable("b"), new Literal(1), Token.Type.opAdd)), 
              new BreakStatement(), new tinyr.ast.Assignment("a", new BinaryOperator( new Variable("a"), new Literal(1), Token.Type.opAdd)))))));
    } 
    
    @Test
    public void whileContinue() {
      Parser p = new Parser();
      Node n = p.parse("b = 1 a = 10 while (b < 10) { b = b + 1 continue a = a + 1}");      
      Assert.assertTrue(n.equals(new Block(new tinyr.ast.Assignment("b", new Literal(1)), new tinyr.ast.Assignment("a", new Literal(10)), 
              new WhileLoop(new BinaryOperator(new Variable("b"), new Literal(10), Token.Type.opLesser), 
              new Block(new tinyr.ast.Assignment("b", new BinaryOperator( new Variable("b"), new Literal(1), Token.Type.opAdd)), 
              new ContinueStatement(), new tinyr.ast.Assignment("a", new BinaryOperator( new Variable("a"), new Literal(1), Token.Type.opAdd)))))));
    }   
        
    @Test
    public void whileBreakContinue() {
      Parser p = new Parser();
      Node n = p.parse("b = 1 a = 5 while (b < 10) { if (b == 10) break else b = b + 1 continue a = a + 1 }}");      
      Assert.assertTrue(n.equals(new Block(new tinyr.ast.Assignment("b", new Literal(1)), new tinyr.ast.Assignment("a", new Literal(5)), 
              new WhileLoop(new BinaryOperator(new Variable("b"), new Literal(10), Token.Type.opLesser), 
              new Block( new IfStatement( new BinaryOperator( new Variable("b"), new Literal(10), Token.Type.opEq), new BreakStatement(), 
                                          new tinyr.ast.Assignment("b",new tinyr.ast.BinaryOperator(new tinyr.ast.Variable("b"),new tinyr.ast.Literal(1.0), Token.Type.opAdd))), 
                                          new tinyr.ast.ContinueStatement(), new tinyr.ast.Assignment("a", new tinyr.ast.BinaryOperator(new tinyr.ast.Variable("a"), new tinyr.ast.Literal(1.0), Token.Type.opAdd)))))));
    }         
}
