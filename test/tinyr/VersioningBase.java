package tinyr;

import org.junit.Assert;
import tinyr.bytecode.*;

import java.util.HashSet;

/**
 */
public class VersioningBase {

    @SuppressWarnings("deprecation")
    public void assertPatchAndRollback(Code.View c, Code.Patch p, Instruction[] original, Instruction... expected) {
        c.closeAll();
        HashSet<Code.Patch> patches = new HashSet<>();
        Code.View o = new Code(original).getView();
        Assert.assertEquals(o.insnsToString(), c.codeObject().getView(patches).insnsToString());
        patches.add(p);
        Code.View e = new Code(expected).getView();
        Assert.assertEquals(e.insnsToString(), c.codeObject().getView(patches).insnsToString());
    }

}
