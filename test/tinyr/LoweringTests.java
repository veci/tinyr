package tinyr;

import java.util.Vector;

import org.junit.*;

import tinyr.Parser.Token;
import tinyr.ast.Assignment;
import tinyr.ast.Block;
import tinyr.ast.BreakStatement;
import tinyr.ast.ContinueStatement;
import tinyr.ast.Node;
import tinyr.ast.RegisterDeclaration;
import tinyr.ast.BinaryOperator;
import tinyr.ast.FunctionCall;
import tinyr.ast.IfStatement;
import tinyr.ast.Literal;
import tinyr.ast.Register;
import tinyr.ast.RegisterAssignment;
import tinyr.ast.Variable;
import tinyr.ast.WhileLoop;
import tinyr.bytecode.*;
import tinyr.bytecode.instructions.*;
import tinyr.runtime.Label;

/**
 */
public class LoweringTests {

    @SuppressWarnings("deprecation")
    private void AssertBlocks(tinyr.ast.Block code, Instruction... expected) {
        Code block = (new Lowering()).lower(code);
        Code.View be = block.getView();
        Code.View other = new Code(expected).getView();
        Assert.assertEquals(be.insnsToString(), other.insnsToString());
    }

    @Test
    public void ADD() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("haha"),
                new tinyr.ast.ADD(new tinyr.ast.Register("haha"), new Literal(1), new Literal(2))
        ),
                new LOAD_CONST(1, 1),
                new LOAD_CONST(2, 2),
                new ADD(0, 1, 2),
                new RET(0)
        );
    }

    @Test
    public void Assignment() {
        AssertBlocks(new tinyr.ast.Block(
                new Assignment("v1", new Literal(1))
        ),
                new LOAD_CONST(0, 1),
                new STORE_VAR("v1", 0),
                new RET(0)
        );
    }


    @Test
    public void BinaryOperatorAdd() {
        AssertBlocks(new tinyr.ast.Block(
                new BinaryOperator(new Literal(1), new Literal(2), Parser.Token.Type.opAdd)
        ),
                new LOAD_CONST(0, 1),
                new LOAD_CONST(1, 2),
                new ADD(2, 0, 1),
                new RET(2)
        );
    }

    @Test
    public void BinaryOperatorSub() {
        AssertBlocks(new tinyr.ast.Block(
                new BinaryOperator(new Literal(1), new Literal(2), Parser.Token.Type.opSub)
        ),
                new LOAD_CONST(0, 1),
                new LOAD_CONST(1, 2),
                new SUB(2, 0, 1),
                new RET(2)
        );
    }

    @Test
    public void BinaryOperatorEq() {
        AssertBlocks(new tinyr.ast.Block(
                new BinaryOperator(new Literal(1), new Literal(2), Parser.Token.Type.opEq)
        ),
                new LOAD_CONST(0, 1),
                new LOAD_CONST(1, 2),
                new EQ(2, 0, 1),
                new RET(2)
        );
    }

    @Test
    public void BinaryOperatorNeq() {
        AssertBlocks(new tinyr.ast.Block(
                new BinaryOperator(new Literal(1), new Literal(2), Parser.Token.Type.opNeq)
        ),
                new LOAD_CONST(0, 1),
                new LOAD_CONST(1, 2),
                new NEQ(2, 0, 1),
                new RET(2)
        );
    }
    
    @Test
    public void BinaryOperatorGT() {      
        AssertBlocks(new tinyr.ast.Block(
                new BinaryOperator(new Literal(3), new Literal(2), Parser.Token.Type.opGreater)
        ),
                new LOAD_CONST(0, 3),
                new LOAD_CONST(1, 2),
                new GT(2, 0, 1),
                new RET(2)
        );
    }
    
    @Test
    public void BinaryOperatorGTE() {
        AssertBlocks(new tinyr.ast.Block(
                new BinaryOperator(new Literal(2), new Literal(2), Parser.Token.Type.opGreaterEquals)
        ),
                new LOAD_CONST(0, 2),
                new LOAD_CONST(1, 2),
                new GTE(2, 0, 1),
                new RET(2)
        );
    }
    
    @Test
    public void BinaryOperatorGTE_() {
        AssertBlocks(new tinyr.ast.Block(
                new BinaryOperator(new Literal(5), new Literal(2), Parser.Token.Type.opGreaterEquals)
        ),
                new LOAD_CONST(0, 5),
                new LOAD_CONST(1, 2),
                new GTE(2, 0, 1),
                new RET(2)
        );
    }
    
    @Test
    public void BinaryOperatorLT() {      
        AssertBlocks(new tinyr.ast.Block(
                new BinaryOperator(new Literal(2), new Literal(3), Parser.Token.Type.opLesser)
        ),
                new LOAD_CONST(0, 2),
                new LOAD_CONST(1, 3),
                new LT(2, 0, 1),
                new RET(2)
        );
    }
    
    @Test
    public void BinaryOperatorLTE() {
        AssertBlocks(new tinyr.ast.Block(
                new BinaryOperator(new Literal(2), new Literal(2), Parser.Token.Type.opLesserEquals)
        ),
                new LOAD_CONST(0, 2),
                new LOAD_CONST(1, 2),
                new LTE(2, 0, 1),
                new RET(2)
        );
    }
    
    @Test
    public void BinaryOperatorLTE_() {
        AssertBlocks(new tinyr.ast.Block(
                new BinaryOperator(new Literal(2), new Literal(5), Parser.Token.Type.opLesserEquals)
        ),
                new LOAD_CONST(0, 2),
                new LOAD_CONST(1, 5),
                new LTE(2, 0, 1),
                new RET(2)
        );
    }

    @Test
    public void Block() {
        AssertBlocks(new tinyr.ast.Block(
                new tinyr.ast.NOP(),
                new tinyr.ast.NOP(),
                new tinyr.ast.NOP()
        ),
                new NOP(),
                new NOP(),
                new NOP()
        );
    }

    @Test
    public void EmptyBlock() {
        AssertBlocks(new tinyr.ast.Block(
        )
        );
    }

    @Test
    public void Call() {
      Vector<Node> a = new Vector<Node>();
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("haha"),
                new tinyr.ast.CALL(new tinyr.ast.Register("haha"), new Variable("a"), a)
        ),
                new LOAD_VAR(1, "a"),
                new CALL(0, 1, new int[0]),
                new RET(0)
        );
    }
    
    @Test
    public void EQ() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("haha"),
                new tinyr.ast.EQ(new tinyr.ast.Register("haha"), new Literal(1), new Literal(2))
        ),
                new LOAD_CONST(1, 1),
                new LOAD_CONST(2, 2),
                new EQ(0, 1, 2),
                new RET(0)
        );
    }

    @Test
    public void FunctionCall() {
      Vector<Node> args = new Vector<Node>();
        AssertBlocks(new tinyr.ast.Block(
                new FunctionCall(new Variable("a"),args)
        ),
                new LOAD_VAR(1, "a"),
                new CALL(0, 1, new int[0]),
                new RET(0)
        );
    }
    
    @Test
    public void FunctionCallArg() {
      Vector<Node> args = new Vector<Node>();
      args.add(new Literal(1));
      args.add(new Literal(2));
        AssertBlocks(new tinyr.ast.Block(
                new FunctionCall(new Variable("a"), args)
        ),
                new LOAD_VAR(1, "a"),
                new LOAD_CONST(2, 1),
                new LOAD_CONST(3, 2),
                new CALL(0, 1, new int[] {2,3}),
                new RET(0)
        );
    }
    
    @Test
    public void GT() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("GT"),
                new tinyr.ast.GT(new tinyr.ast.Register("GT"), new Literal(2), new Literal(0))
        ),
                new LOAD_CONST(1, 2),
                new LOAD_CONST(2, 0),
                new GT(0, 1, 2),
                new RET(0)
        );
    }
    
    @Test
    public void GTE() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("GTE"),
                new tinyr.ast.GTE(new tinyr.ast.Register("GTE"), new Literal(2), new Literal(0))
        ),
                new LOAD_CONST(1, 2),
                new LOAD_CONST(2, 0),
                new GTE(0, 1, 2),
                new RET(0)
        );
    }
    
    @Test
    public void GTE_() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("GT"),
                new tinyr.ast.GTE(new tinyr.ast.Register("GT"), new Literal(2), new Literal(2))
        ),
                new LOAD_CONST(1, 2),
                new LOAD_CONST(2, 2),
                new GTE(0, 1, 2),
                new RET(0)
        );
    }

    @Test
    public void IfEmpty() {
        Label l1 = new Label();
        Label l2 = new Label();
        AssertBlocks(new tinyr.ast.Block(
                new IfStatement(new Literal(1), new tinyr.ast.Block(), null)
        ),
                new LOAD_CONST(0, 1),
                new CBR(0, l1, l2),
                new LABEL(l1),
                new BR(l2),
                new LABEL(l2)
        );
   }

   @Test
   public void IfTrue() {
       Label l1 = new Label();
       Label l2 = new Label();
       AssertBlocks(new tinyr.ast.Block(
               new IfStatement(new Literal(1), new Literal(2), null)
       ),
               new LOAD_CONST(0, 1),
               new CBR(0, l1, l2),
               new LABEL(l1),
               new LOAD_CONST(1, 2),
               new BR(l2),
               new LABEL(l2),
               new RET(1)
       );
   }

    @Test
    public void IfFalse() {
        Label l1 = new Label();
        Label l2 = new Label();
        Label l3 = new Label();
        AssertBlocks(new tinyr.ast.Block(
                new IfStatement(new Literal(1), new tinyr.ast.Block(), new Literal(2))
        ),
                new LOAD_CONST(0, 1),
                new CBR(0, l1, l2),
                new LABEL(l1),
                new BR(l3),
                new LABEL(l2),
                new LOAD_CONST(1, 2),
                new BR(l3),
                new LABEL(l3),
                new RET(1)
        );
    }

    @Test
    public void IfTrueFalse() {
        Label l1 = new Label();
        Label l2 = new Label();
        Label l3 = new Label();
        AssertBlocks(new tinyr.ast.Block(
                new IfStatement(new Literal(1), new Literal(3), new Literal(2))
        ),
                new LOAD_CONST(0, 1),
                new CBR(0, l1, l2),
                new LABEL(l1),
                new LOAD_CONST(1, 3),
                new BR(l3),
                new LABEL(l2),
                new LOAD_CONST(2, 2),
                new LOAD_REG(1, 2),
                new BR(l3),
                new LABEL(l3),
                new RET(1)
        );
    }

    @Test
    public void IS_CLOSURE() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("haha"),
                new RegisterDeclaration("bubu"),
                new tinyr.ast.IS_CLOSURE(new Register("haha"), new Register("bubu"))
        ),
                new IS_CLOSURE(0, 1),
                new RET(0)
        );
    }

    @Test
    public void Literal() {
        AssertBlocks(new tinyr.ast.Block(
                new Literal(1)
        ),
                new LOAD_CONST(0, 1),
                new RET(0)
        );
    }

    @Test
    public void LOAD_REG() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("haha"),
                new RegisterDeclaration("bubu"),
                new tinyr.ast.LOAD_REG(new Register("haha"), new Register("bubu"))
        ),
                new LOAD_REG(0, 1),
                new RET(0)
        );
    }

    @Test
    public void LOAD_VAR() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("haha"),
                new tinyr.ast.LOAD_VAR(new Register("haha"), "haha")
        ),
                new LOAD_VAR(0, "haha"),
                new RET(0)
        );
    }
   
    @Test
    public void LT() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("LT"),
                new tinyr.ast.LT(new tinyr.ast.Register("LT"), new Literal(0), new Literal(2))
        ),
                new LOAD_CONST(1, 0),
                new LOAD_CONST(2, 2),
                new LT(0, 1, 2),
                new RET(0)
        );
    }
    
    @Test
    public void LTE() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("LTE"),
                new tinyr.ast.LTE(new tinyr.ast.Register("LTE"), new Literal(2), new Literal(5))
        ),
                new LOAD_CONST(1, 2),
                new LOAD_CONST(2, 5),
                new LTE(0, 1, 2),
                new RET(0)
        );
    }
    
    @Test
    public void LTE_() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("LTE"),
                new tinyr.ast.LTE(new tinyr.ast.Register("LTE"), new Literal(5), new Literal(5))
        ),
                new LOAD_CONST(1, 5),
                new LOAD_CONST(2, 5),
                new LTE(0, 1, 2),
                new RET(0)
        );
    }

    @Test
    public void NEQ() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("haha"),
                new tinyr.ast.NEQ(new tinyr.ast.Register("haha"), new Literal(1), new Literal(2))
        ),
                new LOAD_CONST(1, 1),
                new LOAD_CONST(2, 2),
                new NEQ(0, 1, 2),
                new RET(0)
        );
    }

    @Test
    public void NOP() {
        AssertBlocks(new tinyr.ast.Block(
                new tinyr.ast.NOP()
        ),
                new NOP()
        );
    }

    @Test
    public void RegisterAssignmentFromReg() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("haha"),
                new RegisterDeclaration("bubu"),
                new RegisterAssignment("haha", new Register("bubu"))
        ),
                new LOAD_REG(0, 1),
                new RET(0)
        );
    }

    @Test
    public void RegisterAssignmentFromVar() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("haha"),
                new RegisterAssignment("haha", new Variable("bubu"))
        ),
                new LOAD_VAR(1, "bubu"),
                new LOAD_REG(0, 1),
                new RET(0)
        );
    }

    @Test
    public void RET() {
        AssertBlocks(new tinyr.ast.Block(
                new tinyr.ast.RET(new Literal(1))
        ),
                new LOAD_CONST(0, 1),
                new RET(0)
        );
    }

    @Test
    public void STORE_VAR() {
        AssertBlocks(new tinyr.ast.Block(
                new tinyr.ast.STORE_VAR("haha", new Literal(1))
        ),
                new LOAD_CONST(0, 1),
                new STORE_VAR("haha", 0),
                new RET(0)
        );
    }

    @Test
    public void SUB() {
        AssertBlocks(new tinyr.ast.Block(
                new RegisterDeclaration("haha"),
                new tinyr.ast.SUB(new tinyr.ast.Register("haha"), new Literal(1), new Literal(2))
        ),
                new LOAD_CONST(1, 1),
                new LOAD_CONST(2, 2),
                new SUB(0, 1, 2),
                new RET(0)
        );
    }

    @Test
    public void Variable() {
        AssertBlocks(new tinyr.ast.Block(
                new Variable("haha")
        ),
                new LOAD_VAR(0, "haha"),
                new RET(0)
        );
    }        
    
    @Test
    public void whileTest() {
        Label l1 = new Label();
        Label l2 = new Label();
        Label l3 = new Label();
      AssertBlocks(new tinyr.ast.Block(
          new tinyr.ast.Assignment("b", new Literal(1)), 
          new WhileLoop(new BinaryOperator(new Variable("b"), new Literal(10), Token.Type.opLesser), 
          new tinyr.ast.Assignment("b", new BinaryOperator( new Variable("b"), new Literal(1), Token.Type.opAdd)))
      ), 
          new LOAD_CONST(0, 1),
          new STORE_VAR("b", 0),
          new BR(l1),
          new LABEL(l1),
          new LOAD_VAR(1, "b"),
          new LOAD_CONST(2, 10),
          new LT(3, 1, 2),
          new CBR(3, l2, l3),
          new LABEL(l2),
          new LOAD_VAR(4, "b"),
          new LOAD_CONST(5, 1),
          new ADD(6, 4, 5),
          new STORE_VAR("b", 6),
          new BR(l1),
          new LABEL(l3)
          );           
    }
    
    @Test
    public void whileBreak() {
        Label l1 = new Label();
        Label l2 = new Label();
        Label l3 = new Label();
        Label l4 = new Label();
      AssertBlocks(new tinyr.ast.Block(
          new tinyr.ast.Assignment("b", new Literal(1)), new tinyr.ast.Assignment("a", new Literal(10)), 
          new WhileLoop(new BinaryOperator(new Variable("b"), new Literal(10), Token.Type.opLesser), 
          new Block(new tinyr.ast.Assignment("b", new BinaryOperator( new Variable("b"), new Literal(1), Token.Type.opAdd)), 
          new BreakStatement(), new tinyr.ast.Assignment("a", new BinaryOperator( new Variable("a"), new Literal(1), Token.Type.opAdd))))
      ), 
          new LOAD_CONST(0, 1),
          new STORE_VAR("b", 0),
          new LOAD_CONST(1, 10),
          new STORE_VAR("a", 1),
          new BR(l1),
          new LABEL(l1),
          new LOAD_VAR(2, "b"),
          new LOAD_CONST(3, 10),
          new LT(4, 2, 3),
          new CBR(4, l2, l4),
          new LABEL(l2),
          new LOAD_VAR(5, "b"),
          new LOAD_CONST(6, 1),
          new ADD(7, 5, 6),
          new STORE_VAR("b", 7),
          new BR(l4),
          new LABEL(l3),
          new LOAD_VAR(8, "a"),
          new LOAD_CONST(9, 1),
          new ADD(10, 8, 9),
          new STORE_VAR("a", 10),
          new BR(l1),
          new LABEL(l4)
          );            
    }
    
    @Test
    public void whileContinue() {
        Label l1 = new Label();
        Label l2 = new Label();
        Label l3 = new Label();
        Label l4 = new Label();
      AssertBlocks(new tinyr.ast.Block(
          new tinyr.ast.Assignment("b", new Literal(1)), new tinyr.ast.Assignment("a", new Literal(10)), 
          new WhileLoop(new BinaryOperator(new Variable("b"), new Literal(10), Token.Type.opLesser), 
          new Block(new tinyr.ast.Assignment("b", new BinaryOperator( new Variable("b"), new Literal(1), Token.Type.opAdd)), 
          new ContinueStatement(), new tinyr.ast.Assignment("a", new BinaryOperator( new Variable("a"), new Literal(1), Token.Type.opAdd))))
      ), 
          new LOAD_CONST(0, 1),
          new STORE_VAR("b", 0),
          new LOAD_CONST(1, 10),
          new STORE_VAR("a", 1),
          new BR(l1),
          new LABEL(l1),
          new LOAD_VAR(2, "b"),
          new LOAD_CONST(3, 10),
          new LT(4, 2, 3),
          new CBR(4, l2, l4),
          new LABEL(l2),
          new LOAD_VAR(5, "b"),
          new LOAD_CONST(6, 1),
          new ADD(7, 5, 6),
          new STORE_VAR("b", 7),
          new BR(l1),
          new LABEL(l3),
          new LOAD_VAR(8, "a"),
          new LOAD_CONST(9, 1),
          new ADD(10, 8, 9),
          new STORE_VAR("a", 10),
          new BR(l1),
          new LABEL(l4)
          );            
    }
    
    @Test
    public void whileContinueBreak() {
        Label l1 = new Label();
        Label l2 = new Label();
        Label l3 = new Label();
        Label l4 = new Label();
        Label l5 = new Label();
        Label l6 = new Label();
        Label l7 = new Label();
      AssertBlocks(new tinyr.ast.Block(
          new tinyr.ast.Assignment("b", new Literal(1)), new tinyr.ast.Assignment("a", new Literal(5)), 
          new WhileLoop(new BinaryOperator(new Variable("b"), new Literal(10), Token.Type.opLesser), 
          new Block( new IfStatement( new BinaryOperator( new Variable("b"), new Literal(10), Token.Type.opEq), new BreakStatement(), 
          new tinyr.ast.Assignment("b",new tinyr.ast.BinaryOperator(new tinyr.ast.Variable("b"),new tinyr.ast.Literal(1.0), Token.Type.opAdd))), 
          new tinyr.ast.ContinueStatement(), new tinyr.ast.Assignment("a", new tinyr.ast.BinaryOperator(new tinyr.ast.Variable("a"), new tinyr.ast.Literal(1.0), Token.Type.opAdd))))
      ), 
          new LOAD_CONST(0, 1),
          new STORE_VAR("b", 0),
          new LOAD_CONST(1, 5),
          new STORE_VAR("a", 1),
          new BR(l1),
          new LABEL(l1),
          new LOAD_VAR(2, "b"),
          new LOAD_CONST(3, 10),
          new LT(4, 2, 3),
          new CBR(4, l2, l7),
          new LABEL(l2),
          new LOAD_VAR(5, "b"),
          new LOAD_CONST(6, 10),
          new EQ(7, 5, 6),
          new CBR(7, l3, l4),
          new LABEL(l3),
          new BR(l7),
          new LABEL(l4),
          new LOAD_VAR(8, "b"),
          new LOAD_CONST(9, 1),
          new ADD(10, 8, 9),
          new STORE_VAR("b", 10),
          new BR(l5),
          new LABEL(l5),
          new BR(l1),
          new LABEL(l6),
          new LOAD_VAR(11, "a"),
          new LOAD_CONST(12, 1),
          new ADD(13, 11, 12),
          new STORE_VAR("a", 13),
          new BR(l1),
          new LABEL(l7)
          );            
    }
}
