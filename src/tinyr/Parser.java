package tinyr;

import tinyr.ast.*;
import tinyr.ast.Assumption;
import tinyr.runtime.*;

import java.util.*;

/**
 * A simple parser, the following is the grammar of tinyThing:
 * <p/>
 * CALLABLE ::= (ident [ ASSIGN ] | number | '(' E ')' ) { opCall } F ::= CALLABLE | FDEF E2 ::= ( '-' | '+' ) E2 | F E1
 * ::= E2 { ( + | - ) E2 } E ::= E1 { ( == | != ) E1 } ASSIGN ::= '=' E FDEF ::= function opCall SORS BODY ::= '{'
 * STATEMENTS '}' STATEMENTS ::= { STATEMENT } STATEMENT ::= IF | IL | E | REGISTER SORS ::= STATEMENT | BODY IF ::= if
 * E SORS [ else SORS ] REGISTER ::= register ident IL ::= direct instructions :)
 */
public class Parser {

    public static class Token {
        public static enum Type {
            identifier,
            number,
            stringLiteral,
            kwIf,
            kwElse,
            kwWhile,
            kwBreak,
            kwContinue,
            kwFunction,
            kwRegister,
            opAdd,
            opSub,
            opMul,
            opDiv,
            opAssign,
            opEq,
            opNeq,
            opAnd,
            opOr,
            opCurlyOpen,
            opCurlyClose,
            opBracketOpen,
            opBracketClose,
            opParOpen,
            opParClose,
            //opCall,
            opComma,
            opLesser,
            opGreater,
            opLesserEquals,
            opGreaterEquals,
            opColon,
            kwClosure,
            kwDouble,
            kwConstClosure,
            kwIs,
            EOF,

            iADD,
            iAND,
            iARRAY_CREATE,
            iARRAY_GET,
            iARRAY_LENGTH,
            iARRAY_SET,
            iASM,
            iBKPT,
            iCALL,
            iDIV,
            iEQ,
            iGT,
            iGTE,
            iINV,
            iIS_ARRAY,
            iIS_CLOSURE,
            iIS_DOUBLE,
            iLOAD_REG,
            iLOAD_VAR,
            iLT,
            iLTE,
            iMUL,
            iNEQ,
            iNOP,
            iOPTIMIZE,
            iOR,
            iRANDOM,
            iRET,
            iSTORE_VAR,
            iSUB,
            comments, iUSE
        }

        static HashMap<String, Type> keywords_ = new HashMap<>();

        static {
            keywords_.put("if", Type.kwIf);
            keywords_.put("else", Type.kwElse);
            keywords_.put("while", Type.kwWhile);
            keywords_.put("break", Type.kwBreak);
            keywords_.put("continue", Type.kwContinue);
            keywords_.put("function", Type.kwFunction);
            keywords_.put("register", Type.kwRegister);
            keywords_.put("+", Type.opAdd);
            keywords_.put("-", Type.opSub);
            keywords_.put("*", Type.opMul);
            keywords_.put("/", Type.opDiv);
            keywords_.put("=", Type.opAssign);
            keywords_.put("==", Type.opEq);
            keywords_.put("!=", Type.opNeq);
            keywords_.put("&&", Type.opAnd);
            keywords_.put("||", Type.opOr);
            keywords_.put("{", Type.opCurlyOpen);
            keywords_.put("}", Type.opCurlyClose);
            keywords_.put("[", Type.opBracketOpen);
            keywords_.put("]", Type.opBracketClose);
            keywords_.put("(", Type.opParOpen);
            keywords_.put(")", Type.opParClose);
            keywords_.put(",", Type.opComma);
            //keywords_.put("()", Type.opCall);
            keywords_.put("<", Type.opLesser);
            keywords_.put(">", Type.opGreater);
            keywords_.put("<=", Type.opLesserEquals);
            keywords_.put(">=", Type.opGreaterEquals);
            keywords_.put(":", Type.opColon);
            keywords_.put("is", Type.kwIs);
            keywords_.put("double", Type.kwDouble);
            keywords_.put("closure", Type.kwClosure);
            keywords_.put("constClosure", Type.kwConstClosure);
            keywords_.put("//", Type.comments);

            keywords_.put("AND", Type.iAND);
            keywords_.put("ADD", Type.iADD);
            keywords_.put("ARRAY_CREATE", Type.iARRAY_CREATE);
            keywords_.put("ARRAY_GET", Type.iARRAY_GET);
            keywords_.put("ARRAY_LENGTH", Type.iARRAY_LENGTH);
            keywords_.put("ARRAY_SET", Type.iARRAY_SET);
            keywords_.put("ASM", Type.iASM);
            keywords_.put("BKPT", Type.iBKPT);
            keywords_.put("CALL", Type.iCALL);
            keywords_.put("DIV", Type.iDIV);
            keywords_.put("EQ", Type.iEQ);
            keywords_.put("GT", Type.iGT);
            keywords_.put("GTE", Type.iGTE);
            keywords_.put("INV", Type.iINV);
            keywords_.put("IS_ARRAY", Type.iIS_ARRAY);
            keywords_.put("IS_CLOSURE", Type.iIS_CLOSURE);
            keywords_.put("IS_DOUBLE", Type.iIS_DOUBLE);
            keywords_.put("LOAD_REG", Type.iLOAD_REG);
            keywords_.put("LOAD_VAR", Type.iLOAD_VAR);
            keywords_.put("LT", Type.iLT);
            keywords_.put("LTE", Type.iLTE);
            keywords_.put("MUL", Type.iMUL);
            keywords_.put("NEQ", Type.iNEQ);
            keywords_.put("NOP", Type.iNOP);
            keywords_.put("OPTIMIZE", Type.iOPTIMIZE);
            keywords_.put("OR", Type.iOR);
            keywords_.put("RANDOM", Type.iRANDOM);
            keywords_.put("RET", Type.iRET);
            keywords_.put("STORE_VAR", Type.iSTORE_VAR);
            keywords_.put("SUB", Type.iSUB);
            keywords_.put("USE", Type.iUSE);
//            keywords_.put("", Type.i);
        }

        final Type type;
        final String name;
        final double value;

        public Token(Type type) {
            this.type = type;
            this.name = "";
            this.value = 0;
        }

        public Token(Type type, String name) {
            this.type = type;
            this.name = name;
            this.value = 0;
        }

        public Token(Type type, double value) {
            this.type = type;
            this.name = "";
            this.value = value;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder(type.toString());
            switch (type) {
                case identifier:
                    sb.append(" ");
                    sb.append(name);
                    break;
                case number:
                    sb.append(" ");
                    sb.append(value);
                    break;
                default:
                    // pass
            }
            return sb.toString();
        }
    }

    private Set<String> registers_ = new HashSet<>();

    private Vector<Token> tokens_ = new Vector<>();
    private int tidx_ = 0;

    private boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    private boolean isIdentifierLetter(char c) {
        return isDigit(c) || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c == '_');
    }

    private void scan(String input) {
        input = input + "\n"; // termination by end of line so that we do not have to check everywhere               
        int i = 0;
        while (i < input.length()) {
            char c = input.charAt(i);
            if (c == ' ' || c == '\n' || c == '\t') {
                ++i;
            } else if (c == '\'') {
                ++i;
                double d = input.charAt(i);
                ++i;
                if (input.charAt(i) != '\'')
                    throw new RuntimeException("Unterminated character");
                ++i;
                tokens_.add(new Token(Token.Type.number, d));
            } else if (c == '"') {
                ++i;
                StringBuilder sb = new StringBuilder();
                while (true) {
                    if (input.charAt(i) == '"') {
                        ++i;
                        break;
                    }
                    if (input.charAt(i) == '\\') {
                        ++i;
                        switch (input.charAt(i)) {
                            case 'n':
                                sb.append("\n");
                                break;
                            case 't':
                                sb.append("\t");
                                break;
                            default:
                                sb.append(input.charAt(i));
                        }
                    } else {
                        sb.append(input.charAt(i));
                    }
                    ++i;
                }
                tokens_.add(new Token(Token.Type.stringLiteral, sb.toString()));
            } else if (isDigit(c)) {
                double value = c - '0';
                ++i;
                while (isDigit(input.charAt(i)))
                    value = value * 10 + (input.charAt(i++) - '0');
                if (input.charAt(i) == '.') {
                    double divider = 10;
                    ++i;
                    while (isDigit(input.charAt(i))) {
                        value = value + (input.charAt(i++) - '0') / divider;
                        divider *= 10;
                    }
                }
                if (input.charAt(i) == 'e') {
                    ++i;
                    int sign = 1;
                    if (input.charAt(i) == '-') {
                        sign = -1;
                        ++i;
                    } else if (input.charAt(i)== '+') {
                        ++i;
                    }
                    int exp = 0;
                    while (isDigit(input.charAt(i)))
                        exp = 10 * exp + (input.charAt(i++) - '0');
                    value = value * Math.pow(10, sign * exp);
                }
                tokens_.add(new Token(Token.Type.number, value));
            } else {
                int start = i;
                if (isIdentifierLetter(c)) { // if we codeAddress as an identifier, consider only identifier characters
                    ++i;
                    while (isIdentifierLetter(input.charAt(i)))
                        ++i;
                } else {
                    c = input.charAt(i);
                    String s = "" + c;
                    int longest = -1;
                    while (!isDigit(c) && c != ' ' && c != '\n' && c != '\t' && !isIdentifierLetter(c)) {
                        Token.Type type = Token.keywords_.get(s);
                        if (type != null)
                            longest = i + 1;
                        c = input.charAt(++i);
                        s = s + c;
                    }
                    if (longest != -1)
                        i = longest;
                }
                String str = input.substring(start, i);
                Token.Type type = Token.keywords_.get(str);
                if (type == null)
                    tokens_.add(new Token(Token.Type.identifier, str));
                else if (type == Token.Type.comments) {
                    i = input.indexOf("\n", i);
                    if (i == -1)
                        i = input.length();
                } else {
                    tokens_.add(new Token(type));
                }
            }
        }
    }

    public Node parse(String input) {
        tokens_.clear();
        registers_.clear();
        tidx_ = 0;
        scan(input);
        tokens_.add(new Token(Token.Type.EOF));
        return parseStatements();
    }

    public boolean eof() {
        return top().type == Token.Type.EOF;
    }

    private Token top() {
        return tokens_.get(tidx_);
    }

    private Token pop() {
        Token t = top();
        if (!eof())
            ++tidx_;
        return t;
    }

    private boolean condPop(Token.Type type) {
        if (top().type != type)
            return false;
        pop();
        return true;
    }

    private Token pop(Token.Type type) {
        if (top().type != type)
            throw new RuntimeException("Expected " + type + " but " + top().type + "found");
        return pop();
    }

    public Register parseRegister() {
        String ident = pop(Token.Type.identifier).name;
        if (!registers_.contains(ident))
            throw new RuntimeException("Expected register, but variable " + ident + " found. Did you forget the register declaration?");
        return new Register(ident);
    }

    private Node parseCallable() {
        Node result = null;
        if (top().type == Token.Type.stringLiteral) {
            result = new Literal(Array.fromString(pop().name));

        } else if (top().type == Token.Type.number) {
            result = new Literal(pop().value);
        } else if (condPop(Token.Type.opParOpen)) {
            result = parseE();
            pop(Token.Type.opParClose);
        } else {
            String ident = pop(Token.Type.identifier).name;
            if (top().type == Token.Type.opAssign) {
                result = parseAssignment(ident);
            } else {
                if (registers_.contains(ident))
                    result = new Register(ident);
                else
                    result = new Variable(Symbol.make(ident));
            }
        }
        while (top().type == Token.Type.opParOpen)
            result = parseFunctionCall(result);

        return result;
    }

    private Node parseF() {
        if (condPop(Token.Type.kwFunction))
            return parseFunctionDefinition();
        else
            return parseCallable();
    }

    private Node parseFunctionDefinition() {
        // the function keyword has already been passed             
        pop(Token.Type.opParOpen);
        Vector<String> arguments = new Vector<String>();
        while (top().type != Token.Type.opParClose) {

            if (top().type != Token.Type.identifier && top().type != Token.Type.opComma) {
                throw new RuntimeException("Keyword " + top().type + " can not be used as argument !");
            } else if (top().type == Token.Type.opComma) pop();
            else {
                Token s = pop();
                arguments.add(s.name);
            }
        }
        pop(Token.Type.opParClose);

        Set<String> oldRegisters = registers_;
        registers_ = new HashSet<>();
        Node body = parseSors();
        registers_ = oldRegisters;

        if (arguments.size() == 0) {
            return new FunctionDefinition(body);
        } else {
            return new FunctionDefinition(arguments, body);
        }
    }

    private Node parseAssignment(String varName) {
        pop(Token.Type.opAssign);
        if (registers_.contains(varName))
            return new RegisterAssignment(varName, parseE());
        else
            return new Assignment(Symbol.make(varName), parseE());
    }

    private Node parseFunctionCall(Node fcall) {
        pop(Token.Type.opParOpen);
        if (condPop(Token.Type.opParClose)) {
            return new FunctionCall(fcall);

        } else {
            Vector<Node> arguments = new Vector<>();
            arguments.add(parseE());
            while (condPop(Token.Type.opComma))
                arguments.add(parseE());
            pop(Token.Type.opParClose);
            return new FunctionCall(fcall, arguments);
        }
    }


    private Node parseE2() {
        if (condPop(Token.Type.opAdd))
            return parseE2();
        else if (condPop(Token.Type.opSub))
            return new BinaryOperator(new Literal(0), parseE2(), Token.Type.opSub);
        else
            return parseF();
    }

    private Node parseE12() {
        Node result = parseE2();
        while (true) {
            if (condPop(Token.Type.opMul))
                result = new BinaryOperator(result, parseE2(), Token.Type.opMul);
            else if (condPop(Token.Type.opDiv))
                result = new BinaryOperator(result, parseE2(), Token.Type.opDiv);
            else
                return result;
        }

    }


    private Node parseE1() {
        Node result = parseE12();
        while (true) {
            if (condPop(Token.Type.opAdd))
                result = new BinaryOperator(result, parseE12(), Token.Type.opAdd);
            else if (condPop(Token.Type.opSub))
                result = new BinaryOperator(result, parseE12(), Token.Type.opSub);
            else
                return result;
        }
    }

    private Node parseE0() {
        Node result = parseE1();
        while (true) {
            if (condPop(Token.Type.opEq))
                result = new BinaryOperator(result, parseE1(), Token.Type.opEq);
            else if (condPop(Token.Type.opNeq))
                result = new BinaryOperator(result, parseE1(), Token.Type.opNeq);
            else if (condPop(Token.Type.opGreater))
                result = new BinaryOperator(result, parseE1(), Token.Type.opGreater);
            else if (condPop(Token.Type.opLesser))
                result = new BinaryOperator(result, parseE1(), Token.Type.opLesser);
            else if (condPop(Token.Type.opLesserEquals))
                result = new BinaryOperator(result, parseE1(), Token.Type.opLesserEquals);
            else if (condPop(Token.Type.opGreaterEquals))
                result = new BinaryOperator(result, parseE1(), Token.Type.opGreaterEquals);
            else
                return result;
        }
    }

    private Node parseE() {
        Node result = parseE0();
        while (true) {
            if (condPop(Token.Type.opAnd))
                result = new BinaryOperator(result, parseE0(), Token.Type.opAnd);
            else if (condPop(Token.Type.opOr))
                result = new BinaryOperator(result, parseE0(), Token.Type.opOr);
            else
                return result;
        }
    }

    private Node parseBody() {
        pop(Token.Type.opCurlyOpen);
        Node result = parseStatements();
        pop(Token.Type.opCurlyClose);
        return result;
    }

    private Node parseStatements() {
        Block statements = new Block();
        Node result = null;
        while (top().type != Token.Type.opCurlyClose && top().type != Token.Type.EOF) {
            result = parseStatement();
            statements.addNode(result);
        }
        return statements.size() <= 1 ? result : statements;
    }

    private tinyr.ast.Assumption parseAssumption() {
        Symbol name = Symbol.make(pop(Token.Type.identifier).name);
        switch (top().type) {
            case opEq:
                pop();
                return new tinyr.ast.Assumption.IsConstDouble(name, pop(Token.Type.number).value);
            case kwIs:
                pop();
                if (condPop(Token.Type.kwDouble))
                    return new Assumption.IsDouble(name);
                else if (condPop(Token.Type.kwClosure))
                    return new tinyr.ast.Assumption.IsClosure(name);
                else if (condPop(Token.Type.kwConstClosure))
                    return new Assumption.IsConstClosure(name, Symbol.make(pop(Token.Type.identifier).name));
            default:
                /* TODO closures are evaluated at runtime and we therefore do not support creation of explicit constant closure guards

                 However, this is a relic from R, tinyR does not require its closures to be evaluated at runtime and so
                 this can be changed if we expect this behavior to stay for long.
                  */
                throw new RuntimeException("Invalid GUARD instruction syntax (name == value, name < value, name > value, name is double or name is closure allowed). Is constant closure is not supported directly because closures are evaluated at runtime.");
        }
    }

    private Node parseStatement() {
        switch (top().type) {
            case kwIf:
                return parseIfStatement();
            case kwWhile:
                return parseWhileStatement();
            case kwContinue: {
                pop(Token.Type.kwContinue);
                return new ContinueStatement();
            }
            case kwBreak: {
                pop(Token.Type.kwBreak);
                return new BreakStatement();
            }
            case kwRegister: {
                pop();
                String registerName = pop(Token.Type.identifier).name;
                if (registers_.contains(registerName))
                    throw new RuntimeException("Register " + registerName + " already declared.");
                registers_.add(registerName);
                return new RegisterDeclaration(registerName);
            }
            case iADD: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new ADD(dreg, sreg, sreg2);
            }
            case iAND: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new AND(dreg, sreg, sreg2);
            }
            case iARRAY_CREATE: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new ARRAY_CREATE(dreg, sreg, sreg2);
            }
            case iARRAY_GET: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new ARRAY_GET(dreg, sreg, sreg2);
            }
            case iARRAY_LENGTH: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                return new ARRAY_LENGTH(dreg, sreg);
            }
            case iARRAY_SET: {
                pop();
                Node dreg = parseE();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new ARRAY_SET(dreg, sreg, sreg2);
            }
            case iASM: {
                pop();
                double aid = pop(Token.Type.number).value;
                if (condPop(Token.Type.opColon))
                    return new ASM((int) aid, parseAssumption());
                else
                    return new ASM((int) aid, null);
            }
            case iBKPT:
                pop();
                return new BKPT();
            case iCALL: {
                pop();
                Register dreg = parseRegister();
                Node function = parseE();

                Vector<Node> arguments = new Vector<>();
                pop(Token.Type.opBracketOpen);
                if (condPop(Token.Type.opBracketClose)) {
                    arguments.clear();
                    pop();
                } else {
                    Node first = parseE();
                    arguments.add(first);
                    while (condPop(Token.Type.opComma))
                        arguments.add(parseE());
                    pop(Token.Type.opBracketClose);
                }
                return new CALL(dreg, function, arguments);
            }
            case iDIV: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new DIV(dreg, sreg, sreg2);
            }
            case iEQ: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new EQ(dreg, sreg, sreg2);
            }
            case iGT: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new GT(dreg, sreg, sreg2);
            }
            case iGTE: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new GTE(dreg, sreg, sreg2);
            }
            case iINV:
                pop();
                return new INV((int) pop(Token.Type.number).value);
            case iIS_ARRAY: {
                pop();
                Register dreg = parseRegister();
                Node function = parseE();
                return new IS_ARRAY(dreg, function);
            }
            case iIS_CLOSURE: {
                pop();
                Register dreg = parseRegister();
                Node function = parseE();
                return new IS_CLOSURE(dreg, function);
            }
            case iIS_DOUBLE: {
                pop();
                Register dreg = parseRegister();
                Node function = parseE();
                return new IS_DOUBLE(dreg, function);
            }
            case iLOAD_REG: {
                pop();
                Register dreg = parseRegister();
                Node function = parseE();
                return new LOAD_REG(dreg, function);
            }
            case iLOAD_VAR: {
                pop();
                Register dreg = parseRegister();
                String name = pop(Token.Type.identifier).name;
                return new LOAD_VAR(dreg, Symbol.make(name));
            }
            case iLT: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new LT(dreg, sreg, sreg2);
            }
            case iLTE: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new LTE(dreg, sreg, sreg2);
            }
            case iMUL: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new MUL(dreg, sreg, sreg2);
            }
            case iNEQ: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new NEQ(dreg, sreg, sreg2);
            }
            case iNOP:
                pop();
                return new NOP();
            case iOPTIMIZE: {
                pop();
                String name = pop(Token.Type.identifier).name;
                return new OPTIMIZE(Symbol.make(name));
            }
            case iOR: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new OR(dreg, sreg, sreg2);
            }
            case iRANDOM: {
                pop();
                Register dreg = parseRegister();
                Node function = parseE();
                return new RANDOM(dreg, function);
            }
            case iRET: {
                pop();
                Node sreg = parseE();
                return new RET(sreg);
            }
            case iSTORE_VAR: {
                pop();
                Register dreg = parseRegister();
                String name = pop(Token.Type.identifier).name;
                return new STORE_VAR(Symbol.make(name), dreg);
            }
            case iSUB: {
                pop();
                Register dreg = parseRegister();
                Node sreg = parseE();
                Node sreg2 = parseE();
                return new SUB(dreg, sreg, sreg2);
            }
            case iUSE: {
                pop();
                return new USE((int) pop(Token.Type.number).value);
            }
            default:
                return parseE();
        }
    }

    private Node parseSors() {
        if (top().type == Token.Type.opCurlyOpen)
            return parseBody();
        else
            return parseStatement();
    }

    private Node parseIfStatement() {
        pop(Token.Type.kwIf);
        Node cond = parseE();
        Node truePart = parseSors();
        Node falsePart = condPop(Token.Type.kwElse) ? parseSors() : null;
        return new IfStatement(cond, truePart, falsePart);
    }

    private Node parseWhileStatement() {
        pop(Token.Type.kwWhile);
        Node cond = parseE();
        Node body = parseSors();
        return new WhileLoop(cond, body);
    }
}
