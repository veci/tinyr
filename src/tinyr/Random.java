package tinyr;

/**
 */
public class Random {

    static java.util.Random random_ = new java.util.Random(43);

    public static int nextInt() {
        return random_.nextInt();
    }

    public static int nextInt(int bound) {
        return random_.nextInt(bound);
    }

    public static double nextDouble() {
        return random_.nextDouble();
    }

}
