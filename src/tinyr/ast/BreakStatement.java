package tinyr.ast;

import tinyr.Lowering;
import tinyr.bytecode.instructions.BR;

public class BreakStatement extends Node{

  @Override
  public int lower( Lowering lowering ) {   
    if (lowering.breakLabel == null)
        throw new RuntimeException("BREAK out of loop");
    else
        lowering.addInstruction(new BR(lowering.breakLabel));
    return -2;
  }
  
  @Override
  public String toString() {   
    return super.toString() + " break";
  }

}
