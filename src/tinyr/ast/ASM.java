package tinyr.ast;

import tinyr.Lowering;

/**
 */
public class ASM extends Node {

    public final int id;
    public final Assumption assumption;
    
    public ASM(int id, Assumption assumption) {
        this.id = id;
        this.assumption = assumption;
    }
    
    @Override
    public int lower(Lowering lowering) {
        tinyr.runtime.Assumption asm;
        if (assumption == null) {
            asm = lowering.getAssumption(id);
        } else {
            asm = assumption.lower(lowering);
            lowering.addAssumption(id, asm);
        }
        lowering.addInstruction(new tinyr.bytecode.instructions.ASM(asm));
        return -2;
    }
}
