package tinyr.ast;

import tinyr.Lowering;

/**
 */
public class INV extends Node {

    public final int id;

    public INV(int id) {
        this.id = id;
    }

    @Override
    public int lower(Lowering lowering) {
        lowering.addInstruction(new tinyr.bytecode.instructions.INV(lowering.getAssumption(id)));
        return -2;
    }
}
