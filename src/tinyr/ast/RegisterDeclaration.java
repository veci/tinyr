package tinyr.ast;

import tinyr.Lowering;

/**
 */
public class RegisterDeclaration extends Node {

    public final String registerName;

    public RegisterDeclaration(String registerName) {
        this.registerName = registerName;
    }

    @Override
    public int lower(Lowering lowering) {
        int result = lowering.newRegister();
        lowering.state.registerAliases.put(registerName, result);
        return result;
    }

    @Override
    public String toString() {
        return super.toString() + "(" + registerName + ")";
    }
}
