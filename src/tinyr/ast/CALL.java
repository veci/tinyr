package tinyr.ast;

import java.util.Vector;

import tinyr.Lowering;

/**
 */
public class CALL extends Node {

    public final Register dreg;
    public final Node function;
    public final Vector<Node> args;

    public CALL(Register dreg, Node function, Vector<Node> args) {
        this.dreg = dreg;
        this.function = function;
        this.args = args;
    }

    @Override
    public int lower(Lowering lowering) {
        int d = dreg.lower(lowering);
        int f = function.lower(lowering);
        int[] arguments = new int[args.size()];
                         
        for (int i = 0; i < args.size(); ++i)
            arguments[i] = args.get(i).lower(lowering);                           
                
        lowering.addInstruction(new tinyr.bytecode.instructions.CALL(d, f, arguments));
        return d;
    }
    @Override
    public String toString() {
        return super.toString() + "(" + dreg.toString() + ", " + function.toString() + " " + args.toString() + " )";
    }
}
