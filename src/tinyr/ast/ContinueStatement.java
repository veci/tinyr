package tinyr.ast;

import tinyr.Lowering;
import tinyr.bytecode.instructions.BR;

public class ContinueStatement extends Node{

    @Override
    public int lower( Lowering lowering ) {
        if (lowering.continueLabel == null)
            throw new RuntimeException("CONTINUE out of loop");
        else
            lowering.addInstruction(new BR(lowering.continueLabel));
        return -2;
    }
  
    @Override
    public String toString() {
        return super.toString() + " continue";
    }

}
