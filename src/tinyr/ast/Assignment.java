package tinyr.ast;

import tinyr.Lowering;
import tinyr.bytecode.instructions.FDEF;
import tinyr.bytecode.instructions.STORE_VAR;
import tinyr.bytecode.instructions.LOAD_REG;
import tinyr.runtime.Closure;
import tinyr.runtime.Symbol;

/**
 */
public class Assignment extends Node {

    public final Symbol variable;
    public final Node rhs;

    public Assignment(Symbol variable, Node rhs) {
        this.variable = variable;
        this.rhs = rhs;
    }

    public Assignment(String variable, Node rhs) {
        this.variable = Symbol.make(variable);
        this.rhs = rhs;
    }

    @Override
    public int lower(Lowering lowering) {
        int r = rhs.lower(lowering);
        if (rhs instanceof FunctionDefinition) {
            Closure cls = ((FDEF) lowering.getLastInstruction()).closure;
            Lowering.definedClosures.put(variable, cls);
        }
        if (lowering.state.localArgs != null) {
            for (int i = 0; i < lowering.state.localArgs.length; ++i) {
                if (lowering.state.localArgs[i] == variable) {
                    lowering.addInstruction(new LOAD_REG(i, r));
                    return i;
                }
            }
        }

        lowering.addInstruction(new STORE_VAR(variable, r));
        return r;
    }

    @Override
    public String toString() {
        return super.toString() + "(" + variable + ", " + rhs.toString() + ")";
    }
}
