package tinyr.ast;

import tinyr.Lowering;

/**
 */
public class BKPT extends Node {

    @Override
    public int lower(Lowering lowering) {
        lowering.addInstruction(new tinyr.bytecode.instructions.BKPT());
        return -2;
    }
}
