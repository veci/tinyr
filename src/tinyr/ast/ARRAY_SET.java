package tinyr.ast;

import tinyr.Lowering;

/**
 */
public class ARRAY_SET extends Node {

    public final Node dreg;
    public final Node sreg;
    public final Node sreg2;

    public ARRAY_SET(Node dreg, Node sreg, Node sreg2) {
        this.dreg = dreg;
        this.sreg = sreg;
        this.sreg2 = sreg2;
    }

    @Override
    public int lower(Lowering lowering) {
        int d = dreg.lower(lowering);
        int s = sreg.lower(lowering);
        int s2 = sreg2.lower(lowering);
        lowering.addInstruction(new tinyr.bytecode.instructions.ARRAY_SET(d, s, s2));
        return d;
    }

    @Override
    public String toString() {
        return super.toString() + "(" + dreg.toString() + sreg.toString() + ", " + sreg2.toString() + ")";
    }
}
