package tinyr.ast;

import java.util.Vector;

import tinyr.Lowering;
import tinyr.bytecode.instructions.BR;
import tinyr.bytecode.instructions.CBR;
import tinyr.bytecode.instructions.LABEL;
import tinyr.runtime.Label;

public class WhileLoop extends Node {
    public final Node condition;
    public final Node body;

    public WhileLoop(Node cond, Node body) {
        this.condition = cond;
        this.body = body;
    }

    /** Loop inversion is automatically performed so that loop invariant code motion can be performed easily.
     *
     * Any while (cond) body loop is therefore transformed to
     *
     * if (cond) {
     *     do { body } while (cond);
     * }
     *
     * This guarantees that when code is moved out of loop, it will only be executed if the loop would have been
     * executed at least once.
     */
    @Override
    public int lower(Lowering lowering) {
        int c1 = condition.lower(lowering);
        Label oldBreak = lowering.breakLabel;
        Label oldContinue = lowering.continueLabel;
        lowering.breakLabel = new Label();
        lowering.continueLabel = new Label();
        lowering.addInstruction(new CBR(c1, lowering.continueLabel, lowering.breakLabel));
        lowering.addInstruction(new LABEL(lowering.continueLabel));
        body.lower(lowering);
        int c2 = condition.lower(lowering);
        lowering.addInstruction(new CBR(c2, lowering.continueLabel, lowering.breakLabel));
        lowering.addInstruction(new LABEL(lowering.breakLabel));
        lowering.breakLabel = oldBreak;
        lowering.continueLabel = oldContinue;
        return -2;
/*      Label oldBreak = lowering.breakLabel;
        Label oldContinue = lowering.continueLabel;
        lowering.breakLabel = new Label();
        lowering.continueLabel = new Label();
        lowering.addInstruction(new BR(lowering.continueLabel));
        lowering.addInstruction(new LABEL(lowering.continueLabel));
        int cond = condition.lower(lowering);
        Label bodyLabel = new Label();
        lowering.addInstruction(new CBR(cond, bodyLabel, lowering.breakLabel));
        lowering.addInstruction(new LABEL(bodyLabel));
        body.lower(lowering);
        if (! lowering.lastInstruction().isTerminating())
            lowering.addInstruction(new BR(lowering.continueLabel));
        lowering.addInstruction(new LABEL(lowering.breakLabel));
        lowering.breakLabel = oldBreak;
        lowering.continueLabel = oldContinue;
        return -2; */
    }

    @Override
    public String toString() {
        return super.toString() + "(" + condition.toString() + ")" + " { " + body.toString() + " }";
    }

}
