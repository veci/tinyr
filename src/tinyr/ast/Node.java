package tinyr.ast;

import tinyr.Lowering;

/**
 */
public abstract class Node {

    public abstract int lower(Lowering lowering);

    /** A comparison hack for the ast nodes. */
    public boolean equals(Node other) {
        return toString().equals(other.toString());
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }

}
