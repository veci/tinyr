package tinyr.ast;

import tinyr.Lowering;

/**
 */
public class RegisterAssignment extends Node {
    public final String registerName;
    public final Node rhs;

    public RegisterAssignment(String registerName, Node rhs) {
        this.registerName = registerName;
        this.rhs = rhs;
    }

    @Override
    public int lower(Lowering lowering) {
        int d = lowering.state.registerAliases.get(registerName);
        int r = rhs.lower(lowering);
        lowering.addInstruction(new tinyr.bytecode.instructions.LOAD_REG(d, r));
        return d;
    }

    @Override
    public String toString() {
        return super.toString() + "(" + registerName + ", " + rhs.toString() + ")";
    }
}
