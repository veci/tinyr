package tinyr.ast;

import tinyr.Lowering;

/**
 */
public class IS_DOUBLE extends Node {

    public final Register dreg;
    public final Node sreg;

    public IS_DOUBLE(Register dreg, Node sreg) {
        this.dreg = dreg;
        this.sreg = sreg;
    }

    @Override
    public int lower(Lowering lowering) {
        int d = dreg.lower(lowering);
        int s = sreg.lower(lowering);
        lowering.addInstruction(new tinyr.bytecode.instructions.IS_DOUBLE(d, s));
        return d;
    }

    @Override
    public String toString() {
        return super.toString() + "(" + dreg.toString() + ", " + sreg.toString() + ", " + ")";
    }

}
