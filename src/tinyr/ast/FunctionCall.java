package tinyr.ast;

import java.util.Vector;

import tinyr.Lowering;
import tinyr.bytecode.instructions.CALL;
import tinyr.bytecode.instructions.INTRINSIC;
import tinyr.runtime.Symbol;

/**
 */
public class FunctionCall extends Node {
    public final Node function;
    public final Vector<Node> arguments;

    public FunctionCall(Node function) {
        this.function = function;       
        this.arguments = null;
    }

    public FunctionCall( Node fcall, Vector<Node> arguments ) {
        this.function = fcall;
        this.arguments = arguments;
    }

    @Override
    public int lower(Lowering lowering) {
        // get return value register
        int result = lowering.newRegister();
        // prepare arguments
        int[] args = arguments == null ? new int[0] : new int[arguments.size()];
        if (args != null)
            for (int i = 0; i < args.length; ++i)
                args[i] = arguments.get(i).lower(lowering);
        // check if we are calling an intrinsic
        if (function instanceof Variable) {
            Symbol s = ((Variable) function).name;
            if (Symbol.intrinsics.contains(s)) {
                lowering.addInstruction(new INTRINSIC(result, s, args));
                return result;
            }
        }
        // we are not calling intrinsic, lower target function and emit CALL instruction
        int target = function.lower(lowering);
        lowering.addInstruction(new CALL(result, target, args));
        return result;
    }

    @Override
    public String toString() {
        if (arguments == null) {
          return super.toString() + "(" + function.toString();
        }else{
          return super.toString() + "(" + function.toString() + " " + arguments.toString() + " )";
        }                  
    }
}
