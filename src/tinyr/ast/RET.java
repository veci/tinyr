package tinyr.ast;

import tinyr.Lowering;

/**
 */
public class RET extends Node {

    public final Node sreg;

    public RET(Node sreg) {
        this.sreg = sreg;
    }

    @Override
    public int lower(Lowering lowering) {
        int s = sreg.lower(lowering);
        lowering.addInstruction(new tinyr.bytecode.instructions.RET(s));
        return -1;
    }

    @Override
    public String toString() {
        return super.toString() + "(" + sreg.toString() + ")";
    }
}
