package tinyr.ast;

import tinyr.Lowering;
import tinyr.runtime.Symbol;

/**
 */
public class LOAD_VAR extends Node {

    public final Register dreg;
    public final Symbol name;

    public LOAD_VAR(Register dreg, Symbol name) {
        this.dreg = dreg;
        this.name = name;
    }

    public LOAD_VAR(Register dreg, String name) {
        this.dreg = dreg;
        this.name = Symbol.make(name);
    }

    @Override
    public int lower(Lowering lowering) {
        int d = dreg.lower(lowering);
        lowering.addInstruction(new tinyr.bytecode.instructions.LOAD_VAR(d, name));
        return d;
    }

    @Override
    public String toString() {
        return super.toString() + "(" + dreg.toString() + ", " + name + ")";
    }
}
