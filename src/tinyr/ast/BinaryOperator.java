package tinyr.ast;

import tinyr.Lowering;
import tinyr.Parser;

/**
 */
public class BinaryOperator extends Node {
  public final Node              left;
  public final Node              right;
  public final Parser.Token.Type type;

  public BinaryOperator( Node left, Node right, Parser.Token.Type type ) {
    this.left = left;
    this.right = right;
    this.type = type;
  }

  @Override
  public int lower( Lowering lowering ) {
    int l = left.lower( lowering );
    int r = right.lower( lowering );
    int result = lowering.newRegister();
    switch ( type ) {
      case opAdd:
        lowering.addInstruction( new tinyr.bytecode.instructions.ADD( result, l, r ) );
        break;
      case opSub:
        lowering.addInstruction( new tinyr.bytecode.instructions.SUB( result, l, r ) );
        break;
      case opAnd:
        lowering.addInstruction( new tinyr.bytecode.instructions.AND( result, l, r ) );
        break;
      case opOr:
        lowering.addInstruction( new tinyr.bytecode.instructions.OR( result, l, r ) );
        break;
      case opMul:
        lowering.addInstruction( new tinyr.bytecode.instructions.MUL( result, l, r ) );
        break;
      case opDiv:
        lowering.addInstruction( new tinyr.bytecode.instructions.DIV( result, l, r ) );
        break;
      case opEq:
        lowering.addInstruction( new tinyr.bytecode.instructions.EQ( result, l, r ) );
        break;
      case opNeq:
        lowering.addInstruction( new tinyr.bytecode.instructions.NEQ( result, l, r ) );
        break;
      case opGreater:
        lowering.addInstruction( new tinyr.bytecode.instructions.GT( result, l, r ) );
        break;
      case opLesser:
        lowering.addInstruction( new tinyr.bytecode.instructions.LT( result, l, r ) );
        break;
      case opLesserEquals:
        lowering.addInstruction( new tinyr.bytecode.instructions.LTE( result, l, r ) );
        break;
      case opGreaterEquals:
        lowering.addInstruction( new tinyr.bytecode.instructions.GTE( result, l, r ) );
        break;
      default:
        throw new RuntimeException( "NotImplemented" );
    }
    return result;
  }

  @Override
  public String toString() {
    return super.toString() + "(" + type.toString() + ", " + left.toString() + ", " + right.toString() + ")";
  }
}
