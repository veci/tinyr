package tinyr.ast;
import tinyr.Lowering;
import tinyr.bytecode.instructions.*;
import tinyr.runtime.Label;

/**
 */
public class IfStatement extends Node {
    public final Node condition;
    public final Node trueCase;
    public final Node falseCase;

    public IfStatement(Node condition, Node trueCase, Node falseCase) {
        this.condition = condition;
        // fix for empty true case
        if (trueCase == null)
            trueCase = new Block();
        this.trueCase = trueCase;
        this.falseCase = falseCase;
    }

    @Override
    public int lower(Lowering lowering) {
        Label labelTrue = new Label();
        Label end = new Label();
        int cond = condition.lower(lowering);
        if (falseCase == null) {
            lowering.addInstruction(new CBR(cond, labelTrue, end));
            lowering.addInstruction(new LABEL(labelTrue));
            int result = trueCase.lower(lowering);
            if (! lowering.lastInstruction().isTerminating())
                lowering.addInstruction(new BR(end));
            lowering.addInstruction(new LABEL(end));
            return result;
        } else {
            Label labelFalse = new Label();
            lowering.addInstruction(new CBR(cond, labelTrue, labelFalse));
            lowering.addInstruction(new LABEL(labelTrue));
            int result = trueCase.lower(lowering);
            if (! lowering.lastInstruction().isTerminating())
                lowering.addInstruction(new BR(end));
            lowering.addInstruction(new LABEL(labelFalse));
            int falseResult = falseCase.lower(lowering);
            if (result >= 0)
                lowering.addInstruction(new tinyr.bytecode.instructions.LOAD_REG(result, falseResult));
            else
                result = falseResult;
            if (! lowering.lastInstruction().isTerminating())
                lowering.addInstruction(new BR(end));
            lowering.addInstruction(new LABEL(end));
            return result;
        }
    }

    @Override
    public String toString() {
        if (falseCase == null)
            return super.toString() + "(" + condition.toString() + ", " + trueCase.toString() + ")";
        else
            return super.toString() + "(" + condition.toString() + ", " + trueCase.toString() + ", " + falseCase.toString() + ")";
    }
}
