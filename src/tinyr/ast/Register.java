package tinyr.ast;

import tinyr.Lowering;

/**
 */
public class Register extends Node {

    public final String registerName;

    public Register(String name) {
        this.registerName = name;
    }

    @Override
    public int lower(Lowering lowering) {
        return lowering.state.registerAliases.get(registerName);
    }

    @Override
    public String toString() {
        return super.toString() + "(" + registerName + ")";
    }
}
