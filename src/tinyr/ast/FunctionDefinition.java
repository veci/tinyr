package tinyr.ast;

import java.util.*;

import tinyr.Lowering;
import tinyr.bytecode.*;
import tinyr.bytecode.instructions.FDEF;
import tinyr.runtime.Closure;
import tinyr.runtime.Symbol;

/**
 */
public class FunctionDefinition extends Node {

    public final Node body;
    public final Vector<String> arguments;
    
    public FunctionDefinition(Node body) {
      this.arguments = null;
      this.body = body;
  }

    public FunctionDefinition(Vector<String> arguments, Node body) {
        this.arguments = arguments;
        this.body = body;
    }

    @Override
    public int lower(Lowering lowering) {
        // backup registers and instructions
        Lowering.State oldState = lowering.state;        
        Lowering.State state = new Lowering.State();        
        if (arguments != null){
          state.localArgs = new Symbol[arguments.size()];
          for (int i = 0; i < arguments.size(); i++){
              state.localArgs[i] = Symbol.make(arguments.get(i)); 
          }
          state.registers = arguments.size();
        } else {
          state.localArgs = null;
          state.registers = 0;
        }        
        
        // lower the function
        Code b = lowering.lower(body, state);
        // put registers and instructions back
        lowering.state = oldState;
        // create the FDEF code
        int reg = lowering.newRegister();        
        lowering.addInstruction(new FDEF(reg, new Closure(b.getView(), arguments == null ? 0 : arguments.size())));
        return reg;
    }

    @Override
    public String toString() {
      if (arguments == null)
        return super.toString() + "(" + body.toString() + ")";
      else
        return super.toString() + " " + arguments.toString() +  " (" + body.toString() + ")";
    }
}
