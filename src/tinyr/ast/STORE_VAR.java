package tinyr.ast;

import tinyr.Lowering;
import tinyr.runtime.Symbol;

/**
 */
public class STORE_VAR extends Node {

    public final Symbol name;
    public final Node sreg;

    public STORE_VAR(Symbol name, Node sreg) {
        this.name = name;
        this.sreg = sreg;
    }

    public STORE_VAR(String name, Node sreg) {
        this.name = Symbol.make(name);
        this.sreg = sreg;
    }

    @Override
    public int lower(Lowering lowering) {
        int s = sreg.lower(lowering);
        lowering.addInstruction(new tinyr.bytecode.instructions.STORE_VAR(name, s));
        return s;
    }

    @Override
    public String toString() {
        return super.toString() + "(" + name + ", " + sreg.toString() + ")";
    }
}
