package tinyr.ast;

import tinyr.Lowering;
import tinyr.runtime.Symbol;

public abstract class Assumption {

    public final Symbol name;
  
    protected Assumption(Symbol name) {
        this.name = name;
    }
  
    abstract public tinyr.runtime.Assumption lower(Lowering lowering);

    /** Assumption on a variable or register (function parameter) being a specific double number.
     */
    public static class IsConstDouble extends Assumption {
        final double value;

        public IsConstDouble(Symbol name, double value) {
            super(name);
            this.value = value;
        }
    
        @Override
        public tinyr.runtime.Assumption lower(Lowering lowering) {
            int regIndex = lowering.localArgumentRegister(name);
            if (regIndex == -1)
                return new tinyr.runtime.Assumption.Variable.IsConstDouble(name, value);
            else    
                return new tinyr.runtime.Assumption.Register.IsConstDouble(regIndex, value);
        }
    }
    
    /** Assumption on a variable or register (function parameter) being a general double number.
      */
    public static class IsDouble extends Assumption{
      
        public IsDouble(Symbol name) {
            super(name);
        }
      
        @Override
        public tinyr.runtime.Assumption lower(Lowering lowering) {
            int regIndex = lowering.localArgumentRegister(name);
            if (regIndex == -1)
                return new tinyr.runtime.Assumption.Variable.IsDouble(name);
            else
                return new tinyr.runtime.Assumption.Register.IsDouble(regIndex);
        }
    }
    
    /** Assumption on a variable or register (function parameter) being a general closure.
      */
    public static class IsClosure extends Assumption{
      
        public IsClosure(Symbol name){
            super(name);
        }
      
        @Override
        public tinyr.runtime.Assumption lower(Lowering lowering) {
            int regIndex = lowering.localArgumentRegister(name);
            if (regIndex == -1)
                return new tinyr.runtime.Assumption.Variable.IsClosure(name);
            else
                return new tinyr.runtime.Assumption.Register.IsClosure(regIndex);
        }
    }
    
    /** Assumption on a variable or register (function parameter) being a specific closure.
      */
    public static class IsConstClosure extends Assumption {
      
        public final Symbol closureName;
   
        public IsConstClosure(Symbol name, Symbol closureName) {
            super(name);
            this.closureName = closureName;
        }
      
        @Override
        public tinyr.runtime.Assumption lower(Lowering lowering) {
            int regIndex = lowering.localArgumentRegister(name);
            if (regIndex == -1)
                return new tinyr.runtime.Assumption.Variable.IsConstClosure(name, Lowering.definedClosures.get(closureName));
            else
                return new tinyr.runtime.Assumption.Register.IsConstClosure(regIndex, Lowering.definedClosures.get(closureName));
        }
    }
}
