package tinyr.ast;

import tinyr.Lowering;

/**
 */
public class USE extends Node {

    public final int id;

    public USE(int id) {
        this.id = id;
    }

    @Override
    public int lower(Lowering lowering) {
        lowering.addInstruction(new tinyr.bytecode.instructions.USE(lowering.getAssumption(id)));
        return -2;
    }
}
