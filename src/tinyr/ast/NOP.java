package tinyr.ast;

import tinyr.Lowering;

/**
 */
public class NOP extends Node {

    @Override
    public int lower(Lowering lowering) {
        lowering.addInstruction(new tinyr.bytecode.instructions.NOP());
        return -2;
    }
}
