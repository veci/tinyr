package tinyr.ast;

import tinyr.Lowering;
import tinyr.bytecode.instructions.LOAD_VAR;
import tinyr.runtime.Symbol;

public class Variable extends Node {

    public final Symbol name;

    public Variable(Symbol name) {
        this.name = name;
    }

    public Variable(String name) {
        this.name = Symbol.make(name);
    }

    @Override
    public int lower(Lowering lowering) {
        if (lowering.state.localArgs != null) {
            for (int i = 0; i < lowering.state.localArgs.length; i++) {
                if (lowering.state.localArgs[i] == name)
                    return i;                
            }
        }

        int register = lowering.newRegister();
        lowering.addInstruction(new LOAD_VAR(register, name));
        return register;
    }

    @Override
    public String toString() {
        return super.toString() + "(" + name + ")";
    }
}
