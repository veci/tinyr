package tinyr.ast;

import tinyr.Lowering;
import tinyr.bytecode.instructions.LOAD_CONST;
import tinyr.runtime.*;

/**
 */
public class Literal extends Node {
    public final Value value;

    public Literal(double value) {
        this.value = new Primitive(value);
    }

    public Literal(Value value) {
        this.value = value;
    }

    @Override
    public int lower(Lowering lowering) {
        int register = lowering.newRegister();
        lowering.addInstruction(new LOAD_CONST(register, value));
        return register;
    }

    @Override
    public String toString() {
        return super.toString() + "(" + value + ")";
    }
}
