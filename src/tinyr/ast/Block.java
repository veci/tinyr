package tinyr.ast;

import tinyr.Lowering;

import java.util.*;

/**
 */
public class Block extends Node {
    final Vector<Node> body = new Vector<>();


    public Block() {
    }

    /** Creates the code from a list of nodes. This is useful only for testing.
     */
    public Block(Node... nodes) {
        body.addAll(Arrays.asList(nodes));
    }

    public Block addNode(Node n) {
        body.add(n);
        return this;
    }

    public int size() {
        return body.size();
    }

    @Override
    public int lower(Lowering lowering) {
        int result = -2; // -2 this means null has to be loaded and returned
        for (Node n : body) {
            result = n.lower(lowering);
        }
        return result;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("(");
        for (Node n : body) {
            sb.append(n.toString());
            sb.append(", ");
        }
        sb.append(")");
        return sb.toString();
    }
}
