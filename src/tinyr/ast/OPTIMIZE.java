package tinyr.ast;

import tinyr.Lowering;
import tinyr.runtime.Symbol;

/**
 */
public class OPTIMIZE extends Node {

    public final Symbol name;

    public OPTIMIZE(Symbol name) {
        this.name = name;
    }

    @Override
    public int lower(Lowering lowering) {
        lowering.addInstruction(new tinyr.bytecode.instructions.OPTIMIZE(name));
        return -1;
    }
}
