package tinyr.analysis;

import tinyr.bytecode.*;
import tinyr.bytecode.instructions.*;
import tinyr.runtime.*;
import java.util.*;

/** The tracking analysis prototype class.
 *
 * For the tracking analysis, only forward type is supported as the assumptions themselves are capable of forward
 * propagating only.
 *
 * This is the basic object which defines the analysis principles and framework. It inherits the instruction visitor
 * pattern so all any analysis must do is to override the visiting methods for the instructions that change the
 * abstract state.
 *
 * To conserve memory, the analysis computes the states at fixpoints (beginnings of all blocks). An Optimizer subclass
 * is then used to replay the analysis within the basic blocks and perform any code changes.
 */
@SuppressWarnings("unchecked")
public class Analysis<V extends Value> extends Visitor {

    static class Pair<V extends Value> {
        public int pc;
        public State<V> state;

        public Pair(int pc, State<V> state) {
            this.pc = pc;
            this.state = state;
        }
    }

    /** Stack of the instructions to be analyzed. */
    Stack<Pair<V>> stack_ = new Stack<>();

    int pc;

    protected Code.View code;

    /**
     * When an instruction has more than one previous instruction, it is saved as a merge point together with a
     * clone of the state before its execution.
     */
    Object[] mergePoints_;

    /** Current state of the program. */
    protected State<V> state;

    /** Returns the pc of the current instruction.
     */
    protected int pc() {
        return pc;
    }

    /**
     * Pushes the given instruction on the stack for further processing. The current state is cloned and captured
     * with the instruction. Use this method to add an instruction that will not be executed immediately after the
     * currently executed instruction.
     */
    protected void push(int pc) {
        if ( pc >= code.size() ) return;
        try {
            stack_.push(new Pair(pc, state.clone()));
        } catch ( CloneNotSupportedException e ) {
            assert (false);
        }
    }

    /**
     * Pushes the given instruction on the stack, with the current state (not cloned). This is faster, but can only
     * be used if the instruction will be executed right after the current one.
     */
    protected void pushImmediate(int pc) {
        if ( pc >= code.size() ) return;
        stack_.push(new Pair<>(pc, state));
    }

    // Forward analysis ------------------------------------------------------------------------------------------------

    /**
     * Base class for a forward analysis. The forward analysis extends the visitor class so that each instruction can be
     * handled separately.
     */
    public static class Forward<V extends Value> extends Analysis<V> {

        public Forward(Code.View code) {
            this.code = code;
        }

        boolean ignoreAssumptions_;

        HashSet<Assumption> cbrAssumptions_;

        // indexes are Code.Block indexes
        HashSet<Assumption>[] activeCbrAssumptions_;

        /**
         * Performs the forward analysis on the given code represented by the root node. Initial state must be given to
         * the analysis as in Java templates we are not able to construct it ourselves. The analysis then progresses
         * using the visitor methods. Their implementation in the forward analysis class provides the forward walking
         * capabilities. The user should override them/add new to provide the state changing functionality accordingly.
         */
        private void analyze_(State<V> initialState) {
            try {
                assert (stack_.isEmpty());
                mergePoints_ = new Object[code.blocksCount()];
                mergePoints_[0] = initialState.clone();
                state = initialState;
                if ( code.size() > 0 )
                    stack_.push(new Pair<V>(0, state));
                while ( !stack_.empty() ) {
                    Pair<V> p = stack_.pop();
                    pc = p.pc;
                    state = p.state;
                    Instruction ins = code.get(pc);
                    // if the instruction is LABEL, we are entering a possible merge point
                    if (ins instanceof LABEL ) {
                        LABEL l = (LABEL) ins;
                        Code.Block b = code.block(l.address);
                        // update the cbr assumptions active at the beginning of the label
                        if (!ignoreAssumptions_)
                            activeCbrAssumptions_[b.index()] = (HashSet<Assumption>) cbrAssumptions_.clone();
                        // check whether a state has already been stored for a block: in that case, merge the previous
                        // state with the current state and make it the current state; otherwise, store the current state
                        if (mergePoints_[b.index()] != null) {
                            State<V> stored = (State<V>) mergePoints_[b.index()];
                            if (! stored.mergeFrom(state))
                                continue; // no need to explore the branch further as the incoming state has not changed
                            state = stored.clone();
                        } else {
                            mergePoints_[b.index()] = state.clone();
                        }
                    }
                    // ignore the assumptions if asked to - skip USE and ASM instructions which are the only means of
                    // introducing them
                    if (ignoreAssumptions_ && (ins instanceof ASM || ins instanceof USE)) {
                        pushImmediate(pc + 1);
                        continue;
                    }
                    // now we have the instruction and the state before its execution, all that is
                    // left is to perform the normal visit. The visit itself is responsible for adding
                    // the next instructions where appropriate.
                    ins.accept(this);
                }
            } catch ( CloneNotSupportedException e ) {
                assert (false);
            }
        }


        /** If the two values differ, the with assumptions value is made dependent on the active cbr assumptions and is
         * returned. Otherwise no changes are made.
         */
        private V updateAssumptions(V with, V without, HashSet<Assumption> activeCbr) {
            if ((with == null) && (without == null)) {
                // pass
            } else if (with == null) {
                with = (V) without.bottom();
                with.insertDependencies(activeCbr);
            } else if (without == null) {
                with.insertDependencies(activeCbr);
            } else if (! with.equals(without)) {
                with.insertDependencies(activeCbr);
            }
            return with;
        }

        /** Given two states, and a record of cbr active assumptions (list of assumptions that conditional branches not
         * taken up to the point were dependent on) checks the differences between the states of analysis with
         * assumptions and without them. If any value differs, the value from the run with assumptions is used, but is
         * marked dependent on the conditional branch assumptions seen so far.
         *
         * If the state with assumptions is null, it means that the block is Dead code and no updates are made.
         */
        private void updateAssumptions(State<V> with, State<V> without, HashSet<Assumption> activeCbr) {
            if (with == null)
                return;
            for (int i = 0; i < with.registers.length; ++i) {
              
                if (without == null) {
                  System.out.println("�OPS");
                }
                
                with.set(i, updateAssumptions(with.get(i), without.get(i), activeCbr));
            }
            // check - this is by no means efficient
            HashSet<Symbol> names = new HashSet<>();
            names.addAll(with.variables.keySet());
            names.addAll(without.variables.keySet());
            for (Symbol name : names) {
                V w = with.variables.getOrDefault(name, null);
                V wo = without.variables.getOrDefault(name, null);
                with.variables.put(name, updateAssumptions(w, wo, activeCbr));
            }
        }

        /** Analyzes the given code.
         *
         * First analyzes the code without any assumptions and then analyzes it with assumptions enabled. The two runs
         * are then compared and differences are tagged with the control flow assumptions
         * @param initialState
         */
        public void analyze(State<V> initialState) {
            try {
                // first analyze without assumptions and then with assumptions
                assert (stack_.isEmpty());
                ignoreAssumptions_ = true;
                analyze_(initialState.clone());
                Object[] withoutAsms = mergePoints_;
                State<V> withoutLast = state;
                ignoreAssumptions_ = false;
                cbrAssumptions_ = new HashSet<>();
                activeCbrAssumptions_ = new HashSet[code.blocksCount()];
                analyze_(initialState);
                // now compare the mergePoint states, use the assumed versions, but when their values differ, add the
                // already met control flow assumptions to the values.
                for (int i = 0; i < mergePoints_.length; ++i)
                    updateAssumptions((State<V>) mergePoints_[i], (State<V>) withoutAsms[i], activeCbrAssumptions_[i]);
                updateAssumptions(state, withoutLast, cbrAssumptions_);
            } catch ( CloneNotSupportedException e ) {
                assert (false);
            }
        }

        /** Default visit just moves the analysis to sequentially next instruction corresponding to a movement within
         * a basic block.
         */
        @Override
        public void visit(Instruction ins) {
            pushImmediate(pc + 1);
        }

        /** Branch instruction transfers control to the target label.
         */
        @Override
        public void visit(BR ins) {
            pushImmediate(code.block(ins.address).labelAddress());
        }

        /** By default CBR instruction transfers control to both its labels.
         */
        @Override
        public void visit(CBR ins) {
            visit(ins, true, true);
        }

        /** Given a CBR instruction and information whether its true and false case can be taken, generates the
         * transfers and updates the cbr assumptions set accordingly so that this information can be used for flagging
         * dependencies on the control flow optimizations in tracking mode.
         */
        public void visit(CBR ins, boolean takeTrue, boolean takeFalse) {
            if (takeTrue && takeFalse) {
                push(code.block(ins.addressFalse).labelAddress());
                pushImmediate(code.block(ins.addressTrue).labelAddress());
            } else {
                HashSet<Assumption> deps = state.get(ins.sreg).dependsOn();
                if (deps != null)
                    cbrAssumptions_.addAll(deps);
                if (takeTrue)
                    pushImmediate(code.block(ins.addressTrue).labelAddress());
                else
                    pushImmediate(code.block(ins.addressFalse).labelAddress());
            }
        }
    }
}