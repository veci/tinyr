package tinyr.analysis;

public class Pair<A,B> {
  public Pair( A f, B s ) {
    fst = f;
    snd = s;
  }

  public A fst;
  public B snd;

  public B getValue() {
    return snd;
  }

  public A getKey() {
    return fst;
  }
}