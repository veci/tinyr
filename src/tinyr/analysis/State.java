package tinyr.analysis;

import tinyr.runtime.Symbol;

import java.util.*;

/**
 */
@SuppressWarnings("unchecked")
public class State<V extends Value> implements Cloneable {

    public HashMap<Symbol, V> variables;
    public Object[] registers;

    public State(int numOfRegisters) {
        variables = new HashMap<>();
        registers = new Object[numOfRegisters];
    }

    /** Values can be null (bottom variable with no assumptions), or can be non-null objects with type bottom, in which
     * case they have some assumptions on them and these must be converted to the result value.
     */

    private final V merge(V into, V from, Bool changed) {
        try {
            if ( into == null && from == null )
                return null;
            if ( into == null ) {
                changed.value = true;
                return (V) from.clone();
            }
            if ( from == null ) {
                return (V) into.clone();
            }
            if ( into.isBottom() ) {
                V result = (V) from.clone();
                changed.value = into.insertDependencies(from) || changed.value;
                changed.value = !from.isBottom() || changed.value;
                into = result;
            } else if ( from.isBottom() ) {
                changed.value = into.insertDependencies(from) || changed.value;
            } else {
                changed.value = into.mergeFrom(from) || changed.value;
            }
        } catch ( CloneNotSupportedException e ) {
            assert (false);
        }
        return into;
    }

    public V get(int index) {
        return (V) registers[index];
    }

    public V get(Symbol name) {
        return variables.getOrDefault(name, null);
    }

    public void set(int index, V value) {
        registers[index] = value;
    }

    public void set(Symbol name, V value) {
        variables.put(name, value);
    }

    public void insertDependencies(Symbol symbol, V fromValue) {
        V item = get(symbol);
        if (item == null) {
            item = (V) fromValue.bottom();
            set(symbol, item);
        }
        item.insertDependencies(fromValue);
    }

    public void insertDependencies(int index, V fromValue) {
        V item = get(index);
        if (item == null) {
            item = (V) fromValue.bottom();
            set(index, item);
        }
        item.insertDependencies(fromValue);
    }
    
    /** Merges all information from the other state to itself. If any of the registers or variables have changed,
     * returns true.
     *
     * To make the code much smaller, we assume that null always means bottom value.
     */
    public boolean mergeFrom(State<V> other) {
        assert (registers.length == other.registers.length);
        Bool changed = new Bool(false);
        // first merge registers, this is easy
        for (int i = 0; i < registers.length; ++i) {
            V own = (V) registers[i];
            V theirs = (V) other.registers[i];
            registers[i] = merge(own, theirs, changed);
        }
        // now merge variables - this is by no means efficient
        HashSet<Symbol> names = new HashSet<>();
        names.addAll(variables.keySet());
        names.addAll(other.variables.keySet());
        for (Symbol name : names) {
            V own = variables.getOrDefault(name, null);
            V theirs = other.variables.getOrDefault(name, null);
            variables.put(name, merge(own, theirs, changed));
        }
        return changed.value;
    }

    @Override
    public State<V> clone() throws CloneNotSupportedException {
        State<V> result = (State<V>) super.clone();
        //result.registers = (V[]) Array.newInstance(registers.getClass(), registers.length);
        result.registers = new Object[registers.length];
        for (int i = 0; i < registers.length; ++i)
            result.registers[i] = registers[i] == null ? null : ((V) registers[i]).clone();
        result.variables = (HashMap<Symbol, V>) variables.clone();
        return result;
    }

    private static class Bool {
        boolean value;

        public Bool(boolean value) {
            this.value = value;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < registers.length; ++i)
            sb.append(String.format("%8s : %s\n", i, registers[i] == null ? "bottom" : registers[i]));
        for (Symbol name : variables.keySet())
            sb.append(String.format("%8s : %s\n", name, variables.get(name) == null ? "bottom" : variables.get(name)));
        return sb.toString();
    }


}
