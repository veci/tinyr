package tinyr.analysis;

import java.util.HashSet;
import java.util.Vector;

import tinyr.bytecode.Code;
import tinyr.bytecode.Instruction;
import tinyr.bytecode.Visitor;
import tinyr.bytecode.instructions.LABEL;
import tinyr.runtime.Assumption;
import tinyr.runtime.Closure;
import tinyr.runtime.Label;
import tinyr.runtime.Symbol;

/**
 * Given a analysis and its results, optimizes the code. Each optimizer extends
 * the visitor pattern for instructions and implements all the framework
 * required for tracking the analysis results in the already moving code view
 * and determining the patch dependency of the performed changes. What remains
 * to be done is to inherit this class for each optimization and implement the
 * overriding
 */
public class Optimizer<T extends Value> extends Visitor {

    Vector<Pair<Code.Patch, HashSet<Assumption>>> openedPatches = new Vector<>();

    /**
     * Incoming state to the current instruction.
     */
    State<T> state_;

    /**
     * Index of the block in the original code currently being visited by the
     * optimizer.
     */
    int block_;

    /**
     * Analysis object providing the information for the optimization.
     */
    final Analysis<T> analysis_;

    /**
     * Code being updated by the optimizer.
     */
    final Code.View code_;

    /**
     * The instructions that were analyzed (a snapshot of the code before any
     * patches have been applied).
     */
    final Instruction[] analyzedInsns_;

    /**
     * Keeps track of the pc numbers of the instructions in the original state
     * as the patches modify the code. This vector is always the size of the
     * current code's view and for each instruction in it contains its pc in the
     * original analyzed code, or -1 if the instruction is new.
     */
    final Vector<Integer> sourcePCs_;

    /**
     * PC to the original code. Only moves up.
     */
    int originalPc_;

    /**
     * PC to the current state of the code. Can move up & down.
     */
    int pc_;

    public Optimizer(Code.View code, Analysis<T> analysis) {
	this.analysis_ = analysis;
	this.code_ = code;
	this.analyzedInsns_ = code.insnsAsArray();
	this.sourcePCs_ = new Vector<>();
	for (int i = 0; i < analyzedInsns_.length; ++i)
	    sourcePCs_.add(i);
    }

    /**
     * Creates a snapshot of the code view and the replays the analysis calling
     * the optimizer visitors on the instructions so that they can be changed.
     * Keeps track of the relation of the analyzed snapshot to the updated code.
     */
    @SuppressWarnings("unchecked")
    public void optimize() {
	try {
	    block_ = 0;
	    state_ = ((State<T>) analysis_.mergePoints_[0]).clone();
	    originalPc_ = 0;
	    pc_ = 0;
	    for (Instruction ins : analyzedInsns_) {
		if (ins instanceof LABEL) {
		    ++block_;
		    state_ = (State<T>) analysis_.mergePoints_[block_];
		    if (state_ != null)
			state_ = state_.clone();
		    ins.accept(this); // so that labels are shown too
		} else {
		    if (state_ != null) {
			if (pc_ < 0) // if we are first instruction and already
				     // deleted, make sure
				     // that pc_ points now to a valid code
			    pc_ = 0;
			// check if the instruction we are at is still present
			// in the new code, and if
			// so, execute its
			// optimization via the visitor pattern
			while (sourcePCs_.get(pc_) < originalPc_)
			    ++pc_;
			if (sourcePCs_.get(pc_) == originalPc_) {
			    Instruction current = code_.get(pc_);
			    current.accept(this);
			}
			// execute the instruction using the analysis so that we
			// update the state
			// snapshots
			// only if it is not terminating
			if (!ins.isTerminating()) {
			    analysis_.state = state_;
			    analysis_.pc = originalPc_;
			    ins.accept(analysis_);
			    // no need to copy back, as analysis will only
			    // modify the state object in
			    // place
			}
		    }
		}
		++originalPc_;
	    }
	} catch (CloneNotSupportedException e) {
	    // pass
	    assert (false);
	}/* catch (UseInsertedException e) {
	    return;
	}*/

    }

    @Override
    public void visit(LABEL ins) {
	// pass
    }

    /**
     * Returns the pc of the current instruction. The pc is adjusted to the code
     * in its current state including all already done optimizations.
     */
    public int pc() {
	return pc_;
    }

    /**
     * Returns the pc in the original code corresponding to the currently
     * analyzed instruction.
     */
    public int originalPc() {
	return originalPc_;
    }

    /**
     * Returns the underlying analysis.
     */
    public Analysis<T> analysis() {
	return analysis_;
    }

    /**
     * Incoming state to the currently visited instruction.
     */
    protected State<T> state() {
	return state_;
    }

    /**
     * Removes the currently visited instruction as part of the specified patch.
     */
    protected void remove(Code.Patch p) {
	code_.remove(pc_, p);
	sourcePCs_.remove(pc_);
	--pc_;
    }

    /**
     * Inserts the given instruction before currently visited instruction as
     * part of the specified patch.
     */
    public Instruction insert(Instruction ins, Code.Patch p) {
	code_.insert(ins, pc_, p);
	sourcePCs_.insertElementAt(-1, pc_);
	++pc_;
	return ins;
    }

    public Code.Record insert(Code.Record r) {
	code_.insert(r, pc_);
	sourcePCs_.insertElementAt(-1, pc_);
	++pc_;
	return r;
    }

    /**
     * Inserts the given instruction at the specified pc as part of the patch p.
     */
    public Instruction insertAt(Instruction ins, int pc, Code.Patch p) {
	code_.insert(ins, pc, p);
	sourcePCs_.insertElementAt(-1, pc);
	if (pc <= pc_)
	    ++pc_;
	return ins;
    }

    public void removeAt(int pc, Code.Patch p) {
	sourcePCs_.remove(pc);
	code_.remove(pc, p);
	if (pc <= pc_)
	    --pc_;
    }

    /**
     * Removes the given block as part of the specified patch if the block is
     * not referenced from any other block. This method supports complex code
     * where eliminating one CBR instruction does not necessarily mean that the
     * block is not accessible from some other branch. It should not happen in
     * the structured code produced by the lowering, but might happen in the
     * presence of goto statements.
     */
    protected void removeBlock(Label label, Code.Patch patch) {
	Code.Block b = code_.block(label);
	while (b.length() != 0)
	    removeAt(b.codeStart(), patch);
	removeAt(b.labelAddress(), patch);
    }

    /**
     * Replaces the current instruction with the specified one as part of the
     * patch p.
     */
    protected void replace(Instruction ins, Code.Patch p) {
	assert (!(ins instanceof LABEL));
	Instruction old = code_.get(pc_);
	code_.remove(pc_, p);
	code_.insert(ins, pc_, p);
	// add the ignored assumptions of the old instruction to the new one
	ins.ignoresAssumptions(old.ignoredAssumptions());
    }

    /**
     * For a given value, returns an assumption that has to be used to use its
     * value, or null if no assumption use is to be emitted.
     */
    protected final Assumption isUsing(T value, Symbol variableName) {
	if (value != null && value.dependsOn() != null)
	    for (Assumption a : value.dependsOn()) {
		if (a instanceof tinyr.runtime.Assumption.Variable) {
		    if (((tinyr.runtime.Assumption.Variable) a).name != variableName)
			continue;
		}
		if (value.isDefinedBy(a))
		    return a;
	    }
	return null;
    }

    /**
     * For a given value of a register, returns an assumption that has to be
     * used to use its value, or null if no assumption use is to be emitted.
     */
    protected final Assumption isUsing(T value, int regIndex) {
	if (value != null && value.dependsOn() != null)
	    for (Assumption a : value.dependsOn()) {
		if (a instanceof tinyr.runtime.Assumption.Register) {
		    if (((tinyr.runtime.Assumption.Register) a).regIndex != regIndex)
			continue;
		}
		if (value.isDefinedBy(a))
		    return a;
	    }
	return null;
    }

    /**
     * Inserts the USE and INV check instructions before the actually optimized
     * instruction as part of the specified patch. The inserted code is
     * equivalent to this pseudo-code: if (asm is invalid) INV USE asm
     * 
     * @throws UseInsertedException
     */
    protected final void insertUSE(Assumption asm, Code.Patch patch) {
	// asm.outputInvalidationCheck( this, patch );
	// insert( new USE( asm ), patch );
	Closure.goodASMs.add(new javafx.util.Pair<Closure, Assumption>(code_
		.codeObject().closure, asm));
	// throw new UseInsertedException();
    }

    /**
     * Creates new register by incrementing the number of registers required for
     * the current code.
     */
    public int newRegister() {
	return code_.newRegister();
    }

    /**
     * Creates n new registers and returns the index of the first one.
     */
    public int newRegisters(int num) {
	return code_.newRegisters(num);
    }

    /**
     * Returns a patch associated with the specified set of assumptions. If no
     * such patch has yet been requested a new patch is created and opened in
     * the code manipulator.
     */
    @SuppressWarnings("unchecked")
    public Code.Patch getPatchFor(HashSet<Assumption> assumptions) {
	for (Pair<Code.Patch, HashSet<Assumption>> x : openedPatches)
	    if (assumptions == x.getValue()
		    || (x.getValue() != null && x.getValue()
			    .equals(assumptions)))
		return x.getKey();
	Code.Patch p = code_.openNewPatch();
	// mark the patch to be dependent on the given assumptions
	if (assumptions != null) {
	    for (Assumption a : assumptions)
		p.dependsOnAssumption(a);
	    assumptions = (HashSet<Assumption>) assumptions.clone();
	}
	// add the patch to opened patches
	openedPatches.add(new Pair<>(p, assumptions));
	return p;
    }

    public Code.Patch getPatchFor(HashSet<Assumption> a1, HashSet<Assumption> a2) {
	if (a1 == null || a1.isEmpty())
	    return getPatchFor(a2);
	if (a2 == null || a2.isEmpty())
	    return getPatchFor(a1);
	HashSet<Assumption> a = new HashSet<>();
	a.addAll(a1);
	a.addAll(a2);
	return getPatchFor(a);
    }

    public Code.Patch getPatchFor(Assumption assumption) {
	HashSet<Assumption> a = new HashSet<>();
	a.add(assumption);
	return getPatchFor(a);
    }

    public Code.Patch getDefaultPatch() {
	return getPatchFor(new HashSet<Assumption>());
    }

    public boolean closeAllPatches() {
	openedPatches.clear();
	return code_.closeAll();
    }

    public Code.View code() {
	return code_;
    }

}
