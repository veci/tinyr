package tinyr.analysis;

import tinyr.runtime.*;

import java.util.*;

/** Tracking value prototype
 */
@SuppressWarnings("unchecked")
public abstract class Value implements Cloneable {

    HashSet<Assumption> dependsOn;

    /** Returns a collection of assumptions the value depends on.
     *
     * If the value is statically independent, null is returned.
     */
    public HashSet<Assumption> dependsOn() {
        return dependsOn;
    }

    /** Returns true if after adding the dependencies the Value other depends on, the
     * current collection of assumptions the value depends on has changed.
     */
    public boolean insertDependencies(Value other) {
        if (other != null && other.dependsOn != null) {
            if (dependsOn == null)
                dependsOn = new HashSet<>();
            int oldSize = dependsOn.size();
            dependsOn.addAll(other.dependsOn);
            return oldSize != dependsOn.size();
        } else {
            return false;
        }
    }

    /** Returns true if after merging with another collection of assumptions , the current
     * collection of assumptions the value depends on has changed.
     */    
    public boolean insertDependencies(Collection<Assumption> other) {
        if (other != null) {
            if (dependsOn == null)
                dependsOn = new HashSet<>();
            int oldSize = dependsOn.size();
            dependsOn.addAll(other);
            return oldSize != dependsOn.size();
        } else {
            return false;
        }
    }

    public void insertDependency(Assumption a) {
        if (dependsOn == null)
            dependsOn = new HashSet<>();
        dependsOn.add(a);
    }

    public void removeDependency(Assumption a) {
        if (dependsOn != null) {
            dependsOn.remove(a);
            if ( dependsOn.isEmpty() )
                dependsOn = null;
        }
    }

    public abstract Value top();

    public abstract boolean isTop();

    public abstract Value bottom();

    public abstract boolean isBottom();

    public abstract boolean equals(Value other);

    /** Returns true, if the value is defined by the assumption. This means if the abstract value of the assumption
     * is the same as the current abstract value itself.
     */
    public abstract boolean isDefinedBy(Assumption a);

    /** Merges the value from the other value into this one. Returns true if the value has changed after the merge, false
     * if the value stays the same.
     */
    public boolean mergeFrom(Value other) {
        return insertDependencies(other);
/*        assert (other != null);
        if (other.dependsOn == null)
            return false;
        if (dependsOn == null) {
            dependsOn = (HashSet<Assumption>) other.dependsOn.clone();
            return true;
        } else {
            int oldSize = dependsOn.size();
            dependsOn.addAll(other.dependsOn);
            return dependsOn.size() != oldSize;
        } */
    }

    /** Clones the value.
     */
    @Override
    public Value clone() throws CloneNotSupportedException {
        Value c = (Value) super.clone();
        if (dependsOn != null)
            c.dependsOn = (HashSet<Assumption>) dependsOn.clone();
        return c;
    }

    @Override
    public String toString() {
        if (dependsOn == null)
            return "";
        StringBuilder sb = new StringBuilder(" [");
        for (Assumption as: dependsOn)
            sb.append(as.toString() + ", ");
        sb.append("]");
        return sb.toString();
    }
}
