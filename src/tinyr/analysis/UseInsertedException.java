package tinyr.analysis;
/**
 * Thrown when USE instruction inserted to the code
 * 
 */
public class UseInsertedException extends RuntimeException {   
}
