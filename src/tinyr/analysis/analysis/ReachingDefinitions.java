package tinyr.analysis.analysis;

import tinyr.analysis.*;
import tinyr.bytecode.*;
import tinyr.bytecode.instructions.CALL;
import tinyr.runtime.*;

import java.util.HashSet;

/** Reaching definitions analysis.
 *
 */
public class ReachingDefinitions extends Analysis.Forward<tinyr.analysis.analysis.ReachingDefinitions.Value> {

    public static ReachingDefinitions analyze(Code.View code) {
        ReachingDefinitions a = new ReachingDefinitions(code);
        a.analyze(new State<Value>(code.numOfRegisters()));
        return a;
    }

    private ReachingDefinitions(Code.View code) {
        super(code);
    }

    @Override
    public void visit(Instruction.O ins) {
        state.set(ins.dreg, new Value(pc()));
        super.visit(ins);
    }

    @Override
    public void visit(Instruction.OI ins) {
        state.set(ins.dreg, new Value(pc()));
        super.visit(ins);
    }

    @Override
    public void visit(Instruction.OII ins) {
        state.set(ins.dreg, new Value(pc()));
        super.visit(ins);
    }

    @Override
    public void visit(Instruction.OIIArray ins) {
        state.set(ins.dreg, new Value(pc()));
        super.visit(ins);
    }

    @Override
    public void visit(Instruction.ORIArray ins) {
        state.set(ins.dreg, new Value(pc()));
        super.visit(ins);
    }

    @Override
    public void visit(Instruction.OR ins) {
        if (state.get(ins.name) == null)
            state.set(ins.name, new Value(-1)); // reading a variable that we do not know where it was initialized
        state.set(ins.dreg, new Value(pc()));
        super.visit(ins);
    }

    @Override
    public void visit(Instruction.WI ins) {
        state.set(ins.name, new Value(pc()));
        super.visit(ins);
    }

    @Override
    public void visit(Instruction.OC ins) {
        state.set(ins.dreg, new Value(pc()));
        super.visit(ins);
    }

    @Override
    public void visit(Instruction.OImm ins) {
        state.set(ins.dreg, new Value(pc()));
        super.visit(ins);
    }

    @Override
    public void visit(CALL ins) {
        // mark all variable reaching definitions as unknown
        for (Symbol name : state.variables.keySet())
            state.set(name, new Value(-1));
        state.set(ins.dreg, new Value(pc()));
        super.visit(ins);
    }

    /** Value for reaching definitions. For each register, or variable contains a set of program counters at which the
     * given register or variable have been visibly defined.
     *
     * PC value of -1 means that the reaching definition cannot be determined and no pc value for an item means that the
     * value is undefined.
     *
     * By default all registers should start undefined and all variables should start at -1. Any function call sets all
     * known variables to -1.
     */
    public static class Value extends tinyr.analysis.Value {

        public HashSet<Integer> definitions = new HashSet<>();

        public Value() {
        }

        public Value(int pc) {
            definitions.add(pc);
        }

        @Override
        public boolean mergeFrom(tinyr.analysis.Value v) {
            boolean changed = false;
            int oldSize = definitions.size();
            definitions.addAll(((tinyr.analysis.analysis.ReachingDefinitions.Value) v).definitions);
            if (definitions.size() != oldSize)
                changed = true;
            return super.mergeFrom(v) || changed;
        }

        @Override
        public tinyr.analysis.Value top() {
            return new Value(-1);
        }

        @Override
        public boolean isTop() {
            return definitions.contains(-1);
        }

        @Override
        public tinyr.analysis.Value bottom() {
            return new Value();
        }

        @Override
        public boolean isBottom() {
            return definitions.size() == 0;
        }

        @Override
        public boolean equals(tinyr.analysis.Value other) {
            return other instanceof Value && ((Value) other).definitions.equals(definitions);
        }

        @Override
        public boolean isDefinedBy(Assumption a) {
            // reaching definition values are never defined by assumptions
            return false;
        }

        @Override
        public String toString() {
            if (definitions.size() == 0)
                return "undefined";
            StringBuilder sb = new StringBuilder();
            for (int i : definitions)
                sb.append(i+" ");
            return sb.toString();

        }
    }
}
