package tinyr.analysis.analysis;

import tinyr.analysis.*;
import tinyr.analysis.analysis.ConstantPropagation.Value.Type;
import tinyr.bytecode.*;
import tinyr.bytecode.instructions.*;
import tinyr.runtime.*;

/**
 */
public class ConstantPropagation extends Analysis.Forward<ConstantPropagation.Value> {

    ConstantPropagation(Code.View code) {
        super(code);
    }

    private static final double UNUSED_REGISTER = Double.MIN_VALUE;

    public static ConstantPropagation analyze(Code.View code, int arguments) {
        ConstantPropagation p = new ConstantPropagation(code);
        State<Value> s = new State<>(code.numOfRegisters());
        for (int i = 0; i < arguments; ++i)
            s.set(i, new Value(Value.Type.Top));
        for (int i = arguments; i<code.numOfRegisters();i++)
            s.set(i, new Value(UNUSED_REGISTER));
        p.analyze(s);
        //System.out.println(p.state);
        return p;
    }

    private static enum BOP {
        ADD,
        EQ,
        GT,
        GTE,
        LT,
        LTE,
        NEQ,
        SUB,
        MUL,
        DIV,
        AND,
        OR
    }


    private void calculateBOP(Instruction.OII ins, BOP bop) {
        Value l = state.get(ins.sreg);
        Value r = state.get(ins.sreg2);
        Value result = null;
        if (l != null && r != null && l.isConst() && r.isConst()) {
            switch (bop) {
                case AND:
                    result = new Value((l.doubleValue != 0 && r.doubleValue != 0) ? 1 : 0);
                    break;
                case OR:
                    result = new Value((l.doubleValue != 0 || r.doubleValue != 0) ? 1 : 0);
                    break;
                case ADD:
                    result = new Value(l.doubleValue + r.doubleValue);
                    break;
                case MUL:
                    result = new Value(l.doubleValue * r.doubleValue);
                    break;
                case DIV:
                    result = new Value(l.doubleValue / r.doubleValue);
                    break;
                case GT:
                    result = new Value(l.doubleValue > r.doubleValue ? 1 : 0);
                    break;
                case GTE:
                    result = new Value(l.doubleValue >= r.doubleValue ? 1 : 0);
                    break;
                case LT:
                    result = new Value(l.doubleValue < r.doubleValue ? 1 : 0);
                    break;
                case LTE:
                    result = new Value(l.doubleValue <= r.doubleValue ? 1 : 0);
                    break;
                case SUB:
                    result = new Value(l.doubleValue - r.doubleValue);
                    break;
                default:
                    assert(false);
            }
        } else {
            result = new Value(Value.Type.AnyDouble);
        }
        result.insertDependencies(l);
        result.insertDependencies(r);
        state.set(ins.dreg, result);
    }

    private void calculateEqualityBOP(Instruction.OII ins, BOP bop) {
        Value l = state.get(ins.sreg);
        Value r = state.get(ins.sreg2);
        Value result = null;
        if (l == null || r == null || l.isBottom() || r.isBottom()) {
            result = new Value(Value.Type.AnyDouble);
        } else if (l.isDouble() && r.isDouble()) {
            if (l.isConst() && r.isConst()) {
                switch (bop) {
                    case EQ:
                        result = new Value(l.doubleValue == r.doubleValue ? 1 : 0);
                        break;
                    case NEQ:
                        result = new Value(l.doubleValue != r.doubleValue ? 1 : 0);
                        break;
                    default:
                        assert(false);
                }
            } else {
                result = new Value(Value.Type.AnyDouble);
            }
        } else if (l.isClosure() && r.isClosure()) {
            if (l.isConst() && r.isConst()) {
                switch (bop) {
                    case EQ:
                        result = new Value(l.closureValue == r.closureValue ? 1 : 0);
                        break;
                    case NEQ:
                        result = new Value(l.closureValue != r.closureValue ? 1 : 0);
                        break;
                    default:
                        assert(false);
                }
            } else {
                result = new Value(Value.Type.AnyDouble);
            }
        } else if (l.isTop() || r.isTop()) {
            result = new Value(Value.Type.AnyDouble);
        } else {
            result = new Value(0);
        }
        result.insertDependencies(l);
        result.insertDependencies(r);
        state.set(ins.dreg, result);

    }

    @Override
    public void visit(ADD ins) {
        calculateBOP(ins, BOP.ADD);
        super.visit(ins);
    }

    @Override
    public void visit(AND ins) {
        calculateBOP(ins, BOP.AND);
        super.visit(ins);
    }

    @Override
    public void visit(MUL ins) {
        calculateBOP(ins, BOP.MUL);
        super.visit(ins);
    }

    @Override
    public void visit(DIV ins) {
        calculateBOP(ins, BOP.DIV);
        super.visit(ins);
    }

    @Override
    public void visit(ARRAY_CREATE ins) {
        state.set(ins.dreg, new Value(Value.Type.AnyArray));
        super.visit(ins);
    }

    @Override
    public void visit(ARRAY_GET ins) {
        state.set(ins.dreg, new Value(Value.Type.AnyDouble));
        super.visit(ins);
    }

    @Override
    public void visit(ARRAY_LENGTH ins) {
        state.set(ins.dreg, new Value(Value.Type.AnyDouble));
        super.visit(ins);
    }

    @Override
    public void visit(ARRAY_SET ins) {
        // does not change any values we track at the moment
        super.visit(ins);
    }

    /** After a function call, all variables are set to Top.
     */
    @Override
    public void visit(CALL ins) {
        for ( Symbol name : state.variables.keySet() ) {
            state.set(name, new Value(Value.Type.Top));
        }
        state.set(ins.dreg, new Value(Value.Type.Top));
        super.visit(ins);
    }

    @Override
    public void visit(CBR ins) {
        Value v = state.get(ins.sreg);
        if (v.isConst()) {
            if (!ins.ignoresAssumptions(v.dependsOn())) {
                if (v.doubleValue == 0)
                    visit(ins, false, true);
                else
                    visit(ins, true, false);
                return;
            }
        }
        super.visit(ins);
    }

    @Override
    public void visit(EQ ins) {
        calculateEqualityBOP(ins, BOP.EQ);
        super.visit(ins);
    }

    @Override
    public void visit(FDEF ins) {
        state.set(ins.dreg, new Value(ins.closure));
        super.visit(ins);
    }

    @Override
    public void visit(GT ins) {
        calculateBOP(ins, BOP.GT);
        super.visit(ins);
    }

    @Override
    public void visit(GTE ins) {
        calculateBOP(ins, BOP.GTE);
        super.visit(ins);
    }

    /** After intrinsic call, all values are set to top.
     *
     * This may perhaps change only to inputs and outputs of the intrinsic.
     */
    @Override
    public void visit(INTRINSIC ins) {
        for ( Symbol name : state.variables.keySet() ) {
            state.set(name, new Value(Value.Type.Top));
        }
        state.set(ins.dreg, new Value(Value.Type.Top));
        super.visit(ins);
    }


    @Override
    public void visit(IS_ARRAY ins) {
        Value v = state.get(ins.sreg);
        assert (v != null);
        Value result;
        if (v.isArray()) {
            result = new Value(1);
        } else if (v.isTop()) {
            result = new Value(Value.Type.AnyDouble);
        } else {
            result = new Value(0);
        }
        result.insertDependencies(v);
        state.set(ins.dreg, result);
        super.visit(ins);
    }

    @Override
    public void visit(IS_CLOSURE ins) {
        Value v = state.get(ins.sreg);
        assert (v != null);
        Value result;
        if (v.isClosure()) {
            result = new Value(1);
        } else if (v.isTop()) {
            result = new Value(Value.Type.AnyDouble);
        } else {
            result = new Value(0);
        }
        result.insertDependencies(v);
        state.set(ins.dreg, result);
        super.visit(ins);
    }

    @Override
    public void visit(IS_DOUBLE ins) {
        Value v = state.get(ins.sreg);
        assert (v != null);
        Value result;
        if (v.isDouble()) {
            result = new Value(1);
        } else if (v.isTop()) {
            result = new Value(Value.Type.AnyDouble);
        } else {
            result = new Value(0);
        }
        result.insertDependencies(v);
        state.set(ins.dreg, result);
        super.visit(ins);
    }

    @Override
    public void visit(LOAD_CONST ins) {
        if (ins.value instanceof Primitive)
            state.set(ins.dreg, new Value(((Primitive) ins.value).value));
        else if (ins.value instanceof Array)
            state.set(ins.dreg, new Value(Value.Type.AnyArray));
        else
            state.set(ins.dreg, new Value((Closure) ins.value));
        super.visit(ins);
    }

    @Override
    public void visit(LOAD_REG ins) {
        try {
            Value v = state.get(ins.sreg);
            assert (v != null);
            state.set(ins.dreg, (Value) v.clone());
            super.visit(ins);
        } catch (CloneNotSupportedException e) {
            assert (false);
        }
    }

    @Override
    public void visit(LOAD_VAR ins) {
        try {
            Value v = state.get(ins.name);
            if (v == null) // if variable is not found, it is Top
                state.set(ins.dreg, new Value(Value.Type.Top));
            else
                state.set(ins.dreg, (Value) v.clone());
            super.visit(ins);
        } catch ( CloneNotSupportedException e ) {
            assert (false);
        }
    }

    @Override
    public void visit(LT ins) {
        calculateBOP(ins, BOP.LT);
        super.visit(ins);
    }

    @Override
    public void visit(LTE ins) {
        calculateBOP(ins, BOP.LTE);
        super.visit(ins);
    }

    @Override
    public void visit(NEQ ins) {
        calculateEqualityBOP(ins, BOP.NEQ);
        super.visit(ins);
    }

    @Override
    public void visit(OR ins) {
        calculateBOP(ins, BOP.OR);
        super.visit(ins);
    }

    @Override
    public void visit(RANDOM ins) {
        // we do not support ranges
        state.set(ins.dreg, new Value(Value.Type.AnyDouble));
        super.visit(ins);
    }

    @Override
    public void visit(STORE_VAR ins) {
        try {
            Value v = state.get(ins.sreg);
            if (v == null)
                System.out.println("ERROR");
            assert (v != null): "ouch";
            state.set(ins.name, (Value) v.clone());
            super.visit(ins);
        } catch (CloneNotSupportedException e) {
            assert (false);
        }
    }

    @Override
    public void visit(SUB ins) {
        calculateBOP(ins, BOP.SUB);
        super.visit(ins);
    }


    /** For the CP analysis purposes, ASM and USE behave the same way.
     */
    @Override
    public void visit(ASM ins) {
        insertAssumption(ins.assumption);
        super.visit(ins);
    }

    /** USE removes the dependency from the value specified by the assumption that is being used.
     */
    @Override
    public void visit(USE ins) {
        Value v;
        if (ins.assumption instanceof Assumption.Variable)
            v = state.get(((Assumption.Variable) ins.assumption).name);
        else
            v = state.get(((Assumption.Register) ins.assumption).regIndex);
        v.removeDependency(ins.assumption);
        super.visit(ins);
    }

    /**
     * updates the state by inserting a value dependent on an assumption for a variable or register
     * only when Assumption gives us more specific state 
     */
    private void insertAssumption(Assumption assumption) {
        Value before = null;
        if (assumption instanceof Assumption.Register)
            before = state.get( ((Assumption.Register)assumption).regIndex );
        else if (assumption instanceof Assumption.Variable)
            before = state.get( ((Assumption.Variable)assumption).name );

        Value result = null;
        if (assumption instanceof Assumption.Variable.IsConstDouble) {
            if (before.type == Type.Bottom || before.type == Type.AnyDouble || before.type == Type.Top)
                result = new Value(((Assumption.Variable.IsConstDouble) assumption).value);
        }
        else if (assumption instanceof Assumption.Register.IsConstDouble) {
            if (before.type == Type.Bottom || before.type == Type.AnyDouble || before.type == Type.Top)
                result = new Value(((Assumption.Register.IsConstDouble) assumption).value);
        }
        else if (assumption instanceof Assumption.Variable.IsDouble || assumption instanceof Assumption.Register.IsDouble) {
            if (before.type == Type.Bottom || before.type == Type.Top)
                result = new Value(Value.Type.AnyDouble);
        }
        else if (assumption instanceof Assumption.Variable.IsConstClosure) {
            if (before.type == Type.Bottom || before.type == Type.AnyClosure || before.type == Type.Top)
                result = new Value(((Assumption.Variable.IsConstClosure) assumption).value);
        }
        else if (assumption instanceof Assumption.Register.IsConstClosure) {
            if (before.type == Type.Bottom || before.type == Type.AnyClosure || before.type == Type.Top)
                result = new Value(((Assumption.Register.IsConstClosure) assumption).value);
        }
        else if (assumption instanceof Assumption.Variable.IsClosure || assumption instanceof Assumption.Register.IsClosure) {
            if (before.type == Type.Bottom || before.type == Type.Top)
                result = new Value(Value.Type.AnyClosure);
        }
        else
            assert (false) : " < and > assumptions not yet supported";

        if (result != null) {
            result.insertDependency(assumption);
            if (assumption instanceof tinyr.runtime.Assumption.Variable) {
                state.set( ((tinyr.runtime.Assumption.Variable) assumption).name, result);
            }
            else if (assumption instanceof tinyr.runtime.Assumption.Register) {
                state.set( ((tinyr.runtime.Assumption.Register) assumption).regIndex, result);
            }
        }
    }

    public static class Value extends tinyr.analysis.Value {

        public enum Type {
            Top,
            AnyDouble,
            Double,
            AnyClosure,
            Closure,
            AnyArray,
            Bottom
        }

        Type type;
        public double doubleValue;
        public Closure closureValue;

        public Value(Type type) {
            this.type = type;
        }

        public Value(double value) {
            this.type = Type.Double;
            this.doubleValue = value;
        }

        public Value(Closure value) {
            this.type = Type.Closure;
            this.closureValue = value;
        }

        @Override
        public Value top() {
            return new Value(Type.Top);
        }

        @Override
        public boolean isTop() {
            return type == Type.Top;
        }

        @Override
        public Value bottom() {
            return new Value(Type.Bottom);
        }

        @Override
        public boolean isBottom() {
            return type == Type.Bottom;
        }

        @Override
        public boolean equals(tinyr.analysis.Value other) {
            Value v = (Value) other;
            if (type != v.type)
                return false;
            if ((type == Type.Double) && (doubleValue != v.doubleValue))
                return false;
            if ((type == Type.Closure) && (closureValue != v.closureValue))
                return false;
            if (dependsOn() == null && v.dependsOn() == null)
                return true;
            if (dependsOn() == null || v. dependsOn() == null)
                return false;
            return dependsOn().equals(v.dependsOn());
        }

        @Override
        public boolean isDefinedBy(Assumption a) {
            if (a instanceof Assumption.Variable.IsConstDouble)
                return (type == Type.Double) && doubleValue == ((Assumption.Variable.IsConstDouble) a).value;
            if (a instanceof Assumption.Register.IsConstDouble)
              return (type == Type.Double) && doubleValue == ((Assumption.Register.IsConstDouble) a).value;
            if (a instanceof Assumption.Variable.IsDouble || a instanceof Assumption.Register.IsDouble)
                return (type == Type.AnyDouble);
            if (a instanceof Assumption.Variable.IsClosure || a instanceof Assumption.Register.IsClosure)
                return (type == Type.AnyClosure);
            if (a instanceof Assumption.Variable.IsConstClosure)
                return (type == Type.Closure) && closureValue == ((Assumption.Variable.IsConstClosure) a).value;
            if (a instanceof Assumption.Register.IsConstClosure)
              return (type == Type.Closure) && closureValue == ((Assumption.Register.IsConstClosure) a).value;
            throw new RuntimeException("Other assumption types are not implemented");
        }

        public boolean isDouble() {
            return type == Type.Double || type == Type.AnyDouble;
        }

        public boolean isClosure() {
            return type == Type.Closure || type == Type.AnyClosure;
        }

        public boolean isConst() {
            return type == Type.Double || type == Type.Closure;
        }

        public boolean isArray() {
            return type == Type.AnyArray;
        }

        @Override
        public boolean mergeFrom(tinyr.analysis.Value other) {
            Value v2 = (Value) other;
            boolean result = false;
            switch (type) {
                case Top:
                    break; // nothing to do for Top type
                case AnyDouble:
                    if ((v2.type == Type.AnyDouble) || v2.type == Type.Double)
                        break;
                    result = true;
                    type = Type.Top;
                    break;
                case AnyClosure:
                    if ((v2.type == Type.AnyClosure) || v2.type == Type.Closure)
                        break;
                    result = true;
                    type = Type.Top;
                    break;
                case Double:
                    if (v2.type == Type.Double) {
                        if (doubleValue == v2.doubleValue)
                            break;
                        type = Type.AnyDouble;
                    } else if (v2.type == Type.AnyDouble) {
                        type = Type.AnyDouble;
                    } else {
                        type = Type.Top;
                    }
                    result = true;
                    break;
                case Closure:
                    if (v2.type == Type.Closure) {
                        if (closureValue == v2.closureValue)
                            break;
                        type = Type.AnyClosure;
                    } else if (v2.type == Type.AnyClosure) {
                        type = Type.AnyClosure;
                    } else {
                        type = Type.Top;
                    }
                    result = true;
                    break;
                case AnyArray:
                    if (v2.type == Type.AnyArray)
                        break;
                    type = Type.Top;
                    result = true;
                    break;
            }
            return super.mergeFrom(other) || result;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            switch (type) {
                case Double:
                    sb.append(doubleValue);
                    break;
                case AnyDouble:
                    sb.append("double");
                    break;
                case Closure:
                    sb.append("closure " + closureValue.hashCode());
                    break;
                case AnyClosure:
                    sb.append("closure");
                    break;
                case Top:
                    sb.append("Top");
                    break;
                case Bottom:
                    sb.append("bottom");
                    break;
                case AnyArray:
                    sb.append("array");
                    break;
                default:
                    sb.append("ERROR");

            }
            sb.append(super.toString());
            return sb.toString();
        }

    }

}
