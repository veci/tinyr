package tinyr.analysis.analysis;

import tinyr.analysis.*;
import tinyr.bytecode.*;
import tinyr.bytecode.instructions.*;
import tinyr.runtime.*;

import java.util.HashSet;

public class Liveness extends Analysis.Forward<Liveness.Value> {

    // store a boolean value for every instruction in Code.View
    final boolean[] alive;

    Liveness(Code.View code) {
        super(code);
        alive = new boolean[code.size()];
    }

    public static Liveness analyze(Code.View code) {
        Liveness a = new Liveness(code);
        a.analyze(new State<Value>(code.numOfRegisters()));
        //for (int i = 0; i < a.alive.length; ++i)
            //System.out.println((a.alive[i] ? "   " : " - ") + code.get(i));
        return a;
    }

    public boolean isAlive(int pc) {
        return alive[pc];
    }

    private void setAlive(int register) {
        setAlive(state.get(register));
    }

    private void setAlive(Symbol name) {
        setAlive(state.get(name));
    }
   
    // when we read from a register or a variable, we mark as alive the last write to it; note that due to
    // merge points we might have multiple instructions to mark
    private void setAlive(Value v) {
        if (v != null)
            for (int pc : v.writes)
                alive[pc] = true;
    }

    // every time we write to a register, we have a new HashSet writes containing only the current PC
    private void reset(int register) {
        state.set(register, new Value(pc()));
    }

    private void reset(Symbol name) {
        state.set(name, new Value(pc()));
    }

    @Override
    public void visit(Instruction.OII ins) {
        setAlive(ins.sreg);
        setAlive(ins.sreg2);
        reset(ins.dreg);
        super.visit(ins);
    }

    @Override
    public void visit(Instruction.OI ins) {
        setAlive(ins.sreg);
        reset(ins.dreg);
        super.visit(ins);
    }

    @Override
    public void visit(ARRAY_SET ins) {
        setAlive(ins.dreg); // we read from the array value too here
        setAlive(ins.sreg);
        setAlive(ins.sreg2);
        reset(ins.dreg);
        super.visit(ins);
    }

    @Override
    public void visit(ASM ins) {
        super.visit(ins);
    }

    @Override
    public void visit(BKPT ins) {
        super.visit(ins);
    }

    @Override
    public void visit(BR ins) {
        super.visit(ins);
    }

    @Override
    public void visit(CALL ins) {
        setAlive(ins.sreg);
        for (int i : ins.sregs)
            setAlive(i);
        reset(ins.dreg);
        super.visit(ins);
    }

    @Override
    public void visit(CBR ins) {
        setAlive(ins.sreg);
        super.visit(ins);
    }

    @Override
    public void visit(FDEF ins) {
        reset(ins.dreg);
        super.visit(ins);
    }

    @Override
    public void visit(INTRINSIC ins) {
        for (int i : ins.sregs)
            setAlive(i);
        reset(ins.dreg);
        super.visit(ins);
    }

    @Override
    public void visit(INV ins) {
        super.visit(ins);
    }

    @Override
    public void visit(LOAD_CONST ins) {
        reset(ins.dreg);
        super.visit(ins);
    }

    @Override
    public void visit(LOAD_VAR ins) {
        setAlive(ins.name);
        reset(ins.dreg);
        super.visit(ins);
    }

    @Override
    public void visit(NOP ins) {
        super.visit(ins);
    }

    @Override
    public void visit(RET ins) {
        setAlive(ins.sreg);
        // super.visit(ins);
    }

    @Override
    public void visit(STORE_VAR ins) {
        setAlive(ins.sreg);
        reset(ins.name);
        super.visit(ins);
    }

    @Override
    public void visit(USE ins) {
        super.visit(ins);
    }

    // =================================================================================================================

    public static class Value extends tinyr.analysis.Value {

        // PCs (i.e. instructions) that performed the last write on the register or variable containing this value
        HashSet<Integer> writes = new HashSet<>();

        protected Value() {
        }

        public Value(int writePc) {
            writes.add(writePc);
        }

        @Override
        public tinyr.analysis.Value top() {
            return null;
        }

        @Override
        public boolean isTop() {
            return false;
        }

        @Override
        public tinyr.analysis.Value bottom() {
            return new Value();
        }

        @Override
        public boolean isBottom() {
            return writes.isEmpty();
        }

        @Override
        public boolean equals(tinyr.analysis.Value other) {
            return false;
        }

        @Override
        public boolean isDefinedBy(Assumption a) {
            // this is always false at the moment as we have no assumptions that the liveness analysis might benefit from
            return false;
        }

        @Override
        public boolean mergeFrom(tinyr.analysis.Value other) {
            int wSize = writes.size();
            writes.addAll(((Value) other).writes);
            return super.mergeFrom(other) || writes.size() != wSize;
        }

        @Override
        public String toString() {
            return writes.toString();
        }

        @Override
        public Value clone() {
            Value result = new Value();
            result.mergeFrom(this);
            return result;

        }

    }
}

