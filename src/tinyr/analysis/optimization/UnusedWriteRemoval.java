package tinyr.analysis.optimization;

import tinyr.analysis.*;
import tinyr.analysis.analysis.Liveness;
import tinyr.bytecode.Code;
import tinyr.bytecode.instructions.*;

/**
 */
public class UnusedWriteRemoval extends Optimizer<Liveness.Value> {

    public UnusedWriteRemoval(Code.View code, Analysis<Liveness.Value> analysis) {
        super(code, analysis);
    }

    public static boolean optimize(Code.View code, Analysis<Liveness.Value> analysis) {
        UnusedWriteRemoval instance = new UnusedWriteRemoval(code, analysis);
        instance.optimize();
        return instance.closeAllPatches();
    }

    private void removeIfDead() {
        if (! ((Liveness) analysis()).isAlive(originalPc()))
            remove(getDefaultPatch());
    }

    @Override
    public void visit(ADD ins) {
        removeIfDead();
    }

    @Override
    public void visit(AND ins) {
        removeIfDead();
    }

    @Override
    public void visit(MUL ins) {
        removeIfDead();
    }

    @Override
    public void visit(DIV ins) {
        removeIfDead();
    }

    @Override
    public void visit(EQ ins) {
        removeIfDead();
    }

    @Override
    public void visit(FDEF ins) {
        removeIfDead();
    }
    @Override
    public void visit(GT ins) {
        removeIfDead();
    }
    @Override
    public void visit(GTE ins) {
        removeIfDead();
    }

    @Override
    public void visit(IS_ARRAY ins) {
        removeIfDead();
    }

    @Override
    public void visit(IS_CLOSURE ins) {
        removeIfDead();
    }

    @Override
    public void visit(IS_DOUBLE ins) {
        removeIfDead();
    }

    @Override
    public void visit(LOAD_CONST ins) {
        removeIfDead();
    }

    @Override
    public void visit(LOAD_REG ins) {
        removeIfDead();
    }

    @Override
    public void visit(LOAD_VAR ins) {
        removeIfDead();
    }

    @Override
    public void visit(LT ins) {
        removeIfDead();
    }

    @Override
    public void visit(LTE ins) {
        removeIfDead();
    }

    @Override
    public void visit(NEQ ins) {
        removeIfDead();
    }

    @Override
    public void visit(RANDOM ins) {
        removeIfDead();
    }

    @Override
    public void visit(OR ins) {
        removeIfDead();
    }

    @Override
    public void visit(STORE_VAR ins) {
        /* TODO I am ignoring this for the time being because it would break the incremental mode for now */
        // removeIfDead();
    }

    @Override
    public void visit(SUB ins) {
        removeIfDead();
    }
}
