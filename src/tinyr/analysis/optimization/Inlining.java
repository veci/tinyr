package tinyr.analysis.optimization;

import tinyr.analysis.*;
import tinyr.analysis.analysis.ConstantPropagation;
import tinyr.analysis.optimization.LoopInvariant.LoopMarker;
import tinyr.bytecode.*;
import tinyr.bytecode.instructions.*;
import tinyr.runtime.*;

import java.util.*;

/**
 */
public class Inlining extends Optimizer<ConstantPropagation.Value> {

    public static boolean optimize(Code.View code, Analysis<ConstantPropagation.Value> analysis) {
        Inlining instance = new Inlining(code, analysis);
        //as far as we want to Inline inside loops there is no need to go inside without a loop
        if (LoopMarker.mark(code)) {
            instance.optimize();
            return instance.closeAllPatches();
        }
        else
            return false;
    }

    Inlining(Code.View code, Analysis<ConstantPropagation.Value> analysis) {
        super(code, analysis);
    }

    private static int index = 1;

    private void inline(Closure f, CALL ins, Code.Patch patch) {
        //Inlining optimized function
	insert(new START(f, index), patch);
        //Code.View toBeInlined = f.body.codeObject().getView(new HashSet<Patch>());
        Translator t = new Translator(newRegisters( f.body.numOfRegisters() ));
        // load the arguments to the registers
        for (int i = 0; i < ins.sregs.length; ++i)
            insert(new LOAD_REG(t.translate(i), ins.sregs[i]), patch);
        // now inline the function, translate each function and change all RET functions to LOAD_REG of the returned
        // value to the CALL instruction dreg and branch to the end of the inlined code
        Label end = new Label();
        //detection if function to inline contains RET instruction
      	boolean retIns = false;
        for (int i = 0; i < f.body.size(); ++i) {
            Instruction ii = f.body.get(i);
            if (ii instanceof RET) {
                insert(new LOAD_REG(ins.dreg, t.translate(((RET) ii).sreg)), patch);
                insert(new BR(end), patch);
                retIns = true;
            } else if (!(ii instanceof ASM)) {
        	//we can get rid of assumptions in the code by not inlining them, so only instructions would be inlined
        	//this is not problem of inlining
                insert(ii.translate(t), patch);
            }
        }
        // finally insert the inlining end branch - only in case, that function contains RET instruction, otherwise there would be
        // two LABEL instructions in a row, so we would try to execute one of them 
        if (retIns)
            insert(new LABEL(end), patch);
        insert(new END(f, index), patch);
        // and remove the old CALL instruction
        remove(patch);
        index++;
    }

    private void inlineWithHistory(Closure f, CALL ins, Code.Patch patch) {
        Translator t = new Translator(newRegisters(f.body.numOfRegisters()), f.body, code(), patch);
        // load the arguments to the registers
        for (int i = 0; i < ins.sregs.length; ++i)
            insert(new LOAD_REG(t.translate(i), ins.sregs[i]), patch);
        // now inline the function, translate each function and change all RET functions to LOAD_REG of the returned
        // value to the CALL instruction dreg and branch to the end of the inlined code
        Label end = new Label();
        for (Code.Record r : f.body.codeObject().records()) {
            if (r.instruction instanceof RET) {
                insert(r.translate(t, new LOAD_REG(ins.dreg, t.translate(((RET) r.instruction).sreg))));
                insert(r.translate(t, new BR(end)));
            } else {
                insert(r.translate(t));
            }
        }
        // finally insert the inlining end branch
        insert(new LABEL(end), patch);
        // and remove the old CALL instruction
        remove(patch);
    }

    @Override
    public void visit(CALL ins) {
        ConstantPropagation.Value v = state().get(ins.sreg);
        if (v.isConst() && v.isClosure()) {
            Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
            if (!canBeInlined(ins.frame, this.pc()))
                return;
            if (!v.closureValue.optimized) {
        	Closure.optimizeFunction(v.closureValue);
            }
            //Check whether CALL instruction is in loop
            if (ins.frame.parent.closure.body.blockForPC(pc()).loop != null) {
        	if (a != null)
                    insertUSE(a, getPatchFor(a));
                inline(v.closureValue, ins, getPatchFor(v.dependsOn()));
            }
            /*
            // Inlining every function CALL
            if (a != null)
                insertUSE(a, getPatchFor(a));
            inline(v.closureValue, ins, getPatchFor(v.dependsOn()));
            */
        }
    }

    /**
     * Decides whether function is recursive, or if calling creates a recursive code
     */
    //startPCs/endPCs represent borders for inlined code of current closure, which is to be inlined,
    //I cannot inline to already inlined code of the same function, neither can I inline to the code,
    //which creates recursive CALLING of the functions
    public static boolean canBeInlined(Frame frame, int pc) {
	Closure f = frame.closure;
	if (frame == null || f == null)
	    return false; //Nothing to inline
	frame = frame.parent;
	if (frame == null || frame.closure == null) {
	    return false; //Nowhere to inline
	}
	while(frame != null) {
	    if (frame.closure.equals(f)) {
		System.out.println("REKURZIA : " + Frame.getKey(f) + " DO : " + Frame.getKey(frame.closure));
		return false;
	    }
	    Instruction ins = null;
	    List<Integer> startPCs = new ArrayList<Integer>();
	    List<Integer> endPCs = new ArrayList<Integer>();
	    for (int i = 0; i < frame.closure.body.size(); i++) {
		ins = frame.closure.body.get(i);
		if (ins instanceof START && ((START)ins).closure.equals(f))
		    startPCs.add(i);
		if (ins instanceof END && ((END)ins).closure.equals(f))
		    endPCs.add(i);
	    }
	    for (int i = 0; i < startPCs.size(); i++) {
		if (startPCs.get(i) < pc && pc < endPCs.get(i))
		    return false;
	    }
	    frame = frame.parent;
	}
	return true;
    }

    static class Translator implements tinyr.bytecode.Translator {

        final int reg0;

        final HashMap<Label, Label> labels = new HashMap<>();

        final HashMap<Code.Patch, Code.Patch> patches = new HashMap<>();

        final HashMap<Assumption, Assumption> assumptions = new HashMap<>();

        public Translator(int reg0) {
            this.reg0 = reg0;
        }

        private Code.Patch translateOrCreate(Code.Patch old, Code.View newCode) {
            Code.Patch result = patches.getOrDefault(old, null);
            if (result == null) {
                result = newCode.openNewPatch();
                patches.put(old, result);
            }
            return result;
        }

        private Assumption translateOrCreate(Assumption old) {
            if (old instanceof Assumption.Variable)
                return old;
            Assumption a = assumptions.getOrDefault(old, null);
            if (a == null) {
                a = old.translate(this);
                assumptions.put(old, a);
            }
            return a;
        }

        public Translator(int reg0, Code.View oldCode, Code.View newCode, Code.Patch initialPatch) {
            this.reg0 = reg0;
            patches.put(null, initialPatch);
            HashSet<Code.Patch> oldPatches = new HashSet<>();
            // create a set of patches present in the old code
            for (Code.Record r : oldCode.codeObject().records()) {
                oldPatches.add(r.insertedBy);
                oldPatches.addAll(r.removedBy);
            }
            // for each of the old patches, perform the translation
            for (Code.Patch p : oldPatches) {
                Code.Patch np = translateOrCreate(p, newCode);
                for (Code.Patch dep : p.dependentPatches())
                    np.addDependentPatch(translateOrCreate(dep, newCode));
                for (Assumption a : p.dependsOnAssumptions())
                    np.dependsOnAssumption(translateOrCreate(a));
            }
        }

        @Override
        public int translate(int old) {
            return old + reg0;
        }

        @Override
        public Label translate(Label old) {
            Label result = labels.getOrDefault(old, null);
            if (result == null) {
                result = new Label();
                labels.put(old, result);
            }
            return result;
        }

        @Override
        public Code.Patch translate(Code.Patch old) {
            return patches.get(old);
        }

        @Override
        public Assumption translate(Assumption old) {
            return translateOrCreate(old);
        }
    }
}
