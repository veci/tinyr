package tinyr.analysis.optimization.peephole;

import tinyr.bytecode.Code;

/** Simplifies the structure of basic blocks.
 *
 * Nothing fancy yet, a branch followed by immediate label which is referenced only by that branch is removed and the
 * two blocks are joined together.
 */
public class StructureSimplifier {

    public static boolean optimize(Code.View code) {
        Code.Patch p = code.openNewPatch();
        int i = 0;
        while (i < code.blocksCount()) {
            Code.Block b = code.block(i);
            if (b.next.size() == 1) {
                Code.Block next = b.next.iterator().next();
                if (next.previous.size() == 1)
                    if (code.block(i + 1) == next) {
                        code.remove(next.labelAddress() - 1, p);
                        code.remove(next.labelAddress(), p);
                        --i; // the current block has changed by the merge
                    }
            }
            ++i;
        }
        return code.closeAll();
    }
}
