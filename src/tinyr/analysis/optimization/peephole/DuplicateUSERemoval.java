package tinyr.analysis.optimization.peephole;

import tinyr.bytecode.*;
import tinyr.bytecode.instructions.USE;

/** Removes duplicate USE instructions.
 *
 * This is a simple fix for the situation when multiple USE instructions of the same assumptions appear directly next
 * to each other as a result of the cycle in guard optimizations.
 *
 * It removes all but one consequent USE instructions of the same assumption.
 */
public class DuplicateUSERemoval  {

    public static boolean optimize(Code.View code) {
        Code.Patch p = code.openNewPatch();
        int i = 0;
        while (i < code.size() - 1) {
            Instruction ins = code.get(i);
            Instruction ins2 = code.get(i + 1);
            if (ins instanceof USE && ins2 instanceof USE)
                if (((USE) ins).assumption == ((USE) ins2).assumption) {
                    code.remove(i, p);
                    continue;
                }
            ++i;
        }
        return code.closeAll();
    }
}
