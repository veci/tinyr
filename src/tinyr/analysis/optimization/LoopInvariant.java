package tinyr.analysis.optimization;

import tinyr.analysis.*;
import tinyr.analysis.analysis.ReachingDefinitions;
import tinyr.bytecode.*;
import tinyr.bytecode.instructions.*;
import tinyr.runtime.*;

import java.util.*;
import javafx.util.Pair;

/** x = y1 ... yn is invariant of the loop if the following is true:
 *
 * all reaching definitions of y1 ... yn are coming from outside the loop (i.e. the basic blocks for the definitions
 * have different loop header (or their loop header is null).
 *
 * reaching definitions of x are either non-existent, or coming from outside the loop, or it is the instruction at
 * question itself (this is to make sure that the value of x stays the same for the duration of the loop.
 *
 * If x does not change, we can still perform the instruction outside the loop, but store its value to different
 * register and then only copy the register at the original point.
 *
 */
public class LoopInvariant extends Optimizer<ReachingDefinitions.Value> {

    public static boolean optimize(Code.View code, Analysis<ReachingDefinitions.Value> analysis) {
        LoopInvariant instance = new LoopInvariant(code, analysis);
        if (LoopMarker.mark(code)) {
            instance.optimize();
            instance.moveInvariantCode();
            return instance.closeAllPatches();
        } else {
            //System.out.println("Loop invariant optimization not used - no loops found");
            return false;
        }
    }

    public LoopInvariant(Code.View code, Analysis<ReachingDefinitions.Value> analysis) {
        super(code, analysis);
        cb = null;
    }

    private HashSet<Integer> invariantRegisters = new HashSet<>();
    private HashSet<Symbol> invariantVariables = new HashSet<>();

    private HashMap<Code.Block, Vector<Integer>> invariants = new HashMap<>();

    private Code.Block cb;

    private int moveInvariantsForBlock(Code.Block b, int pcOffset) {
        Vector<Integer> insns = invariants.get(b);
        // for simplicity always create new basic block
        Label l = new Label();
        final Code.Patch p = getDefaultPatch();
        int ip = b.labelAddress();
        insertAt(new LABEL(l), ip, p);
        ++pcOffset;
        ++ip;
        insertAt(new BR(b.address), ip, p);
        ++pcOffset;
        for (int pc : insns) {
            if ( pc >= 0 ) {
                pc += pcOffset;
                Instruction ins = code().get(pc);
                removeAt(pc, p);
                insertAt(ins, ip, p);
                ++ip;
            } else {
                pc = -pc;
                pc += pcOffset;
                Instruction ins = code().get(pc);
                final int reg = code().newRegister();
                final int oldReg = ((Instruction.O) ins).dreg;
                removeAt(pc, p);
                insertAt(new LOAD_REG(oldReg, reg), pc, p);
                ++pcOffset;
                insertAt(ins.translate(new Translator.SingleRegister(oldReg, reg)), ip, p);
                ++ip;
            }
        }
        // patch incoming links to the header block that do not come from the loop itself to jump to the newly created
        // prologue
        for (Code.Block bb : (HashSet<Code.Block>) b.previous.clone()) {
            if (bb.loop == b || bb.address == l)
                continue;
            int endPc = bb.codeStart() + bb.length() - 1;
            Instruction ins = code().get(endPc);
            if (ins instanceof BR) {
                removeAt(endPc, p);
                insertAt(new BR(l), endPc, p);
            } else {
                assert (ins instanceof CBR);
                CBR cbr = (CBR) ins;
                Label tl = (cbr.addressTrue == b.address) ? l : cbr.addressTrue;
                Label fl = (cbr.addressFalse == b.address) ? l : cbr.addressFalse;
                removeAt(endPc, p);
                insertAt(new CBR(cbr.sreg, tl, fl), endPc, p);
            }
        }
        return pcOffset;
    }

    private void moveInvariantCode() {
        // this is not optimal at all, but I do not care at the moment
        int pcOffset = 0;
        for (int i = 0; i < code().blocksCount(); ++i) {
            Code.Block b = code().block(i);
            if (invariants.containsKey(b)) {
                pcOffset = moveInvariantsForBlock(b, pcOffset);
                ++i;
                //moveInvariantsForBlock creates a new Block, we jump to another block, so we do not work with the same block once again
            }
        }
    }

    private boolean inLoop() {
        return cb != null && cb.loop != null;
    }

    private boolean isLoopInvariantInput(int reg) {
        if (invariantRegisters.contains(reg))
            return true;
        ReachingDefinitions.Value v = state().get(reg);
        if (v == null)
            return true;
        for (int i : v.definitions) {
            if (i == -1)
                return false;
            Code.Block b = code().blockForPC(i);
            if (b.loop == cb.loop) // input is defined somewhere in the room we are in right now
                return false;
            //checking if loop header is the same is not enough, we also have to look all the way through the previous blocks
            visitedBlocks.clear();
            visitedBlocks.add(cb.codeStart());
            if (predecessor(b, cb))
        	return false;
        }
        return true;
    }

    private boolean isLoopInvariantInput(Symbol name) {
        if (invariantVariables.contains(name))
            return true;
        ReachingDefinitions.Value v = state().get(name);
        if (v == null)
            return true;
        for (int i : v.definitions) {
            if (i == -1)
                return false;
            Code.Block b = code().blockForPC(i);
            if (b.loop == cb.loop) // input is defined somewhere in the room we are in right now
                return false;
            //checking if loop header is the same is not enough, we also have to look all the way through the previous blocks
            visitedBlocks.clear();
            visitedBlocks.add(cb.codeStart());
            if (predecessor(b, cb))
        	return false;
        }
        return true;
    }

    private boolean isLoopInvariantOutput(int reg) {
        ReachingDefinitions.Value v = state().get(reg);
        if (v == null)
            return true;
        for (int i : v.definitions) {
            if (i == pc())
                continue;
            if (i == -1)
                return false;
            Code.Block b = code().blockForPC(i);

            if (b.loop == cb.loop) // if the output is defined somewhere in the loop, it value changes and cannot be completely precomputed
                return false;
            //checking if loop header is the same is not enough, we also have to look all the way through the previous blocks
            visitedBlocks.clear();
            visitedBlocks.add(cb.codeStart());
            if (predecessor(b, cb))
        	return false;
        }
        return true;
    }

    private boolean isLoopInvariantOutput(Symbol name) {
        if (invariantVariables.contains(name))
            return true;
        ReachingDefinitions.Value v = state().get(name);
        if (v == null)
            return true;
        for (int i : v.definitions) {
            if (i == pc())
                continue;
            if (i == -1)
                return false;
            Code.Block b = code().blockForPC(i);
            if (b.loop == cb.loop) // if the output is defined somewhere in the loop, it value changes and cannot be completely precomputed
                return false;
            //checking if loop header is the same is not enough, we also have to look all the way through the previous blocks
            visitedBlocks.clear();
            visitedBlocks.add(cb.codeStart());
            if (predecessor(b, cb))
        	return false;
        }
        return true;
    }
    
    /**
     * For marking purpose, not to check same block twice
     */
    Set<Integer> visitedBlocks = new HashSet<Integer>();

    /**
     * Iterates through the previous blocks of the current one and compares to block b
     */
    private boolean predecessor(Code.Block b, Code.Block current) {
	//possible improvement, I do not have to look for predecessor only,
	// I could return true, only if the predecessor has different value (not pc, value of definition)
	boolean result = false;
	for (Code.Block block : current.previous) {
	    if (!visitedBlocks.contains(block.codeStart())) {
		visitedBlocks.add(block.codeStart());
		if (b == block)
		    return true;
		result =  result || predecessor(b, block);
	    }
	}
	return result;
    }

    private void markAsInvariant(int dreg) {
        invariantRegisters.add(dreg);
        Vector<Integer> invs = invariants.getOrDefault(cb.loop, null);
        if (invs == null) {
            invs = new Vector<>();
            invariants.put(cb.loop, invs);
        }
        invs.add(pc());
    }

    private void markAsInvariant(Symbol name) {
        invariantVariables.add(name);
        Vector<Integer> invs = invariants.getOrDefault(cb.loop, null);
        if (invs == null) {
            invs = new Vector<>();
            invariants.put(cb.loop, invs);
        }
        invs.add(pc());
    }

    /*
    private void markAsInvariantWithCopy() {
        Vector<Integer> invs = invariants.getOrDefault(cb.loop, null);
        if (invs == null) {
            invs = new Vector<>();
            invariants.put(cb.loop, invs);
        }
        invs.add(-pc());
    }
    */

    @Override
    public void visit(LABEL ins) {
        cb = code().block(ins.address);
        super.visit(ins);
    }

    /** It only makes sense for LOAD_REG to be marked as loop invariant if also the output register is loop invariant.
     *  Invariant with copy serves no purpose as there is nothing to precompute before the loop and it only causes
     *  infinite loops.
     */
    @Override
    public void visit(LOAD_REG ins) {
        if (inLoop() && isLoopInvariantInput(ins.sreg))
            if (isLoopInvariantOutput(ins.dreg))
                markAsInvariant(ins.dreg);
    }

    @Override
    public void visit(Instruction.OI ins) {
        if (inLoop() && isLoopInvariantInput(ins.sreg))
            if (isLoopInvariantOutput(ins.dreg))
                markAsInvariant(ins.dreg);
            //else
                //markAsInvariantWithCopy();
        super.visit(ins);
    }

    @Override
    public void visit(Instruction.OII ins) {
        if (inLoop() && isLoopInvariantInput(ins.sreg) && isLoopInvariantInput(ins.sreg2))
            if (isLoopInvariantOutput(ins.dreg))
                markAsInvariant(ins.dreg);
            //else
                //markAsInvariantWithCopy();
        super.visit(ins);
    }

    @Override
    public void visit(Instruction.OImm ins) {
        if (inLoop())
            if (isLoopInvariantOutput(ins.dreg))
                markAsInvariant(ins.dreg);
//            else
//                markAsInvariantWithCopy();
        super.visit(ins);
    }

    @Override
    public void visit(LOAD_VAR ins) {
        if (inLoop() && isLoopInvariantInput(ins.name))
            if (isLoopInvariantOutput(ins.name))
                markAsInvariant(ins.dreg);
            //else
                //markAsInvariantWithCopy();
        super.visit(ins);
    }

    @Override
    public void visit(STORE_VAR ins) {
        if (inLoop() && isLoopInvariantInput(ins.sreg))
            if (isLoopInvariantOutput(ins.name))
                markAsInvariant(ins.name);            
// no point in STORE_VAR being invariant with copy as the output is variable
//            else
//                markAsInvariantWithCopy();
    }

    @Override
    public void visit(RANDOM ins) {
        // random can never be moved out of the loop
    }

    public static class LoopMarker {

        final Code.View code;

        LoopMarker(Code.View code) {
            this.code = code;
        }

        int count;

        private void resetBlockCountersAndLoops() {
            for (int i = 0; i < code.blocksCount(); ++i) {
                code.block(i).cStart = 0;
                code.block(i).cEnd = 0;
                code.block(i).loop = null;
            }
            markedLoops.clear();
            count = 1;
        }

        private void dfs(Code.Block b) {
            b.cStart = count++;
            for (Code.Block bb : b.next) {
                if (bb.cStart == 0)
                    dfs(bb);
            }
            b.cEnd = count++;
        }

        private HashSet<Code.Block> backedgeOrigins(Code.Block b) {
            HashSet<Code.Block> borigs = new HashSet<>();
            for (Code.Block bb : b.previous) {
                if (bb.cStart >= b.cStart && bb.cEnd <= b.cEnd)
                    borigs.add(bb);
            }
            return borigs;
        }

        private Set<Pair<Integer, Integer>> markedLoops = new HashSet<Pair<Integer, Integer>>();

        private void markLoop(Code.Block header, Code.Block node) {
            Pair<Integer, Integer> p = new Pair<Integer, Integer>(header.index(), node.index());
            if (markedLoops.contains(p)) {
        	return;
            }
            markedLoops.add(p);

            node.loop = header;
            if (node == header)
                return;

            for (Code.Block bb : node.previous)
                // mark forward edges and crossedges too - they are fine in our implementation (presence of if statements)
                if ((bb.cStart < node.cStart && bb.cEnd > node.cEnd) || (bb.cStart > node.cEnd))
                    markLoop(header, bb);
        }

        private boolean detectLoops(Code.Block b) {
            boolean result = false;
            for (Code.Block n : backedgeOrigins(b)) {
                result = true;
                markLoop(b, n);
            }
            if (result)
                return result;
            for (Code.Block bb : b.next)
                if (bb.cStart > b.cStart && bb.cEnd < b.cEnd)
                    result = detectLoops(bb) || result;
            return result;
        }

        public static boolean mark(Code.View code) {
            LoopMarker lm = new LoopMarker(code);
            lm.resetBlockCountersAndLoops();
            lm.dfs(code.block(0));
            return lm.detectLoops(code.block(0));
        }
    }
}