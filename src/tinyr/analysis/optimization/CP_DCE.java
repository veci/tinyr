package tinyr.analysis.optimization;

import tinyr.analysis.*;
import tinyr.analysis.analysis.ConstantPropagation;
import tinyr.bytecode.*;
import tinyr.bytecode.instructions.*;
import tinyr.runtime.*;

import java.util.*;

/**
 */
public class CP_DCE extends Optimizer<ConstantPropagation.Value> {

    public static boolean optimize(Code.View code, Analysis<ConstantPropagation.Value> analysis) {
        CP_DCE instance = new CP_DCE(code, analysis);
        instance.optimize();
        instance.removeUnusedBlocks();
        return instance.closeAllPatches();
    }

    HashMap<Label, HashSet<Assumption>> blockAssumptions = new HashMap<>();

    public void addBlockAssumptions(Label l, HashSet<Assumption> asms) {
        if (asms != null && asms.size() != 0) {
            HashSet<Assumption> x = blockAssumptions.getOrDefault(l, null);
            if (x == null) {
                x = new HashSet<>();
                blockAssumptions.put(l, x);
            }
            x.addAll(asms);
        }
    }

    public CP_DCE(Code.View code, Analysis<ConstantPropagation.Value> analysis) {
        super(code, analysis);
    }

    private boolean isConstDouble(ConstantPropagation.Value v) {
        return v != null && v.isDouble() && v.isConst();
    }

    private boolean isConstClosure(ConstantPropagation.Value v) {
        return v != null && v.isClosure() && v.isConst();
    }

    private boolean canOptimizeBasedOn(Instruction ins, ConstantPropagation.Value v) {
        return v == null || ! ins.ignoresAssumptions(v.dependsOn());
    }

    private void markBlock(Code.Block b) {
        b.cStart = 1;
        for (Code.Block bb : b.next)
            if (bb.cStart == 0)
                markBlock(bb);
    }

    private void removeUnusedBlocks() {
        for (int i = 0; i < code().blocksCount(); ++i) {
            code().block(i).cStart = 0;
            code().block(i).cEnd = 0;
        }
        markBlock(code().block(0));
        int i = 0;
        while (i < code().blocksCount()) {
            Code.Block b = code().block(i);
            if (b.cStart == 0) {
                removeBlock(b.address, getPatchFor(blockAssumptions.getOrDefault(b.address, null)));
            } else {
                ++i;
            }
        }
    }

    @Override
    public void visit(ADD ins) {
        ConstantPropagation.Value lhs = state().get(ins.sreg);
        ConstantPropagation.Value rhs = state().get(ins.sreg2);
        if (canOptimizeBasedOn(ins, lhs) && canOptimizeBasedOn(ins, rhs))          
            if (isConstDouble(lhs) && isConstDouble(rhs)) {
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                a = isUsing(state().get(ins.sreg2), ins.sreg2);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, lhs.doubleValue + rhs.doubleValue), getPatchFor(lhs.dependsOn(), rhs.dependsOn()));                
            }
    }

    @Override
    public void visit(AND ins) {
        ConstantPropagation.Value lhs = state().get(ins.sreg);
        ConstantPropagation.Value rhs = state().get(ins.sreg2);
        if (canOptimizeBasedOn(ins, lhs) && canOptimizeBasedOn(ins, rhs))
            if (isConstDouble(lhs) && isConstDouble(rhs)){
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                a = isUsing(state().get(ins.sreg2), ins.sreg2);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, (lhs.doubleValue != 0  && rhs.doubleValue != 0) ? 1 : 0), getPatchFor(lhs.dependsOn(), rhs.dependsOn()));
            }
    }

    @Override
    public void visit(MUL ins) {
        ConstantPropagation.Value lhs = state().get(ins.sreg);
        ConstantPropagation.Value rhs = state().get(ins.sreg2);
        if (canOptimizeBasedOn(ins, lhs) && canOptimizeBasedOn(ins, rhs))
            if (isConstDouble(lhs) && isConstDouble(rhs)){
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                a = isUsing(state().get(ins.sreg2), ins.sreg2);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, lhs.doubleValue * rhs.doubleValue), getPatchFor(lhs.dependsOn(), rhs.dependsOn()));
            }
    }

    @Override
    public void visit(DIV ins) {
        ConstantPropagation.Value lhs = state().get(ins.sreg);
        ConstantPropagation.Value rhs = state().get(ins.sreg2);
        if (canOptimizeBasedOn(ins, lhs) && canOptimizeBasedOn(ins, rhs))
            if (isConstDouble(lhs) && isConstDouble(rhs)){
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                a = isUsing(state().get(ins.sreg2), ins.sreg2);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, lhs.doubleValue / rhs.doubleValue), getPatchFor(lhs.dependsOn(), rhs.dependsOn()));
            }
    }

    @Override
    public void visit(CBR ins) {
        ConstantPropagation.Value v = state().get(ins.sreg);
        if (canOptimizeBasedOn(ins, v))
            if (isConstDouble(v)) {
                if (v.doubleValue == 0) {
                    Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                    if (a != null)
                        insertUSE(a, getPatchFor(a));
                    replace(new BR(ins.addressFalse), getPatchFor(v.dependsOn()));
                    addBlockAssumptions(ins.addressTrue, v.dependsOn());
                    //removeBlockIfUnused(ins.addressTrue, getPatchFor(v.dependsOn()));
                } else {
                    Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                    if (a != null)
                        insertUSE(a, getPatchFor(a));
                    replace(new BR(ins.addressTrue), getPatchFor(v.dependsOn()));
                    addBlockAssumptions(ins.addressFalse, v.dependsOn());
                    //removeBlockIfUnused(ins.addressFalse, getPatchFor(v.dependsOn()));
                }
            }
    }

    @Override
    public void visit(EQ ins) {
        ConstantPropagation.Value lhs = state().get(ins.sreg);
        ConstantPropagation.Value rhs = state().get(ins.sreg2);
        if (canOptimizeBasedOn(ins, lhs) && canOptimizeBasedOn(ins, rhs))
            if (isConstDouble(lhs) && isConstDouble(rhs)){
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                a = isUsing(state().get(ins.sreg2), ins.sreg2);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, lhs.doubleValue == rhs.doubleValue ? 1 : 0), getPatchFor(lhs.dependsOn(), rhs.dependsOn()));
            }
    }

    @Override
    public void visit(GT ins) {
        ConstantPropagation.Value lhs = state().get(ins.sreg);
        ConstantPropagation.Value rhs = state().get(ins.sreg2);
        if (canOptimizeBasedOn(ins, lhs) && canOptimizeBasedOn(ins, rhs))
            if (isConstDouble(lhs) && isConstDouble(rhs)){
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                a = isUsing(state().get(ins.sreg2), ins.sreg2);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, lhs.doubleValue > rhs.doubleValue ? 1 : 0), getPatchFor(lhs.dependsOn(), rhs.dependsOn()));
            }
    }

    @Override
    public void visit(GTE ins) {
        ConstantPropagation.Value lhs = state().get(ins.sreg);
        ConstantPropagation.Value rhs = state().get(ins.sreg2);
        if (canOptimizeBasedOn(ins, lhs) && canOptimizeBasedOn(ins, rhs))
            if (isConstDouble(lhs) && isConstDouble(rhs)){
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                a = isUsing(state().get(ins.sreg2), ins.sreg2);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, lhs.doubleValue >= rhs.doubleValue ? 1 : 0), getPatchFor(lhs.dependsOn(), rhs.dependsOn()));
            }
    }

    @Override
    public void visit(IS_ARRAY ins) {
        ConstantPropagation.Value v = state().get(ins.sreg);
        if (canOptimizeBasedOn(ins, v))          
            if (v.isArray()) {
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, new Primitive(1)), getPatchFor(v.dependsOn()));
            }
            else if (! v.isTop()) {
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, new Primitive(0)), getPatchFor(v.dependsOn()));
            }
    }

    @Override
    public void visit(IS_CLOSURE ins) {
        ConstantPropagation.Value v = state().get(ins.sreg);
        if (canOptimizeBasedOn(ins, v))            
            if (v.isClosure()) {
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, new Primitive(1)), getPatchFor(v.dependsOn()));
            }
            else if (! v.isTop()) {
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, new Primitive(0)), getPatchFor(v.dependsOn()));
            }        
    }

    @Override
    public void visit(IS_DOUBLE ins) {
        ConstantPropagation.Value v = state().get(ins.sreg);
        if (canOptimizeBasedOn(ins, v))                       
            if (v.isDouble()) {
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, new Primitive(1)), getPatchFor(v.dependsOn()));
            }
            else if (! v.isTop()) {
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, new Primitive(0)), getPatchFor(v.dependsOn()));
            }
    }

    /** LOAD_CONST of same value that is already in the register should be ignored
     */
    @Override
    public void visit(LOAD_CONST ins) {
        ConstantPropagation.Value v = state().get(ins.dreg);
        if (ins.value instanceof Primitive){
            if (isConstDouble(v) && v.doubleValue == ((Primitive) ins.value).value)
                remove(getPatchFor(v.dependsOn()));  
        }
    }

    @Override
    public void visit(LOAD_REG ins) {
        ConstantPropagation.Value lhs = state().get(ins.sreg);
        if (canOptimizeBasedOn(ins, lhs))
            if (isConstDouble(lhs)){
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));                
                replace(new LOAD_CONST(ins.dreg, lhs.doubleValue), getPatchFor(lhs.dependsOn()));
            } else if (isConstClosure(lhs)){
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, lhs.closureValue), getPatchFor(lhs.dependsOn()));
            }
    }

    @Override
    public void visit(LOAD_VAR ins) {
        ConstantPropagation.Value lhs = state().get(ins.name);
        if (canOptimizeBasedOn(ins, lhs)) {
            if (isConstDouble(lhs)) {
                Assumption a = isUsing(state().get(ins.name), ins.name);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, lhs.doubleValue), getPatchFor(lhs.dependsOn()));
            } else if (isConstClosure(lhs)) {
                Assumption a = isUsing(state().get(ins.name), ins.name);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, lhs.closureValue), getPatchFor(lhs.dependsOn()));
            }
        }
    }

    @Override
    public void visit(LT ins) {
        ConstantPropagation.Value lhs = state().get(ins.sreg);
        ConstantPropagation.Value rhs = state().get(ins.sreg2);
        if (canOptimizeBasedOn(ins, lhs) && canOptimizeBasedOn(ins, rhs))
            if (isConstDouble(lhs) && isConstDouble(rhs)){
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                a = isUsing(state().get(ins.sreg2), ins.sreg2);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, lhs.doubleValue < rhs.doubleValue ? 1 : 0), getPatchFor(lhs.dependsOn(), rhs.dependsOn()));
            }
    }

    @Override
    public void visit(LTE ins) {
        ConstantPropagation.Value lhs = state().get(ins.sreg);
        ConstantPropagation.Value rhs = state().get(ins.sreg2);
        if (canOptimizeBasedOn(ins, lhs) && canOptimizeBasedOn(ins, rhs))
            if (isConstDouble(lhs) && isConstDouble(rhs)){
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                a = isUsing(state().get(ins.sreg2), ins.sreg2);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, lhs.doubleValue <= rhs.doubleValue ? 1 : 0), getPatchFor(lhs.dependsOn(), rhs.dependsOn()));
            }
    }

    @Override
    public void visit(NEQ ins) {
        ConstantPropagation.Value lhs = state().get(ins.sreg);
        ConstantPropagation.Value rhs = state().get(ins.sreg2);
        if (canOptimizeBasedOn(ins, lhs) && canOptimizeBasedOn(ins, rhs))
            if (isConstDouble(lhs) && isConstDouble(rhs)) {
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                a = isUsing(state().get(ins.sreg2), ins.sreg2);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, lhs.doubleValue != rhs.doubleValue ? 1 : 0), getPatchFor(lhs.dependsOn(), rhs.dependsOn()));
            }
    }

    @Override
    public void visit(OR ins) {
        ConstantPropagation.Value lhs = state().get(ins.sreg);
        ConstantPropagation.Value rhs = state().get(ins.sreg2);
        if (canOptimizeBasedOn(ins, lhs) && canOptimizeBasedOn(ins, rhs))
            if (isConstDouble(lhs) && isConstDouble(rhs)) {
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                a = isUsing(state().get(ins.sreg2), ins.sreg2);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, (lhs.doubleValue != 0 || rhs.doubleValue != 0) ? 1 : 0), getPatchFor(lhs.dependsOn(), rhs.dependsOn()));
            }
    }


    @Override
    public void visit(SUB ins) {
        ConstantPropagation.Value lhs = state().get(ins.sreg);
        ConstantPropagation.Value rhs = state().get(ins.sreg2);
        if (canOptimizeBasedOn(ins, lhs) && canOptimizeBasedOn(ins, rhs))
            if (isConstDouble(lhs) && isConstDouble(rhs)) {
                Assumption a = isUsing(state().get(ins.sreg), ins.sreg);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                a = isUsing(state().get(ins.sreg2), ins.sreg2);
                if (a != null)
                    insertUSE(a, getPatchFor(a));
                replace(new LOAD_CONST(ins.dreg, lhs.doubleValue - rhs.doubleValue), getPatchFor(lhs.dependsOn(), rhs.dependsOn()));
            }
    }

}
