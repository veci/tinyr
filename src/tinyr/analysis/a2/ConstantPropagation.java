package tinyr.analysis.a2;

import tinyr.runtime.Closure;

/**
 */
public class ConstantPropagation extends Analysis {

    @Override
    public State initialState() {
        return new State(phase_.code_.numOfRegisters());
    }

    public static class Value extends tinyr.analysis.a2.Value {

        public static enum Type {
            Double,
            ConstDouble,
            Closure,
            ConstClosure,
            Array,
            Top
        }
        public Type type;
        public double valueDouble;
        public Closure valueClosure;

        public Value(Type type) {
            assert (type != Type.ConstDouble);
            assert (type != Type.ConstClosure);
            assert (type != Type.Top);
            this.type = type;
            this.valueDouble = 0;
            this.valueClosure = null;
        }

        public Value(double value) {
            this.type = Type.ConstDouble;
            this.valueDouble = value;
            this.valueClosure = null;
        }

        public Value(Closure value) {
            this.type = Type.ConstClosure;
            this.valueDouble = 0;
            this.valueClosure = value;
        }

        public static Type join(Type first, Type second) {
            if (first == second)
                return first;
            if ((first == Type.ConstDouble) && (second == Type.Double))
                return Type.Double;
            if ((first == Type.Double && (second == Type.ConstDouble)))
                return Type.Double;
            if ((first == Type.ConstClosure) && (second == Type.Closure))
                return Type.Closure;
            if ((first == Type.Closure) && (second == Type.ConstClosure))
                return Type.Closure;
            return Type.Top;
        }

        @Override
        public tinyr.analysis.a2.Value merge(tinyr.analysis.a2.Value other) {
            // if other is top or bottom, super case will deal with it
            tinyr.analysis.a2.Value result = super.merge(other);
            if (result != null)
                return result;
            // it is not, check the real merger
            Value o = (Value) other;
            switch (join(type, o.type)) {
                case ConstDouble:
                    if (valueDouble == o.valueDouble)
                        return new Value(valueDouble);
                case Double:
                    return new Value(Type.Double);
                case ConstClosure:
                    if (valueClosure == o.valueClosure)
                        return new Value(valueClosure);
                case Closure:
                    return new Value(Type.Closure);
                case Array:
                    return new Value(Type.Array);
                default:
                    return tinyr.analysis.a2.Value.top;
            }
        }

        @Override
        public boolean equals(Object other) {
            if (! (other instanceof Value))
                return false;
            Value o = (Value) other;
            return type == o.type && valueDouble == o.valueDouble && valueClosure == o.valueClosure;
        }

    }
}
