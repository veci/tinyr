package tinyr.analysis.a2;

import tinyr.ast.Assumption;
import tinyr.bytecode.*;
import tinyr.bytecode.instructions.*;
import tinyr.runtime.Label;

import java.util.*;

/** Phase first executes the analyses it has and then executes the optimizations.
 */
public class Phase {

    Code.View code_ = null;

    Vector<Analysis> analyses_ = new Vector<>();

    HashSet<Assumption> currentControlFlowAssumptions_;
    HashSet<Assumption>[] controlFlowAssumptions_;
    State[] currentState_;
    State[][] states_;

    Label ignoredBranch_= null;
    Analysis ignoredBranchAnalysis_ = null;


    int currentPc_;


    static class Entry {
        final int pc;
        final State[] state;

        public Entry(int pc, State[] state) {
            this.pc = pc;
            this.state = state;
        }
    }

    Stack<Entry> stack_ = new Stack<>();


    public Phase() {

    }

    public Phase addAnalysis(Analysis a) {
        analyses_.add(a);
        return this;
    }


    private void fillInitialState() {
        for (int i = 0; i < analyses_.size(); ++i)
            states_[0][i] = analyses_.get(i).initialState();
    }

    private void pushImmediate(int pc) {
        stack_.push(new Entry(pc, currentState_));
    }

    private void push(int pc) {
        State[] s = new State[currentState_.length];
        for (int i = 0; i < s.length; ++i)
            s[i] = currentState_[i].clone();
        stack_.push(new Entry(pc, s));
    }

    public Phase analyze(Code.View code) {
        code_ = code;
        assert (stack_.isEmpty());
        analyze(true);
        State[][] ignored = states_;
        analyze(false);
        for (int a = 0; a < currentState_.length; ++a) {
            for (int i = 0; i < code_.blocksCount(); ++i)
                states_[a][i].updateAssumptions(ignored[a][i], controlFlowAssumptions_[i]);
        }
        return this;
    }

    /** Used by analysis to flag that certain branch target is not possible.
     */
    public void ignoreBranch(Label target, Analysis analysis) {
        assert (ignoredBranch_ == null || ignoredBranch_ == target) : "If more than one analysis says branch will be ignored they must agree on which one";
        ignoredBranchAnalysis_ = analysis;
    }

    private void analyze(boolean ignoreAssumptions) {
        assert (stack_.isEmpty());
        states_ = new State[analyses_.size()][code_.blocksCount()];
        currentState_ = new State[analyses_.size()];
        currentControlFlowAssumptions_ = new HashSet<>();
        controlFlowAssumptions_ = new HashSet[code_.blocksCount()];
        controlFlowAssumptions_[0] = (HashSet)currentControlFlowAssumptions_.clone();
        fillInitialState();
        pushImmediate(0);
        while (! stack_.isEmpty()) {
            currentPc_ = stack_.peek().pc;
            currentState_ = stack_.pop().state;
            Instruction ins = code_.get(currentPc_);
            // if the instruction is label, perform the mergepoints update & merge
            if (ins instanceof LABEL ) {
                LABEL l = (LABEL) ins;
                Code.Block b = code_.block(l.address);
                // update the control flow assumptions for the basic block to the current control flow assumptions
                if (!ignoreAssumptions)
                    controlFlowAssumptions_[b.index()] = (HashSet) currentControlFlowAssumptions_.clone();
                // for each analysis we have, update the incoming state for the BB. If none of the incoming states
                // change,
                boolean changed = false;
                for (int i = 0; i < states_.length; ++i) {
                    State stored = states_[i][b.index()];
                    if (stored == null) {
                        states_[i][b.index()] = currentState_[i].clone();
                        changed = true;
                    } else {
                        changed = stored.merge(currentState_[i]) || changed;
                        currentState_[i] = stored.clone();
                    }
                }
                // if any of the stored states did not change, continue without analysing the basic block
                if (! changed)
                    continue;
            }
            // if we are ignoring assumptions, ignore ASM and USE instructions
            if (ignoreAssumptions && (ins instanceof ASM || ins instanceof USE)) {
                pushImmediate(currentPc_ + 1);
                continue;
            }
            // if decision is made to advance further, call each analysis on the instruction given and modify the
            // current state
            for (int i = 0; i < currentState_.length; ++i) {
                Analysis a = analyses_.get(i);
                a.setState(currentState_[i]);
                ins.accept(a);
            }
            // now determine the next instruction and push it to the stack - this can't be done by the analyses any more
            // because more than one is executed at once
            if (ins instanceof CBR) {
                CBR cbr = (CBR) ins;
                if (ignoredBranch_ != null) {
                    // If there is a branch to be ignored, jump only to the other branch
                    Label address = ignoredBranch_ == cbr.addressTrue ? cbr.addressFalse : cbr.addressTrue;
                    pushImmediate(code_.block(address).labelAddress());
                    // if there are any assumptions attached to the CBR's incoming register in the analysis that
                    // flagged it as ignored, these must be added to the control flow assumptions
                    int analysisIndex = 0;
                    while (analyses_.get(analysisIndex) != ignoredBranchAnalysis_)
                        ++analysisIndex;
                    HashSet<Assumption> preqs = currentState_[analysisIndex].getPrerequisites(cbr.sreg);
                    if (preqs != null)
                        currentControlFlowAssumptions_.addAll(preqs);
                    ignoredBranch_ = null;
                    ignoredBranchAnalysis_ = null;
                } else {
                    push(code_.block(cbr.addressFalse).labelAddress());
                    pushImmediate(code_.block(cbr.addressTrue).labelAddress());
                }
            } else if (ins instanceof BR) {
                pushImmediate(code_.block(((BR) ins).address).labelAddress());
            } else if (ins instanceof RET) {
                continue;
            } else {
                pushImmediate(currentPc_ + 1);
            }
        }
    }

    public Phase optimize() {
        assert (code_ != null);

        return this;
    }




}
