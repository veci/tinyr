package tinyr.analysis.a2;

/**
 */
public abstract class Value implements Cloneable {

    public static Value bottom = new Bottom();

    public static Value top = new Top();

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Value merge(Value other) {
        if (other == top)
            return other;
        if (other == bottom)
            return this;
        return null;
    }

    static class Bottom extends Value {
        /** Anything merged with bottom value is always the other value.
         */
        @Override
        public Value merge(Value other) {
            return other;
        }

        @Override
        public boolean equals(Object other) {
            return this == other;
        }
    }

    static class Top extends Value {

        /** Anything merged with top is always top.
         */
        @Override
        public Value merge(Value other) {
            return this;
        }

        @Override
        public boolean equals(Object other) {
            return this == other;
        }
    }
}
