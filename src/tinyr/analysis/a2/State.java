package tinyr.analysis.a2;

import tinyr.ast.Assumption;
import tinyr.runtime.Symbol;

import java.util.*;

/** A state for single analysis.
 *
 * State tracks
 *
 */
public class State implements Cloneable {

    static class Element {
        Value value = null;
        HashSet<Assumption> prerequisites = null;

        public Element(Value value) {
            this.value = value;
        }

        public Element(Element from) {
            try {
                this.value = (Value) from.value.clone();
                this.prerequisites = from.prerequisites != null ? (HashSet<Assumption>) from.prerequisites.clone() : null;
            } catch (CloneNotSupportedException e) {
                // pass
            }
        }

        public Element clone() {
            try {
                Element result = new Element((Value) value.clone());
                if ( prerequisites != null )
                    result.prerequisites = (HashSet<Assumption>) prerequisites.clone();
                return result;
            } catch (CloneNotSupportedException e) {
                // pass - cannot happen
                return null;
            }
        }

    }

    Element[] registers_;
    HashMap<Symbol, Element> variables_ = new HashMap<>();

    public State(int numOfRegisters) {
        registers_ = new Element[numOfRegisters];
    }

    void set(int index, Value value) {
        registers_[index] = new Element(value);
    }

    Value get(int index) {
        Element e = registers_[index];
        if (e != null)
            return e.value;
        return null;
    }

    // Set discards all previous prerequisites (!!!!)
    void set(Symbol name, Value value) {
        variables_.put(name, new Element(value));
    }

    Value get(Symbol name) {
        Element e = variables_.getOrDefault(name, null);
        if (e != null)
            return e.value;
        return null;
    }

    HashSet<Assumption> getPrerequisites(int index) {
        Element e = registers_[index];
        if (e != null)
            return e.prerequisites;
        return null;
    }

    void setPrerequisites(int index, HashSet<Assumption> preqs) {
        Element e = registers_[index];
        if (e == null) {
            e = new Element(get(index));
            registers_[index] = e;
        }
        e.prerequisites = preqs;
    }

    HashSet<Assumption> getPrerequisites(Symbol name) {
        Element e = variables_.getOrDefault(name, null);
        if (e != null)
            return e.prerequisites;
        return null;
    }

    void setPrerequisites(Symbol name, HashSet<Assumption> preqs) {
        Element e = variables_.getOrDefault(name, null);
        if (e == null) {
            e = new Element(get(name));
            variables_.put(name, e);
        }
        e.prerequisites = preqs;
    }

    void addPrerequisites(int reg, HashSet<Assumption> preqs) {
        if (preqs == null || preqs.isEmpty())
            return;
        HashSet<Assumption> p = getPrerequisites(reg);
        if (p == null)
            setPrerequisites(reg, (HashSet) preqs.clone());
        else
            p.addAll(preqs);
    }

    void addPrerequisites(Symbol name, HashSet<Assumption> preqs) {
        if (preqs == null || preqs.isEmpty())
            return;
        HashSet<Assumption> p = getPrerequisites(name);
        if (p == null)
            setPrerequisites(name, (HashSet) preqs.clone());
        else
            p.addAll(preqs);
    }

    private static class Result {
        boolean value = false;
    }

    public Element mergeElements(Element first, Element second, Result result) {
        if (first == null) {
            if (second != null) {
                result.value = true;
                first = new Element(second);
            }
        } else if (second == null) {
            // pass
        } else { // both first and second exists
            // we can be sure values are never null and that merge returns a cloned object
            Value v = first.value.merge(second.value);
            if (! v.equals(first.value))
                result.value = true;
            first.value = v;
            if (first.prerequisites == null) {
                if (second.prerequisites != null) {
                    result.value = true;
                    first.prerequisites = (HashSet<Assumption>) second.prerequisites.clone();
                }
            } else {
                if (second.prerequisites != null) {
                    int oldSize = first.prerequisites.size();
                    first.prerequisites.addAll(second.prerequisites);
                    if (oldSize != first.prerequisites.size())
                        result.value = true;
                }
            }
        }
        return first;
    }


    public boolean merge(State other) {
        Result result = new Result();
        // update all registers
        for (int i = 0; i < registers_.length; ++i) {
            Element own = registers_[i];
            Element their = other.registers_[i];
            registers_[i] = mergeElements(own, their, result);
        }
        // update all variables - that is read all variables from the other state and update those that we have already and just copy-clone those we do not
        for (Map.Entry<Symbol, Element> entry : other.variables_.entrySet()) {
            Element own = variables_.getOrDefault(entry.getKey(), null);
            Element their = entry.getValue();
            variables_.put(entry.getKey(),  mergeElements(own, their, result));
        }
        return result.value;
    }

    public void updateAssumptions(State without, HashSet<Assumption> controlFlowAssumptions) {
        for (int i = 0; i < registers_.length; ++i) {
            Value own = get(i);
            Value wo = without.get(i);
            if (own.equals(wo))
                setPrerequisites(i, null);
            else
                addPrerequisites(i, controlFlowAssumptions);
        }
        HashSet<Symbol> names = new HashSet<>();
        names.addAll(variables_.keySet());
        names.addAll(without.variables_.keySet());
        for (Symbol name : names) {
            Value own = get(name);
            Value wo = without.get(name);
            if (own.equals(wo))
                setPrerequisites(name, null);
            else
                addPrerequisites(name, controlFlowAssumptions);
        }
    }

    public State clone() {
        State result = new State(registers_.length);
        for (int i = 0; i < registers_.length; ++i)
            result.registers_[i] = registers_[i] == null ? null : registers_[i].clone();
        for (Map.Entry<Symbol, Element> entry : variables_.entrySet())
            result.variables_.put(entry.getKey(), entry.getValue().clone());
        return result;
    }

}
