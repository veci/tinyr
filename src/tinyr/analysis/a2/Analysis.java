package tinyr.analysis.a2;

import tinyr.bytecode.*;
import tinyr.runtime.Symbol;

/**
 * Created by Peta on 24-Oct-14.
 */
public abstract class Analysis extends Visitor {

    protected State state_;
    protected Phase phase_;

    void setState(State state) {
        state_ = state;
    }

    void setPhase(Phase phase) {
        phase_ = phase;

    }

    public Value defaultRegister() {
        return Value.bottom;
    }

    public Value defaultVariable() {
        return Value.top;
    }

    public abstract State initialState();

    protected Value get(int reg) {
        Value result = state_.get(reg);
        if (result == null)
            result = defaultRegister();
        return result;
    }

    protected Value get(Symbol name) {
        Value result = state_.get(name);
        if (result == null)
            result = defaultVariable();
        return result;
    }

    protected void set(int reg, Value value) {
        state_.set(reg, value);
    }

    protected void set(Symbol name, Value value) {
        state_.set(name, value);
    }

    protected Value get(int reg, Analysis analysis) {
        for (int i = 0; i < phase_.currentState_.length; ++i) {
            assert (phase_.analyses_.get(i) != this) : "Analyses can only request states for analyses already executed for the instruction";
            if (phase_.analyses_.get(i) == analysis)
                return phase_.currentState_[i].get(reg);
        }
        return null; // this should never happen because of the assertion above
    }

    protected Value get(Symbol name, Analysis analysis) {
        for (int i = 0; i < phase_.currentState_.length; ++i) {
            assert (phase_.analyses_.get(i) != this) : "Analyses can only request states for analyses already executed for the instruction";
            if (phase_.analyses_.get(i) == analysis)
                return phase_.currentState_[i].get(name);
        }
        return null; // this should never happen because of the assertion above
    }



}
