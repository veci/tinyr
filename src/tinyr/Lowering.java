package tinyr;

import java.util.*;

import tinyr.ast.Node;
import tinyr.bytecode.*;
import tinyr.bytecode.instructions.BR;
import tinyr.bytecode.instructions.CBR;
import tinyr.bytecode.instructions.LABEL;
import tinyr.bytecode.instructions.RET;
import tinyr.runtime.*;

/**
 */
public class Lowering {
    public static HashMap<Symbol, Closure> definedClosures = new HashMap<>();

    public static class State {
        Vector<tinyr.bytecode.Instruction> insns = new Vector<>();
        
        public int registers = 0;
        public HashMap<String,Integer> registerAliases = new HashMap<>();
        
        private tinyr.bytecode.Instruction lastInstruction = null;        
        
        public HashMap<Integer, Assumption> localAssumptions = new HashMap<>();
        
        /** Will be initialized by FunctionDefinition. It will contain the symbols for all the function
         * parameters; in particular, the first parameter will correspond to the register 0 and so on, and the
         * number of registers initially in use will be subsequently set to the size of localArgs.
         */
        public Symbol[] localArgs = null;
    }

    private HashMap<Integer, Assumption> globalAssumptions = new HashMap<>();

    public Assumption getAssumption(int id) {
        Assumption result = state.localAssumptions.getOrDefault(id, null);
        if (result == null)
            result = globalAssumptions.getOrDefault(id, null);
        if (result == null)
            throw new RuntimeException("Undeclared assumption " + id);
        return result;
    }

    public void addAssumption(int id, Assumption assumption) {
        if (assumption instanceof Assumption.Variable) {
            assert (!globalAssumptions.containsKey(id));
            globalAssumptions.put(id, assumption);
        } else {
            assert (!state.localAssumptions.containsKey(id));
            state.localAssumptions.put(id, assumption);
        }
    }

    public State state;

    /** Returns the register associated with a function argument.     
     * @return The number of the corresponding register if name is a function parameter, -1 otherwise.
     */
    public int localArgumentRegister(Symbol name) {
        if (state.localArgs == null)
           return -1;
        for (int i = 0; i < state.localArgs.length; ++i)
            if (state.localArgs[i] == name)
                return i;
        return -1;
    }

    public tinyr.bytecode.Instruction lastInstruction() {
        return state.lastInstruction;
    }
    
    public boolean isLastTerminating() {
      return state.lastInstruction != null && (state.lastInstruction instanceof BR || state.lastInstruction instanceof CBR || state.lastInstruction instanceof RET);
    }

    public Label breakLabel = null;

    public Label continueLabel = null;

    public int newRegister() {
        return state.registers++;
    }

    public <T extends tinyr.bytecode.Instruction> T addInstruction(T i) {
        if (! (i instanceof LABEL) && isLastTerminating())
            state.insns.add(new LABEL(new Label()));
        state.insns.add(i);
        state.lastInstruction = i;
        return i;
    }
    
    public Instruction getLastInstruction() {
      return state.insns.lastElement();
    }

    /** Sets the instruction for the given PC.
     */
    public <T extends tinyr.bytecode.Instruction> T patch(int pc, T i) {
        state.insns.set(pc, i);
        return i;
    }

    public int pc() {
        return state.insns.size();
    }

    public Code lower (Node root) {
      return lower(root, new State());      
    }
        
    public Code lower(Node root, State state) {        
        this.state = state;
        if (root != null) {
            int last = root.lower(this);
            if ( last >= 0 )
                addInstruction(new tinyr.bytecode.instructions.RET(last));
            else if ( last == -2 ) { // ASM, INV, USE
                // TODO we should return null here in reality, but null is not yet supported in tinyR
            }
        }
//        for (Instruction i : state.insns)
//          System.out.println(i);
        return new Code(state.insns.toArray(new tinyr.bytecode.Instruction[state.insns.size()]), state.registers );
    }
}
