package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.Frame;

/**
 */
public class RET extends Instruction.I {

    public RET(int sreg) {
        super(sreg);
    }

    @Override
    public void execute(Frame frame) {
        frame.result = frame.get(sreg);
        frame.pc = Integer.MAX_VALUE - 1; // -1 because it will get incremented automatically in the code.execute() loop
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new RET(t.translate(sreg));
    }

    @Override
    public boolean isTerminating() {
        return true;
    }

}
