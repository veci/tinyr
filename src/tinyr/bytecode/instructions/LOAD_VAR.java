package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class LOAD_VAR extends Instruction.OR {

    public LOAD_VAR(int dreg, Symbol name) {
        super(dreg, name);
    }

    /** Only for testing. */
    public LOAD_VAR(int dreg, String name) {
        super(dreg, Symbol.make(name));
    }

    @Override
    public void execute(Frame frame) {
        Value v = Frame.getVariable(name);
        if (v == null)
            throw new RuntimeException("Variable " + name + " not found");
        frame.set(dreg, v);
        computePossibleAssumptions(frame, v, dreg, frame.pc + 1);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new LOAD_VAR(t.translate(dreg), name);
    }
}
