package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class ARRAY_CREATE extends Instruction.OII {
    public ARRAY_CREATE(int dreg, int sreg, int sreg2) {
        super(dreg, sreg, sreg2);
    }

    @Override
    public void execute(Frame frame) {
        int l = (int) ((Primitive) frame.get(sreg)).value;
        double v = ((Primitive) frame.get(sreg2)).value;
        Array a = new Array(l);
        for (int i = 0; i < l; ++i)
            a.contents[i] = v;
        frame.set(dreg, a);
        computePossibleAssumptions(frame, a, dreg, frame.pc + 1);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new ARRAY_CREATE(t.translate(dreg), t.translate(sreg), t.translate(sreg2));
    }
}
