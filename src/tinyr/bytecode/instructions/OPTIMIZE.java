package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;


/** Optimizes the function.
 */
public class OPTIMIZE extends Instruction.R {

    public OPTIMIZE(Symbol name) {
        super(name);
    }

    @Override
    public void execute(Frame frame) {
        optimize((Closure) Frame.getVariable(name));
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new OPTIMIZE(name);
    }

    /** Optimizes the code and returns a patch describing the changes.
     */
    public static void optimize(Closure c) {
        Closure.optimizeFunction(c);
    }
}
