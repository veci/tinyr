package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.Frame;

/** Simple instruction for making breakpoints in the code when debugging tinyR.
 */
public class BKPT extends Instruction {
    @Override
    public void execute(Frame frame) {
        System.out.println("BREAKPOINT");
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new BKPT();
    }

}
