package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class ARRAY_LENGTH extends Instruction.OI {

    public ARRAY_LENGTH(int dreg, int sreg) {
        super(dreg, sreg);
    }

    @Override
    public void execute(Frame frame) {
        Array a = (Array) frame.get(sreg);
        Value result = new Primitive(a.contents.length);
        frame.set(dreg, result);
        computePossibleAssumptions(frame, result, dreg, frame.pc + 1);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new ARRAY_LENGTH(t.translate(dreg), t.translate(sreg));
    }
}
