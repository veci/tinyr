package tinyr.bytecode.instructions;

import tinyr.bytecode.Instruction;
import tinyr.bytecode.Translator;
import tinyr.bytecode.Visitor;
import tinyr.runtime.Closure;
import tinyr.runtime.Frame;

public class START extends Instruction.INL {

    public START(Closure closure, int index) {
	super(closure, index);
    }

    @Override
    public void execute(Frame frame) {
	// Nothing to do while executing this, for marking purpose
    }

    @Override
    public void accept(Visitor visitor) {
	visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
	return new START(closure, index);
    }

}
