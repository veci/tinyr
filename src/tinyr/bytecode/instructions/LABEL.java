package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class LABEL extends Instruction.L {

    public LABEL(Label value) {
        super(value);
    }

    @Override
    public void execute(Frame frame) {        
        assert (false) : "LABEL instruction should never be executed.";
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new LABEL(t.translate(address));
    }
}
