package tinyr.bytecode.instructions;

import java.util.Collection;

import tinyr.bytecode.Instruction;
import tinyr.bytecode.Translator;
import tinyr.bytecode.Visitor;
import tinyr.runtime.Closure;
import tinyr.runtime.Frame;
import tinyr.runtime.Symbol;
import tinyr.runtime.Value;

/**
 */
public class CALL extends Instruction.OIIArray {

    public CALL( int dreg, int sreg, int[] sregs ) {
        super( dreg, sreg, sregs );
    }

    public Frame frame = null;

    private static boolean mainFunction = true;

    @Override
    public void execute( Frame frame ) {
        Closure fn = (Closure) frame.get( sreg );
        if (mainFunction) {
            fn.mainFunction = true;
            mainFunction = false;
        }
        if ( fn.numOfArgs != sregs.length ) throw new RuntimeException( "Number of Arguments do not match !" );
        // no need to optimize already optimized function
        if ( !fn.optimized ) {
            Closure.optimizeFunction(fn);
        }
        Frame calleeFrame = new Frame(fn);
        //System.out.println(Frame.getKey(frame.closure) + " CALLS : " + Frame.getKey(calleeFrame.closure));
        calleeFrame.parent = frame;
        this.frame = calleeFrame;

        for ( int i = 0; i < sregs.length; ++i ) {
            calleeFrame.set( i, frame.get( sregs[i] ) );
            if (!fn.mainFunction) {//no captures on arguments of main function
                computePossibleAssumptions(calleeFrame, frame.get(sregs[i]), i, calleeFrame.pc);
            }
        }
        fn.executed = true;
        Value result = fn.body.execute( calleeFrame );
        frame.set( dreg, result );
        //computePossibleAssumptions(frame, result, dreg, frame.pc + 1);
        //No captures on dreg register of CALL ins
    }

    @Override
    public void accept( Visitor visitor ) {
        visitor.visit( this );
    }

    @Override
    public Instruction translate( Translator t ) {
        int[] args = new int[sregs.length];
        for ( int i = 0; i < sregs.length; ++i )
            args[i] = t.translate( sregs[i] );
        return new CALL( t.translate( dreg ), t.translate( sreg ), args );
    }

    @Override
    public boolean reads( Symbol symbol ) {
        return true;
    }

    @Override
    public boolean writes( Symbol symbol ) {
        return true;
    }

    @Override
    public void addSymbolReadsTo( Collection<Symbol> symbols ) {
        symbols.add( Symbol.ANY );
    }

    @Override
    public void addSymbolWritesTo( Collection<Symbol> symbols ) {
        symbols.add( Symbol.ANY );
    }
}
