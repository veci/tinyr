package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.Frame;
import tinyr.runtime.Value;

/**
 */
public class LOAD_REG extends Instruction.OI {

    public LOAD_REG(int dreg, int sreg) {
        super(dreg, sreg);
    }

    @Override
    public void execute(Frame frame) {
        Value result = frame.get(sreg);
        frame.set(dreg, result);
        computePossibleAssumptions(frame, result, dreg, frame.pc + 1);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new LOAD_REG(t.translate(dreg), t.translate(sreg));
    }
}
