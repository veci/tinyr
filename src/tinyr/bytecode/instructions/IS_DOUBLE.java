package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class IS_DOUBLE extends Instruction.OI {

    public IS_DOUBLE(int dreg, int sreg) {
        super(dreg, sreg);
    }

    @Override
    public void execute(Frame frame) {
        double result = frame.get(sreg) instanceof Primitive ? 1 : 0;
        frame.set(dreg, new Primitive(result));
        computePossibleAssumptions(frame, new Primitive(result), dreg, frame.pc + 1);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new IS_DOUBLE(t.translate(dreg), t.translate(sreg));
    }
}
