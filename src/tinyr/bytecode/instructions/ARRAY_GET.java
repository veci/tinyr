package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class ARRAY_GET extends Instruction.OII {

    public ARRAY_GET(int dreg, int sreg, int sreg2) {
        super(dreg, sreg, sreg2);
    }

    @Override
    public void execute(Frame frame) {
        Array a = (Array) frame.get(sreg);
        int idx = (int) ((Primitive) frame.get(sreg2)).value;
        Value result = new Primitive(a.contents[idx - 1]); // use R's indexing starting from 1
        frame.set(dreg, result);
        computePossibleAssumptions(frame, result, dreg, frame.pc + 1);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new ARRAY_GET(t.translate(dreg), t.translate(sreg), t.translate(sreg2));
    }
}
