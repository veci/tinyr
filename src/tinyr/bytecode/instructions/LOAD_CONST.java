package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class LOAD_CONST extends Instruction.OImm {

    public LOAD_CONST(int dreg, double value) {
        super(dreg, new Primitive(value));
    }

    public LOAD_CONST(int dreg, Value value) {
        super(dreg, value);
    }

    @Override
    public void execute(Frame frame) {
        frame.set(dreg, value);
        computePossibleAssumptions(frame, value, dreg, frame.pc + 1);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new LOAD_CONST(t.translate(dreg), value);
    }
}
