package tinyr.bytecode.instructions;

import tinyr.Random;
import tinyr.bytecode.*;
import tinyr.runtime.*;

/** With given probability return 1, otherwise returns 0. The probability must be in range <0:1>.
 */
public class RANDOM extends Instruction.OI {

    public RANDOM(int dreg, int sreg) {
        super(dreg, sreg);
    }

    @Override
    public void execute(Frame frame) {
        Value threshold = frame.get(sreg);
        double result = Random.nextDouble() < ((Primitive)threshold).value ? 1 : 0;
        frame.set(dreg, new Primitive(result));
        computePossibleAssumptions(frame, new Primitive(result), dreg, frame.pc + 1);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new RANDOM(t.translate(dreg), t.translate(sreg));
    }
}
