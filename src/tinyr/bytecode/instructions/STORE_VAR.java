package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class STORE_VAR extends Instruction.WI {

    public STORE_VAR(Symbol name, int sreg) {
        super(name, sreg);
    }

    /** For testing purposes only
     */
    public STORE_VAR(String name, int sreg) {
        super(Symbol.make(name), sreg);
    }

    @Override
    public void execute(Frame frame) {
        // TODO guards are not implemented yet
        /*
        Guard.Global guard = Guard.Global.get(name);
        Value v = frame.get(sreg);
        frame.setVariable(name, v);
        if (guard == null) {
            Guard.Global.create(name);
        } else {
            guard.value.invalidate();
            if (guard.type.type != v.type()) guard.type.invalidate();
        } */
        Value v = frame.get(sreg);
        frame.setVariable(name, v);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new STORE_VAR(name, t.translate(sreg));
    }
}
