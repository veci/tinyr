package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class USE extends Instruction.As {

    public USE(Assumption assumption) {
        super(assumption);
    }

    @Override
    public void execute(Frame frame) {
        // Do nothing, USE just stands as code movement guard
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new USE(t.translate(assumption));
    }
}
