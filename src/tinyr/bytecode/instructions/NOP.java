package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.Frame;

/**
 */
public class NOP extends Instruction {
    @Override
    public void execute(Frame frame) {
        // pass
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new NOP();
    }
}
