package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class FDEF extends Instruction.OC {

    public FDEF(int dreg, Closure closure) {
        super(dreg, closure);
    }

    @Override
    public void execute(Frame frame) {
        frame.set(dreg, closure);
        computePossibleAssumptions(frame, closure, dreg, frame.pc + 1);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new FDEF(t.translate(dreg), closure);
    }
}
