package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class MUL extends Instruction.OII {

    public MUL(int dreg, int sreg, int sreg2) {
        super(dreg, sreg, sreg2);
    }

    @Override
    public void execute(Frame frame) {
        Value l = frame.get(sreg);
        Value r = frame.get(sreg2);        
        if (l == null || r == null)
          assert (false);
        double result = ((Primitive) l).value * ((Primitive) r).value;
        frame.set(dreg, new Primitive(result));
        computePossibleAssumptions(frame, new Primitive(result), dreg, frame.pc + 1);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new MUL(t.translate(dreg), t.translate(sreg), t.translate(sreg2));
    }
}
