package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class OR extends Instruction.OII {

    public OR(int dreg, int sreg, int sreg2) {
        super(dreg, sreg, sreg2);
    }

    @Override
    public void execute(Frame frame) {
        boolean l = ((Primitive) frame.get(sreg)).value != 0;
        boolean r = ((Primitive) frame.get(sreg2)).value != 0;
        double result = l || r ? 1 : 0;
        frame.set(dreg, new Primitive(result));
        computePossibleAssumptions(frame, new Primitive(result), dreg, frame.pc + 1);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new tinyr.bytecode.instructions.OR(t.translate(dreg), t.translate(sreg), t.translate(sreg2));
    }
}
