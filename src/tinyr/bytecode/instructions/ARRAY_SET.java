package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class ARRAY_SET extends Instruction.OII {
    public ARRAY_SET(int dreg, int sreg, int sreg2) {
        super(dreg, sreg, sreg2);
    }

    @Override
    public void execute(Frame frame) {
        Array a = (Array) frame.get(dreg);
        int idx = (int) ((Primitive) frame.get(sreg)).value;
        double v = ((Primitive) frame.get(sreg2)).value;
        Array aa = new Array(a.contents.length);
        System.arraycopy(a.contents, 0, aa.contents, 0, a.contents.length);
        aa.contents[idx - 1] = v; // use R's indexing starting from 1
        frame.set(dreg, aa);
        computePossibleAssumptions(frame, aa, dreg, frame.pc + 1);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new ARRAY_SET(t.translate(dreg), t.translate(sreg), t.translate(sreg2));
    }
}
