package tinyr.bytecode.instructions;

import javafx.util.Pair;
import tinyr.bytecode.Instruction;
import tinyr.bytecode.Translator;
import tinyr.bytecode.Visitor;
import tinyr.runtime.Assumption;
import tinyr.runtime.Closure;
import tinyr.runtime.Frame;

/**
 */
public class INV extends Instruction.As {

    public INV( Assumption assumption ) {
        super( assumption );
    }

    @Override
    public void execute( Frame frame ) {
        //Closure function = frame.closure;
        //Closure.prohibitedASMs.add(new Pair<Closure, Assumption>(function, assumption));
        //Closure.restoreFunctionsToTheBeginning();
        //System.out.println("RESTARTING ENVIRONMENT");
        //System.out.println( "Failed assumption " + assumption + " function : " + Frame.getKey(function) );
        assert (false) : "No more using INV instruction";
    }

    @Override
    public void accept( Visitor visitor ) {
        visitor.visit( this );
    }

    @Override
    public Instruction translate( Translator t ) {
        return new INV( t.translate( assumption ) );
    }
}