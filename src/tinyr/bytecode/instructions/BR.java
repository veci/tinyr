package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class BR extends Instruction.L {

    public BR(Label target) {
        super(target);
    }

    @Override
    public void execute(Frame frame) {
        frame.pc = frame.closure.body.labelAddress(address);
        // address must point to a label instruction, so increment in code.execute() will make it point to the correct
        // instruction to execute
        //frame.pc = frame.code.block(address).labelAddress();
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new BR(t.translate(address));
    }

    @Override
    public boolean isTerminating() {
        return true;
    }
}

