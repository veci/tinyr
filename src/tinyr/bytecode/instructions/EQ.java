package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class EQ extends Instruction.OII {

    public EQ(int dreg, int sreg, int sreg2) {
        super(dreg, sreg, sreg2);
    }

    @Override
    public void execute(Frame frame) {
        Value l = frame.get(sreg);
        Value r = frame.get(sreg2);
        int result = 0;
        if (l instanceof Closure && r instanceof Closure) {
            result = l == r ? 1 : 0;
        } else if (l instanceof Primitive && r instanceof Primitive) {
            result = ((Primitive) l).value == ((Primitive) r).value ? 1 : 0;
        }
        frame.set(dreg, new Primitive(result));
        computePossibleAssumptions(frame, new Primitive(result), dreg, frame.pc + 1);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new EQ(t.translate(dreg), t.translate(sreg), t.translate(sreg2));
    }


}
