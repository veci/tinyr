package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class SUB extends Instruction.OII {

    public SUB(int dreg, int sreg, int sreg2) {
        super(dreg, sreg, sreg2);
    }

    @Override
    public void execute(Frame frame) {
        Value l = frame.get(sreg);
        Value r = frame.get(sreg2);
        double result = ((Primitive) l).value - ((Primitive) r).value;
        frame.set(dreg, new Primitive(result));
        computePossibleAssumptions(frame, new Primitive(result), dreg, frame.pc + 1);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new SUB(t.translate(dreg), t.translate(sreg), t.translate(sreg2));
    }

}


