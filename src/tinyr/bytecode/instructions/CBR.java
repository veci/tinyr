package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class CBR extends Instruction.ILL {

    public CBR(int sreg, Label targetTrue, Label targetFalse) {
        super(sreg, targetTrue, targetFalse);
    }

    @Override
    public void execute(Frame frame) {
        // label address instead of the code address is used so that the +1 in main execution loop gets the proper code
        // start
        double p = ((Primitive) frame.get(sreg)).value;
        frame.pc = (p == 0) ? frame.closure.body.labelAddress(addressFalse) : frame.closure.body.labelAddress(addressTrue);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new CBR(t.translate(sreg), t.translate(addressTrue), t.translate(addressFalse));
    }

    @Override
    public boolean isTerminating() {
        return true;
    }

}
