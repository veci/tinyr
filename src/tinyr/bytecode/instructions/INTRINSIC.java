package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

import java.io.*;
import java.util.*;
import java.util.regex.*;

/**
 */
public class INTRINSIC extends Instruction.ORIArray {

    public INTRINSIC(int dreg, Symbol name, int[] sregs) {
        super(dreg, name, sregs);
    }

    static BufferedReader br = null;

    @Override
    public void execute(Frame frame) {
        if (name == Symbol.sqrt) {
            assert (sregs.length == 1);
            double d = ((Primitive) frame.get(sregs[0])).value;
            frame.set(dreg, new Primitive(Math.sqrt(d)));
            computePossibleAssumptions(frame, new Primitive(Math.sqrt(d)), dreg, frame.pc + 1);
        } else if (name == Symbol.c) {
            ArrayList<Object> result = new ArrayList<>();
            for (int i : sregs) {
                Value v = frame.get(i);
                if (v instanceof Primitive) {
                    result.add(((Primitive) v).value);
                } else if (v instanceof Array) {
                    for (double d : ((Array) v).contents)
                          result.add(d);
                }
            }
            frame.set(dreg, Array.fromCollection(result));
            computePossibleAssumptions(frame, Array.fromCollection(result), dreg, frame.pc + 1);
        } else if (name == Symbol.assertion) {
            assert (sregs.length == 1);
            double v = ((Primitive) frame.get(sregs[0])).value;
            assert (v != 0);
        } else if (name == Symbol.print) {
            for (int i : sregs) {
                System.out.print(frame.get(i));
                System.out.print(" ");
            }
            System.out.println("");
        } else if (name == Symbol.printStr) {
            for (int i : sregs) {
                System.out.print(((Array) frame.get(i)).toCharString());
                System.out.print(" ");
            }
            System.out.println("");
        } else if (name == Symbol.printRes) {
            double count = (double)((Primitive)frame.get(sregs[0])).value;
            double number = (double)((Primitive)frame.get(sregs[1])).value;
            int length = (int)((Primitive)frame.get(sregs[2])).value;
            StringBuilder s = new StringBuilder();
            for (int i = 0;i<length;i++){                
                s.append(Character.toChars((int)((Primitive)frame.get(sregs[3+i])).value));
            }
            System.out.println(s + " " + number*100/count);          
        }else if (name == Symbol.printHex) {
            for (int i : sregs) {
                int v = (int) ((Primitive) frame.get(i)).value;
                if (v < 16)
                    System.out.print("0");
                System.out.print(Integer.toHexString(v));
                System.out.print(" ");
            }
            System.out.println("");
        } else if (name == Symbol.iDiv) {
            assert (sregs.length == 2);
            int a = (int) ((Primitive) frame.get(sregs[0])).value;
            int b = (int) ((Primitive) frame.get(sregs[1])).value;
            frame.set(dreg, new Primitive(a/b));
            computePossibleAssumptions(frame, new Primitive(a/b), dreg, frame.pc + 1);
        } else if (name == Symbol.mod) {
            assert (sregs.length == 2);
            int a = (int) ((Primitive) frame.get(sregs[0])).value;
            int b = (int) ((Primitive) frame.get(sregs[1])).value;
            frame.set(dreg, new Primitive(a % b));
            computePossibleAssumptions(frame, new Primitive(a % b), dreg, frame.pc + 1);
        } else if (name == Symbol.pow) {
            assert (sregs.length == 2);
            double a = ((Primitive) frame.get(sregs[0])).value;
            double b = ((Primitive) frame.get(sregs[1])).value;
            frame.set(dreg, new Primitive(Math.pow(a, b)));
            computePossibleAssumptions(frame, new Primitive(Math.pow(a, b)), dreg, frame.pc + 1);
        } else if (name == Symbol.openFile) {
            assert (sregs.length == 1);
            String filename = ((Array) frame.get(sregs[0])).toCharString();
            if (br != null)
                throw new RuntimeException("Only one file may be opened at a time");
            try {
                br = new BufferedReader(new FileReader(filename));
                frame.set(dreg, new Primitive(1));
                computePossibleAssumptions(frame, new Primitive(1), dreg, frame.pc + 1);
            } catch (FileNotFoundException e) {
                System.out.println("Unable to open file " + filename);
                frame.set(dreg, new Primitive(0));
                computePossibleAssumptions(frame, new Primitive(0), dreg, frame.pc + 1);
            }

        } else if (name == Symbol.eof) {
            assert (sregs.length == 0);
            if (br == null)
                throw new RuntimeException("File not opened");
            try {
                int r = br.ready() ? 0 : 1;
                frame.set(dreg, new Primitive(r));
                computePossibleAssumptions(frame, new Primitive(r), dreg, frame.pc + 1);
            } catch (IOException e) {
                frame.set(dreg, new Primitive(1));
                computePossibleAssumptions(frame, new Primitive(1), dreg, frame.pc + 1);
            }
        } else if (name == Symbol.readLine) {
            assert (sregs.length == 0);
            if (br == null)
                throw new RuntimeException("File not opened");
            try {
                String line = br.readLine();
                if (line == null) {
                    frame.set(dreg, new Array(0));
                    computePossibleAssumptions(frame, new Array(0), dreg, frame.pc + 1);
                }
                else {
                    frame.set(dreg, Array.fromString(line));
                    computePossibleAssumptions(frame, Array.fromString(line), dreg, frame.pc + 1);
                }
            } catch (IOException e) {
                e.printStackTrace();
                frame.set(dreg, new Array(0));
                computePossibleAssumptions(frame, new Array(0), dreg, frame.pc + 1);
            }
        } else if (name == Symbol.closeFile) {
            assert (sregs.length == 0);
            if (br == null)
                throw new RuntimeException("File not opened");
            try {
                br.close();
                br = null;
                frame.set(dreg, new Primitive(1));
                computePossibleAssumptions(frame, new Primitive(1), dreg, frame.pc + 1);
            } catch (IOException e) {
                e.printStackTrace();
                frame.set(dreg, new Primitive(0));
                computePossibleAssumptions(frame, new Primitive(0), dreg, frame.pc + 1);
            }
        } else if (name == Symbol.strFind) {
            assert (sregs.length == 2);
            String s = ((Array) frame.get(sregs[0])).toCharString();
            String p = ((Array) frame.get(sregs[1])).toCharString();
            Pattern pt = Pattern.compile(p);
            Matcher m = pt.matcher(s);
            ArrayList<Object> result = new ArrayList<>();
            while (m.find())
                result.add((double) m.start());
            frame.set(dreg, Array.fromCollection(result));
            computePossibleAssumptions(frame, Array.fromCollection(result), dreg, frame.pc + 1);
        } else if (name == Symbol.strReplace) {
            assert (sregs.length == 3);
            String s = ((Array) frame.get(sregs[0])).toCharString();
            String p = ((Array) frame.get(sregs[1])).toCharString();
            String n = ((Array) frame.get(sregs[2])).toCharString();
            s = s.replaceAll(p, n);
            frame.set(dreg, Array.fromString(s));
            computePossibleAssumptions(frame, Array.fromString(s), dreg, frame.pc + 1);
        } else if (name == Symbol.time) {
            assert(sregs.length == 0);
            double currentTime = System.currentTimeMillis();
            frame.set(dreg, new Primitive(currentTime));
            computePossibleAssumptions(frame, new Primitive(currentTime), dreg, frame.pc + 1);
        } else {
            throw new RuntimeException("Unrecognized intrinsic function "+name.toString());
        }
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        int[] args = new int[sregs.length];
        for (int i = 0; i < sregs.length; ++i)
            args[i] = t.translate(sregs[i]);
        return new INTRINSIC(t.translate(dreg), name, args);
    }
}
