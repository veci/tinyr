package tinyr.bytecode.instructions;

import tinyr.bytecode.*;
import tinyr.runtime.*;

/**
 */
public class ASM extends Instruction.As {

    public ASM(Assumption assumption) {
        super(assumption);
    }

    @Override
    public void execute(Frame frame) {
        // noop
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Instruction translate(Translator t) {
        return new ASM(t.translate(assumption));
    }
}
