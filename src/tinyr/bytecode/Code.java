package tinyr.bytecode;

import tinyr.bytecode.instructions.*;
import tinyr.runtime.*;

import java.util.*;

import javafx.util.Pair;

/** Code within all possible optimization stages of a single closure.
 *
 * When a closure is created all its instructions are stored as record in the code object. Each record remembers a patch
 * that has created it, the instruction it contains and all patches that have removed it (i.e. the record itself is
 * never removed).
 *
 * As the function gets optimized, new records are added by and old ones flagged as removed by different patches. At any
 * point, a Code.View object can be constructed with a given signature (set of enabling patches) which produces the code
 * as it would have looked with only those patches applied.
 */
public class Code {

    /** The largest register that is being used in the code in any possible view.
     */
    int largestRegister;
    
    /** The largest register at the beginning, to be able to return after restarting
     */
    int originNumOfRegister;
    
    public Closure closure;

    /** Vector of all instruction records ever inserted to the code.
     */
    Vector<Record> records = new Vector<>();

    /** Creates the code from an array of instructions and the number of registers used.
     *
     * This is the information provided at the end of the lowering stage.
     */
    public Code(Instruction[] insns, int numOfRegisters) {
        this.largestRegister = numOfRegisters;
        this.originNumOfRegister = numOfRegisters;
        for (Instruction i : insns)
            this.records.add(new Record(i, null));
    }


    /** Creates the code out of an array of instructions.
     *
     * Only useful in tests.
     */
    public Code(Instruction[] insns) {
        new Filler().fill(insns);
    }


    /** Inserts the specified instruction as a new record to the code as part of the patch p.
     *
     * The record will be inserted in such way that it will be directly before the instruction before, or at the end of
     * the code, if before is null. The pcHint is the minimal index at which the before instruction can be found so that
     * we do not have to search from beginning all the time.
     *
     * TODO this is by far not efficient, but who cares atm
     */
    Record insert(Record rec, Record before, int pcHint) {
        if (before == null) {
            records.add(rec);
        } else {
            for (;pcHint < records.size(); ++pcHint)
                if (records.get(pcHint) == before) {
                    records.insertElementAt(rec, pcHint);
                    return rec;
                }
            assert (false) : "Insertion point not found";
        }
        return rec;
    }

    /** Returns the index of the given record. The pcHint is the minimal index the record must have, that is provided
     * for performance issues.
     */
    int recordIndex(Record change, int pcHint) {
        for ( ; pcHint < records.size() ; ++pcHint)
            if (records.get(pcHint) == change)
                return pcHint;
        assert (false);
        return -1;
    }

    /** Returns a default view to the code, that is a view with an empty signature.
     */
    public View getView() {
        return new View();
    }

    /** Returns a view to the code with the corresponding signature.
     */
    public View getView(Collection<Patch> signature) {
        return new View(signature);
    }

    /** Returns collection of all records stored in the code object in their global order.
     */
    public Collection<Record> records() {
        return records;
    }


    // =================================================================================================================

    /** A view into the code.
     *
     * The view keeps its actual records and block information and presents a view to the code based on its signature,
     * which is a collection of patches. The view then contains all instructions inserted by any of the signature
     * patches and not deleted by either of them.
     *
     * The view has methods for its execution and modification during the optimization phases.
     *
     * The basic block structure is kept synchronized as the code in the view changes.
     *
     * TODO the basic block structure does not properly reflect the registers and variables written at the moment.
     *
     * TODO the View does not support code movement. This code will be added when we have a use for it.
     *
     * (the code can be found in the Code2.java class in the same package)
     */

    public class View {

        HashSet<Patch> signature = new HashSet<>();

        Vector<Record> insns = new Vector<>();

        Vector<Block> blocks = new Vector<>();

        HashMap<Label, Block> labels = new HashMap<>();

        HashSet<Patch> openedPatches = new HashSet<>();

        /** Creates a view for the code with an empty signature (that is a view that only contains the initial
         * instructions for the code).
         */
        View() {
            setSignature(signature);
        }

        /** Creates a view for given signature.
         */
        View(Collection<Patch> signature) {
            this.signature.addAll(signature);
            setSignature(this.signature);
        }

        /** Returns the signature of the block. The signature consists of all patches used to produce the code.
         */
        public Collection<Patch> signature() {
            return signature;
        }

        /** Sets the signature for the view. Updates its insns array and the blocks information.
         */
        public void setSignature(Collection<Patch> signature) {
            if (this.signature != signature) {
                this.signature.clear();
                this.signature.addAll(signature);
            }
            insns.clear();
            for (Record r : records)
                if (r.belongsTo(this.signature))
                    insns.add(r);
            updateBlocks();
        }


        /** Sets the signature of the code view and updates its blocks.
         */
        private int setSignature(Collection<Patch> signature, int pcBefore) {
            if (this.signature != signature) {
                this.signature.clear();
                this.signature.addAll(signature);
            }
            insns.clear();
            int pcAfter = -2; 
            for (int i = 0; i < records.size(); ++i) {
                Record r = records.get(i);
                if (r.belongsTo(signature)) {
                    if (pcAfter == -2 && i >= pcBefore)
                        pcAfter = insns.size() - 1;
                    insns.add(r);
                }
            }
            updateBlocks();            
            return pcAfter;
        }

        /** Returns the number of instructions currently present in the view.
         */
        public int size() {
            return insns.size();
        }

        /** Returns the number of registers required to run the code.
         */
        public int numOfRegisters() {
            return largestRegister;
        }

        /** Returns the index-th instruction in the view.
         */
        public Instruction get(int index) {
            return insns.get(index).instruction;
        }

        /** Executes the code under the given view in the provided frame.
         */
        public Value execute(Frame frame) {
            frame.pc = 0; // to be sure
            while (frame.pc < size()) {
                get(frame.pc).execute(frame);
                ++frame.pc;
            }
            return frame.result;
        }

        /** For given pc, returns the index of the block that the instruction belongs to.
         */
        private int blockIndexForPC(int pc) {
            for (int i = 0; i < blocks.size(); ++i)
                if (blocks.get(i).end() >= pc)
                    return i;
            assert (false);
            return -1;
        }

        /** For a given pc, returns the block to which it belongs.
         */
        public Block blockForPC(int pc) {
            for (Block b : blocks)
                if (b.end() >= pc)
                    return b;
            assert (false);
            return null;
        }

        /** Returns a pc corresponding to the LABEL instrcution of the specified label.
         *
         * This is pc of the first instruction in the block - 1, which is a target address for all branches in the code.
         */
        public int labelAddress(Label label) {
            Block b = labels.get(label);
            return b.labelAddress();
        }

        /** Returns the block corresponding to the given label.
         */
        public Block block(Label label) {
            return labels.get(label);
        }

        // Editing instructions ----------------------------------------------------------------------------------------

        /** Opens the given patch for editing in the current code.
         *
         * The patch is made dependent on all patches in signature that are not opened at the same time and becomes part
         * of the signature itself.
         */
        public void open(Patch p) {
            assert (!openedPatches.contains(p));
            for (Patch pp: signature)
                if (! openedPatches.contains(pp))
                    pp.addDependentPatch(p);
            openedPatches.add(p);
            signature.add(p);
        }

        /** Creates a new patch and adds it to the signature and opened patches of the view.
         *
         * The patch is made dependent on all previously closed patches in the code.
         */
        public Patch openNewPatch() {
            Patch result = new Patch();
            open(result);
            return result;
        }

        /** Closes the current patch opened previously for editing.
         *
         * Returns true if the patch has done some changes to the code.
         *
         * Returns false if the patch has not been used, in which case it is also removed from the signature of the
         * view.
         */
        public boolean close(Patch p) {
            assert (openedPatches.contains(p));
            openedPatches.remove(p);
            if (p.isEmpty())
                signature.remove(p);
            return ! p.isEmpty();
        }

        /** Closes all opened patches. Returns true if at least one of the patches was not empty (that is if at least
         * one patch actually changed the code view). The empty patches are closed and removed from the signature.
         */
        public boolean closeAll() {
            boolean result = false;
            while (! openedPatches.isEmpty())
                result = close(openedPatches.iterator().next()) || result;
            return result;
        }

        /** Returns an index of next unused register.
         */
        public int newRegister() {
            return largestRegister++;
        }

        /** Creates n new registers in the code and returns the index of the first one of them.
         */
        public int newRegisters(int num) {
            int result = largestRegister;
            largestRegister += num;
            return result;
        }
        
        /**
         * Restores number of used registers to the beginning 
         */
        public void restoreLargestRegister() {
            largestRegister = originNumOfRegister;
        }

        /** Inserts the given record at the specified pc wrt the current view. The change will be associated with the
         * record.insertedBy patch.
         *
         * This method allows full control over the record inserted, but for normal insertions and deletions the
         * instruction specific methods below should be used.
         */

        public void insert(Record rec, int pc) {
            rec.insertedBy.empty = false;
            Code.this.insert(rec, pc == insns.size() ? null : insns.get(pc), pc);
            insns.insertElementAt(rec, pc);
            // if the instruction is label or a branch, just update all basic blocks.
            // TODO this is not efficient, but quite simple code
            if (rec.instruction instanceof LABEL || rec.instruction instanceof BR || rec.instruction instanceof CBR) {
                updateBlocks();
            } else {
                // insert the instruction
                int i = blockIndexForPC(pc);
                ++blocks.get(i).length;
                for ( ++i; i < blocks.size(); ++i )
                    ++blocks.get(i).codeStart;
            }
            
            if (rec.instruction instanceof tinyr.bytecode.instructions.ASM)
                rec.insertedBy.dependsOnAssumption(((tinyr.bytecode.instructions.ASM) rec.instruction).assumption);            
        }

        /** Inserts given instruction to the specified pc. The change will be associated with the specified patch.
         *
         * Creates a record which then is inserted.
         */
        public void insert(Instruction ins, int pc, Patch p) {
            p.empty = false;
            insert(new Record(ins, p), pc);                       
        }

        /** Removes the specified instruction from the view. The change will be associated with the specified patch.
         */
        public void remove(int pc, Patch p) {
            p.empty = false;
            Record ins = insns.get(pc);
            ins.removeInPatch(p);
            insns.remove(pc);
            if (ins.instruction instanceof LABEL) {
                updateBlocks();
            } else {
                int i = blockIndexForPC(pc);
                --blocks.get(i).length;
                for ( ++i; i < blocks.size(); ++i )
                    --blocks.get(i).codeStart;
            }
        }

        /** Outputs the current view to a string showing patches and instructions.
         */
        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("    signature:     ");
            for (Patch p : signature)
                sb.append(p);
            sb.append("    size:          " + insns.size()+ "\n");
            sb.append("    total changes: " + records.size() + "\n");
            sb.append("    registers:     " + largestRegister + "\n");
            sb.append("\n");
            sb.append("pc   p\n");
            for (int i = 0; i < insns.size(); ++i) {
                Record c = insns.get(i);
                sb.append(String.format("%-4s %-4s ", i, c.insertedBy == null ? " " : c.insertedBy));
                if (c.instruction instanceof LABEL) {
                    LABEL ins = (LABEL) c.instruction;
                    sb.append("LABEL " + labels.get(ins.address).index());
                } else if (c.instruction instanceof BR) {
                    BR ins = (BR) c.instruction;
                    sb.append("BR " + labels.get(ins.address).index());
                } else if (c.instruction instanceof CBR) {
                    CBR ins = (CBR) c.instruction;
                    sb.append("CBR " + ins.sreg + " " + labels.get(ins.addressTrue).index() + " " + labels.get(ins.addressFalse).index());
                } else {
                    sb.append(c.instruction.toString().replace("\n", "\n    "));
                }
                sb.append(c.instruction.ignoredAssumptionsToString());
                sb.append("\n");
            }
            return sb.toString();
        }

        /** Returns the instructions converted to string. This method is only expected to be used it tests as it
         * replaces label objects with label numbers for easy comparisons. Does not display any additional information.
         */
        public String insnsToString() {
            StringBuilder sb = new StringBuilder();
            for ( Record c : insns ) {
                if ( c.instruction instanceof LABEL ) {
                    LABEL ins = (LABEL) c.instruction;
                    sb.append("LABEL " + labels.get(ins.address).index() + "\n");
                } else if ( c.instruction instanceof BR ) {
                    BR ins = (BR) c.instruction;
                    sb.append("BR " + labels.get(ins.address).index() + "\n");
                } else if ( c.instruction instanceof CBR ) {
                    CBR ins = (CBR) c.instruction;
                    sb.append("CBR " + ins.sreg + " " + labels.get(ins.addressTrue).index() + " " + labels.get(ins.addressFalse).index() + "\n");
                } else {
                    sb.append(c.instruction.toString().replace("\n", "\n    "));
                    sb.append("\n");
                }
            }
            return sb.toString();
        }

        private Block getOrCreateBlock(Label l, HashMap<Label, Block> oldBlocks) {
            Block b = labels.getOrDefault(l, null);
            if (b == null) {
                b = oldBlocks.getOrDefault(l, null);
                if (b == null) {
                    b = new Block(l, -1);
                } else {
                    b.length = -1;
                    b.previous.clear();
                    b.next.clear();
                    b.registerReads.clear();
                    b.registerWrites.clear();
                    b.symbolReads.clear();
                    b.symbolWrites.clear();
                }
                labels.put(l, b);
            }
            return b;

        }

        @SuppressWarnings("unchecked")
        private void updateBlocks() {
            blocks.clear();
            // we keep the old blocks so that the block objects do not change over the update if possible
            // (the label -> block mapping will remain)
            HashMap<Label, Block> oldLabels = (HashMap<Label, Block>) labels.clone();
            labels.clear();
            Block current = new Block(null, 0);
            blocks.add(current);
            int pc = 0;
            for (Record r : insns) {
                Instruction i = r.instruction;
                if (i instanceof LABEL) {
                    Block b = getOrCreateBlock(((LABEL) i).address, oldLabels);
                    b.index = blocks.size();
                    blocks.add(b);
                    b.codeStart = pc + 1;
                    b.length = -1;
                    current = b;
                } else if (i instanceof BR) {
                    Block b = getOrCreateBlock(((BR) i).address, oldLabels);
                    current.addForwardLink(b);
                } else if (i instanceof CBR) {
                    Block b1 = getOrCreateBlock(((CBR) i).addressTrue, oldLabels);
                    Block b2 = getOrCreateBlock(((CBR) i).addressFalse, oldLabels);
                    current.addForwardLink(b1);
                    current.addForwardLink(b2);
                }
                ++pc;
                ++current.length;
                current.addInstruction(i);
            }
        }

        /** Returns the instructions in the current view as array of Instruction.
         *
         * This should not be used in production code, but only in tests as a shorthand for comparisons.
         */
        public Instruction[] insnsAsArray() {
            Instruction[] result = new Instruction[insns.size()];
            for (int i = 0; i < result.length; ++i)
                result[i] = insns.get(i).instruction;
            return result;
        }

        /** Returns the number of blocks in the current code view.
         */
        public int blocksCount() {
            return blocks.size();
        }

        /** Returns the underlying code object.
         */
        public Code codeObject() {
            return Code.this;
        }

        /** Invalidates patch and all its dependent patches by removing them from the view's signature.
         */
        private void invalidateInternal(Patch patch) {
            if (signature.contains(patch)) {
                signature.remove(patch);
                for ( Patch p : patch.dependentPatches )
                    invalidateInternal(p);
            }
        }

        /** Invalidates the given patch in the executable view.
         *
         * If the patch has any dependent patches, these are invalidated too. Given the current pc before the
         * invalidation, returns the pc corresponding to the same address after the invalidation took place. This means
         * that the returned pc + 1 will be the address of the next instruction to execute in the invalidated code.
         */
        public int invalidate(Patch patch, int pc) {
            if (!signature.contains(patch))
                return pc;
            pc = recordIndex(insns.get(pc), pc);
            invalidateInternal(patch);
            // all the patches have been removed from the signature,
            return setSignature(signature, pc);
        }

        /** Invalidates the given assumption.
         *
         * All patches dependent on the assumption and all patches depending on them will be invalidated. The pc
         * argument is the next instruction before the invalidation. The address of a corresponding pc of the next
         * instruction after the invalidation is returned.
         */
        public int invalidate(Assumption assumption, int pc) {
            pc = recordIndex(insns.get(pc), pc);
            for (Code.Patch p : assumption.dependents(Code.this))
                invalidateInternal(p);
            return setSignature(signature, pc);
        }

        /** Returns the i-th block in the current view, or null if the index is outside the view's range..
         */
        public Block block(int i) {
            if (i >= blocks.size())
                return null;
            return blocks.get(i);
        }
    }

    // =================================================================================================================

    private static int globalPatchIndex = 0;


    /** Patch to the code.
     *
     * Each record in the code must be associated with a patch that has inserted it and any of the patches which have
     * removed it from their respective views.
     *
     * Each patch knows a set of patches that depend on him (i.e. patches that must be invalidated when this one is)
     * and a list of assumptions it depends on (this is also mirrored in the assumptions).
     *
     * Patch also knows whether it is empty or not (an empty patch is a patch that did not change the code).
     *
     */
    public class Patch {


        public final int index = ++globalPatchIndex;

        final HashSet<Patch> dependentPatches = new HashSet<>();

        final HashSet<Assumption> dependsOnAssumptions = new HashSet<>();

        boolean empty = true;

        /** Marks the given patch as dependent on the current patch.
         */
        public void addDependentPatch(Patch p) {
            dependentPatches.add(p);
        }

        /** Returns true if the patch has not changed its code yet.
         */
        public boolean isEmpty() {
            return empty;
        }

        /** Converts the patch to string - just uses its index for simple reporting.
         */
        @Override
        public String toString() {
            return "P" + String.valueOf(index);
        }

        /** Returns the collection of patches that depend on the current patch.
         */
        public Collection<Patch> dependentPatches() {
            return dependentPatches;
        }

        /** Returns the collection of assumptions the current patch depends on.
         */
        public Collection<Assumption> dependsOnAssumptions() {
            return dependsOnAssumptions;
        }

        /** Adds new assumption to the list dependencies.
         */
        public void dependsOnAssumption(Assumption a) {
            a.addDependent(Code.this, this);
            dependsOnAssumptions.add(a);
        }
    }

    // =================================================================================================================

    /** Code block used in the analyses.
     *
     * The code block corresponds to a basic block where all but the first basic block in a function must start with
     * a LABEL instruction and all basic blocks must end with a terminating instruction that either returns from the
     * function, or transfers the control to another block.
     *
     * The blocks contain previous and next links which form a control flow graph among them.
     */
    public static class Block implements DDGObject {

        private int index = 0;

        public final Label address;

        int codeStart;

        int length;

        public final HashSet<Block> previous = new LinkedHashSet<>();

        public final HashSet<Block> next = new LinkedHashSet<>();

        /** Contains reference to the closest loop header for any given basic block. If the reference contained is the
         * block itself, the block is a loop header.
         */
        public Block loop;

        public int cStart;
        public int cEnd;


        /** A basic block is initialized by its label and pc of the first instruction excluding the LABEL instruction.
         *
         * For the first basic block in a function, the label is set to null.
         */
        public Block(Label address, int codeStart) {
            this.address = address;
            this.codeStart = codeStart;
            this.loop = null;
        }

        /** Adds a forward link from current block to the block specified. Updates the previous and next sets of the
         * two blocks.
         */
        public void addForwardLink(Block other) {
            next.add(other);
            other.previous.add(this);
        }

        /** Returns the address of the first non LABEL instruction of the block.
         */
        public int codeStart() {
            return codeStart;
        }

        /** Returns the address of the LABEL instruction for the block. Is undefined when called on the first block in
         * the function.
         */
        public int labelAddress() {
            assert (codeStart > 0) : "Cannot obtain label address from first block";
            return codeStart - 1;
        }

        /** Returns the address of the last instruction in the block.
         */
        public int end() {
            return codeStart + length;
        }

        /** Returns the index of the block.
         *
         * All blocks in the view are indexed in their order of appearance. This method returns this index.
         */
        public int index() {
            return index;
        }

        /** Returns the length of the block. This means the number of non LABEL instructions the block contains.
         */
        public int length() {
            return length;
        }

        /* Stuff from the DDGObject - bookkeeping read and written registers and symbols.
         */

        public final HashSet<Integer> registerReads = new HashSet<>();
        public final HashSet<Integer> registerWrites = new HashSet<>();
        public final HashSet<Symbol> symbolReads = new HashSet<>();
        public final HashSet<Symbol> symbolWrites = new HashSet<>();

        public final void addInstruction(DDGObject o) {
            o.addRegisterReadsTo(registerReads);
            o.addRegisterWritesTo(registerWrites);
            o.addSymbolReadsTo(symbolReads);
            o.addSymbolWritesTo(symbolWrites);
        }

        @Override
        public boolean reads(int register) {
            return registerReads.contains(register);
        }

        @Override
        public boolean writes(int register) {
            return registerWrites.contains(register);
        }

        @Override
        public boolean reads(Symbol symbol) {
            return symbolReads.contains(symbol);
        }

        @Override
        public boolean writes(Symbol symbol) {
            return symbolWrites.contains(symbol);
        }

        @Override
        public void addRegisterReadsTo(Collection<Integer> registers) {
            registers.addAll(registerReads);
        }

        @Override
        public void addRegisterWritesTo(Collection<Integer> registers) {
            registers.addAll(registerWrites);
        }

        @Override
        public void addSymbolReadsTo(Collection<Symbol> symbols) {
            symbols.addAll(symbolReads);
        }

        @Override
        public void addSymbolWritesTo(Collection<Symbol> symbols) {
            symbols.addAll(symbolWrites);
        }

    }

    // =================================================================================================================

    /** Instruction record.
     *
     * Contains the instruction itself, patch that has inserted the instruction to the code (null if the instruction has
     * been in the code from the beginning) and a set of all patches that have ever removed the instruction from the
     * code.
     */
    public static class Record {

        public final Instruction instruction;

        public final Patch insertedBy;

        public final HashSet<Patch> removedBy = new HashSet<>();

        /** Creates new instruction record for given instruction and marks it as added by the specified patch.
         */
        public Record(Instruction instruction, Patch insertedBy) {
            this.instruction = instruction;
            this.insertedBy = insertedBy;            
        }

        /** Marks the instruction as deleted by the given patch.
         */
        public final void removeInPatch(Patch p) {
            assert (! removedBy.contains(p));
            assert (p != insertedBy);
            removedBy.add(p);
        }

        /** Returns true if the record belongs to given patch. That is if the instruction has either been inserted by
         * the patch, or it has been in the code from the beginning and was not deleted by the patch.
         */
        public final boolean belongsTo(Patch p) {
            assert (p != null);
            return insertedBy == p || (insertedBy == null && ! removedBy.contains(p));
        }

        /** Returns true if the record is part of the given signature. That is if it has not been removed by any of the
         * patches in the signature and it has been inserted by one of them, or has been in the code from the beginning.
         */
        public final boolean belongsTo(Collection<Patch> signature) {
            if (! (insertedBy == null || signature.contains(insertedBy)))
                return false;
            for (Patch p : signature)
                if (removedBy.contains(p))
                    return false;
            return true;
        }

        /** Translates the record using given translator.
         *
         * Translates the record patches and the record's instruction using the specified translator.
         */
        public Record translate(Translator t) {
            return translate(t, instruction);
        }

        /** Translates the record given the translator and substitutes its instruction for the one given.
         */
        public Record translate(Translator t, Instruction ins) {
            Record result = new Record(ins, t.translate(insertedBy));
            for (Patch p : removedBy)
                result.removedBy.add(t.translate(p));
            return result;
        }
    }

    // =================================================================================================================

    /** A simple class that fills the code object with instructions and calculates the largest register used.
     *
     * It is useful for tests when code is constructed from Instruction arrays directly - otherwise this information is
     * supplied by the lowering engine.
     */
    private class Filler extends Visitor {

        void fill(Instruction[] insns) {
            largestRegister = -1;
            for (Instruction i : insns)
                i.accept(this);
        }

        private void updateLargestRegister(int reg) {
            if (largestRegister < reg)
                largestRegister = reg;
        }

        @Override
        public void visit(Instruction ins) {
            records.add(new Record(ins, null));
        }

        @Override
        public void visit(Instruction.I ins) {
            updateLargestRegister(ins.sreg);
            visit( (Instruction) ins);
        }

        @Override
        public void visit(Instruction.O ins) {
            updateLargestRegister(ins.dreg);
            visit( (Instruction) ins);
        }

        @Override
        public void visit(Instruction.OI ins) {
            updateLargestRegister(ins.dreg);
            updateLargestRegister(ins.sreg);
            visit( (Instruction) ins);
        }

        @Override
        public void visit(Instruction.OII ins) {
            updateLargestRegister(ins.dreg);
            updateLargestRegister(ins.sreg);
            updateLargestRegister(ins.sreg2);
            visit( (Instruction) ins);
        }

        @Override
        public void visit(Instruction.OIIArray ins) {
            updateLargestRegister(ins.dreg);
            updateLargestRegister(ins.sreg);
            for (int i : ins.sregs)
                updateLargestRegister(i);
            visit( (Instruction) ins);
        }

        @Override
        public void visit(Instruction.OR ins) {
            updateLargestRegister(ins.dreg);
            visit( (Instruction) ins);
        }

        @Override
        public void visit(Instruction.WI ins) {
            updateLargestRegister(ins.sreg);
            visit( (Instruction) ins);
        }

        @Override
        public void visit(Instruction.L ins) {
            visit( (Instruction) ins);
        }

        @Override
        public void visit(Instruction.ILL ins) {
            updateLargestRegister(ins.sreg);
            visit( (Instruction) ins);
        }

        @Override
        public void visit(Instruction.OC ins) {
            updateLargestRegister(ins.dreg);
            visit( (Instruction) ins);
        }

        @Override
        public void visit(Instruction.OImm ins) {
            updateLargestRegister(ins.dreg);
            visit( (Instruction) ins);
        }
    }
}
