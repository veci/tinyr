//package tinyr.bytecode;
//
//import tinyr.bytecode.instructions.*;
//import tinyr.runtime.*;
//
//import java.util.*;
//
///** Keeps the history of the code in a sequential form as opposed to the diffs for patches used by the standard Code
// * class.
// *
// * For the time being the code history works on instruction level granularity for the sake of simplicity, but basicBlocks
// * can be used freely, as long as each block is only insertions, or only deletions.
// *
// * TODO perhaps rewrite all collections to hashsets?
// *
// */
//public class Code2 {
//
//    public static class Patch {
//        private static int icount = 0;
//
//        public final int id;
//
//        public Patch() {
//            id = icount++;
//        }
//
//        HashSet<Patch> dependents = new HashSet<>();
//
//        public void addDependent(Patch p) {
//            dependents.add(p);
//        }
//
//    }
//
//    /** A view to the code object allowing its alterations.
//     *
//     * Each view contains the signature, which is a set of patches enabled in the current view into the code. It
//     * also contains a view vector, which holds all the code changes that are part of the view to the code
//     * in their increasing pc order.
//     *
//     * The view also keeps updated vector of the basic blocks present within the current view.
//     *
//     * Multiple independent views to a single code object can exist at a time. Multithreaded editing of the
//     * views is not supported, but should not be a problem in the future.
//     *
//     * The view allows simple changes to be made to the code under the given signature.
//     */
//
//    public class View {
//        /** Collection of patches that have produced the view to the code object that the manipulator is using.
//         */
//        Collection<Patch> signature;
//        /** View of the manipulator to the code. Contains a set of changes in their pc order. These changes are
//         * references to the Code changes.
//         */
//        Vector<Change> view;
//        /** Basic blocks in the view.
//         */
//        Vector<BasicBlock> basicBlocks;
//
//        /** Set of patches that are opened on the manipulator. These are the patches that can be used to add new
//         * changes or remove old ones.
//         */
//        Set<Patch> opened;
//
//        /** Sets the signature of the manipulator and calculates the basic blocks.
//         */
//        private void setSignature(Collection<Patch> signature) {
//            this.signature = signature;
//            view.clear();
//            for (Change c : changes)
//                if (c.isPresentIn(signature))
//                    view.add(c);
//            basicBlocks = BasicBlockEnumerator.getBasicBlocks(this);
//        }
//
//        private int setSignature(Collection<Patch> signature, int changePC) {
//            this.signature = signature;
//            view.clear();
//            int pcAfter = -1;
//            for (int i = 0; i < changes.size(); ++i) {
//                Change c = changes.get(i);
//                if (c.isPresentIn(signature)) {
//                    if (pcAfter == -1 && i >= changePC)
//                        pcAfter = view.size() - 1;
//                    view.add(c);
//                }
//            }
//            basicBlocks = BasicBlockEnumerator.getBasicBlocks(this);
//            return pcAfter;
//        }
//
//        /** Creates a manipulator corresponding to given signature.
//         */
//        public View(Collection<Patch> signature) {
//            view = new Vector<>();
//            basicBlocks = new Vector<>();
//            opened = new HashSet<>();
//            setSignature(signature);
//        }
//
//        public final int numOfRegisters() {
//            return numOfRegisters;
//        }
//
//
//        public final int size() {
//            return view.size();
//        }
//
//        public final Instruction get(int i) {
//            return view.get(i).instruction;
//        }
//
//        public final int blockSize() {
//            return basicBlocks.size();
//        }
//
//        public final BasicBlock block(int i) {
//            return basicBlocks.get(i);
//        }
//
//        public final Change[] changes() {
//            return view.toArray(new Change[view.size()]);
//        }
//
//        public final Instruction[] insns() {
//            Instruction[] result = new Instruction[view.size()];
//            for (int i = 0; i < view.size(); ++i)
//                result[i] = view.get(i).instruction;
//            return result;
//        }
//
//        public View getManipulator(Collection<Patch> signature) {
//            return Code2.this.getManipulator(signature);
//        }
//
//        public Value execute(Frame frame) {
//            frame.pc = 0; // to be sure
//            while (frame.pc < view.size()) {
//                get(frame.pc).execute(frame);
//                ++frame.pc;
//            }
//            return frame.result;
//        }
//
//
//        public void open(Patch p) {
//            assert (!opened.contains(p));
//            opened.add(p);
//            signature.add(p);
//        }
//
//        public Patch openNew() {
//            Patch result = new Patch();
//            opened.add(result);
//            return result;
//        }
//
//        public void close(Patch p) {
//            assert (opened.contains(p));
//            opened.remove(p);
//        }
//
//        public void closeAll() {
//            opened.clear();
//        }
//
//        private void addBBLink(BasicBlock from, int to) {
//            BasicBlock dest = basicBlocks.get(to);
//            from.next.add(dest);
//            dest.previous.add(from);
//        }
//
//        private void removeBBLink(BasicBlock from, int to) {
//            BasicBlock dest = basicBlocks.get(to);
//            from.next.remove(dest);
//            dest.previous.remove(from);
//        }
//
//        public BasicBlock blockForPC(int pc) {
//            for (BasicBlock b : basicBlocks) {
//                if (pc > b.labelAddress() && pc <= (b.end() + 1))
//                    return b;
//            }
//            return basicBlocks.lastElement();
//        }
//
//        private void insert_(Instruction ins, int pc, Patch p) {
//            assert (opened.contains(p));
//            Change c = new Change(ins, p);
//            // insert the change to the original code object
//            if (pc == view.size())
//                changes.add(c);
//            else
//                insertBefore(c, view.get(pc), pc);
//            // insert the change into the view object
//            view.insertElementAt(c, pc);
//        }
//
//        private void remove_(int pc, Patch p) {
//            assert (opened.contains(p));
//            Instruction ins = get(pc);
//            // remove the change from current view
//            view.get(pc).removed.add(p);
//            view.remove(pc);
//        }
//
//        private void updateBasicBlocks(int from, int diff, Patch p) {
//            for (int i = from; i < basicBlocks.size(); ++i) {
//                BasicBlock b = basicBlocks.get(i);
//                for (BasicBlock pb : b.previous) {
//                    Instruction ins = get(pb.end());
//                    if (ins instanceof BR2 ) {
//                        remove_(pb.end(), p);
//                        insert_(new BR2(b.index), pb.end(), p);
//                    } else {
//                        CBR2 cbr = (CBR2) ins;
//                        int aT = cbr.addressTrue == i - diff ? i : cbr.addressTrue;
//                        int aF = cbr.addressFalse == i - diff ? i : cbr.addressFalse;
//                        remove_(pb.end(), p);
//                        insert_(new CBR2(cbr.sreg, aT, aF), pb.end(), p);
//                    }
//                }
//            }
//        }
//
//        public void insert(Instruction ins, int pc, Patch p) {
//            insert_(ins, pc, p);
//            // update the basic blocks
//            BasicBlock b = blockForPC(pc);
//            ++b.length;
//            for (int i = b.index + 1; i < basicBlocks.size(); ++i)
//                ++basicBlocks.get(i).start;
//            // update basic block links if we are adding a terminating instruction
//            if (ins instanceof BR2 ) {
//                addBBLink(b, ((BR2) ins).address);
//            } else if (ins instanceof CBR2 ) {
//                addBBLink(b, ((CBR2) ins).addressTrue);
//                addBBLink(b, ((CBR2) ins).addressFalse);
//            }
//        }
//
//        /** Inserts new label and basic block to the code.
//         */
//        public int insertLabel(int into, Patch p) {
//            assert (opened.contains(p));
//            if (into == changes.size()) {
//                // if the inserted label will be last instruction, we only need to add it and add a new basic block
//                BasicBlock b = new BasicBlock(basicBlocks.size(), into + 1, 0);
//                LABEL2 l = new LABEL2(b.index);
//                insert_(l, view.size(), p);
//                basicBlocks.add(b);
//                return b.index;
//            } else {
//                // we are inserting the label in the middle of the code and we may even end up splitting an existing
//                // basic block. To make the code simple now, we will just insert the label, update the LABEL and
//                // terminating instructions to their new values and then recompute all basic blocks.
//                BasicBlock prev = blockForPC(into);
//                // prev basic block remains the same, walk all the blocks after the prev block and update their labels
//                // and any jumps to to their new value
//                for (int i = basicBlocks.size() - 1; i > prev.index ; --i) {
//                    BasicBlock b = basicBlocks.get(i);
//                    // update the LABEL instruction of the block to be +1 index
//                    remove_(b.labelAddress(), p);
//                    insert_(new LABEL2(b.index + 1), b.labelAddress(), p);
//                    // update all links to the block to point to the +1 index
//                    for (BasicBlock pb : b.previous) {
//                        Instruction ins = get(pb.end());
//                        if (ins instanceof BR2 ) {
//                            remove_(pb.end(), p);
//                            insert_(new BR2(i + 1), pb.end(), p);
//                        } else {
//                            CBR2 cbr = (CBR2) ins;
//                            int aT = cbr.addressTrue == i ? i + 1 : cbr.addressTrue;
//                            int aF = cbr.addressFalse == i ? i + 1 : cbr.addressFalse;
//                            remove_(pb.end(), p);
//                            insert_(new CBR2(cbr.sreg, aT, aF), pb.end(), p);
//                        }
//                    }
//                    // it is not necessary to update the basic block index, as it will be deleted anyways
//                }
//                // insert the label instruction at the appropriate
//                insert_(new LABEL2(prev.index + 1), into, p);
//                // recompute the basic blocks
//                basicBlocks = BasicBlockEnumerator.getBasicBlocks(this);
//                return prev.index + 1;
//            }
//        }
//
//        public void remove(int pc, Patch p) {
//            Instruction ins = get(pc);
//            if (ins instanceof LABEL2 ) {
//                removeLabel(pc, p);
//            } else {
//                remove_(pc, p);
//                // update the basic blocks
//                BasicBlock b = blockForPC(pc);
//                --b.length;
//                for ( int i = b.index + 1; i < basicBlocks.size(); ++i )
//                    --basicBlocks.get(i).start;
//                // update basic block links if we are removing a terminating instruction
//                if ( ins instanceof BR2 ) {
//                    removeBBLink(b, ((BR2) ins).address);
//                } else if ( ins instanceof CBR2 ) {
//                    removeBBLink(b, ((CBR2) ins).addressTrue);
//                    removeBBLink(b, ((CBR2) ins).addressFalse);
//                }
//            }
//        }
//
//        public void removeLabel(int pc, Patch p) {
//            LABEL2 l = (LABEL2) get(pc);
//            // first delete all instructions of the label
//            remove_(pc, p);
//            while (pc < view.size() && ! (get(pc) instanceof LABEL2))
//                remove_(pc, p);
//            BasicBlock removed = basicBlocks.get(l.address);
//            basicBlocks.remove(removed.index);
//            // update basic block links
//            for (BasicBlock n : removed.next)
//                n.previous.remove(removed);
//            for ( int i = removed.index; i < basicBlocks.size(); ++i ) {
//                BasicBlock bb = basicBlocks.get(i);
//                bb.start -= removed.length() + 1;
//                --bb.index;
//                remove_(bb.labelAddress(), p);
//                insert_(new LABEL2(bb.index), bb.labelAddress(), p);
//            }
//            updateBasicBlocks(removed.index, -1, p);
//        }
//
//        public int pcForChange(Change change) {
//            for (int i = 0; i < view.size(); ++i)
//                if (view.get(i) == change)
//                    return i;
//            assert (false);
//            return -1;
//        }
//
//        @Override
//        public String toString() {
//            StringBuilder sb = new StringBuilder();
//            sb.append("    signature:     ");
//            for (Patch p : signature)
//                sb.append(" "+p.id);
//            sb.append("    size:          " + view.size()+ "\n");
//            sb.append("    total changes: " + changes.size() + "\n");
//            sb.append("    registers:     " + numOfRegisters + "\n");
//            sb.append("\n");
//            sb.append("pc   p\n");
//            for (int i = 0; i < view.size(); ++i) {
//                Change c = view.get(i);
//                sb.append(String.format("%-4s %-4s %s\n", i, c.inserted == null ? " " : c.inserted.id, c.instruction.toString().replace("\n", "\n    ")));
//            }
//            return sb.toString();
//        }
//
//        public int newRegister() {
//            return numOfRegisters++;
//        }
//
//        public Code2 codeObject() {
//            return Code2.this;
//        }
//
//        private void invalidateInternal(Patch patch) {
//            if (signature.contains(patch)) {
//                signature.remove(patch);
//                for ( Patch p : patch.dependents )
//                    invalidateInternal(p);
//            }
//        }
//
//        /** Invalidates the given patch in the executable view.
//         *
//         * If the patch has any dependent patches, these are invalidated too. Given the current pc before the
//         * invalidation, returns the pc corresponding to the same address after the invalidation took place. This means
//         * that the returned pc + 1 will be the address of the next instruction to execute in the invalidated code.
//         */
//        public int invalidate(Patch patch, int pc) {
//            if (!signature.contains(patch))
//                return pc;
//            pc = changeIndex(view.get(pc), pc);
//            invalidateInternal(patch);
//            // all the patches have been removed from the signature,
//            return setSignature(signature, pc);
//        }
//
//        public int invalidate(Assumption assumption, int pc) {
//            pc = changeIndex(view.get(pc), pc);
//            for (Code2.Patch p : assumption.dependents(Code2.this))
//                invalidateInternal(p);
//            return setSignature(signature, pc);
//        }
//
//
//        public Collection<Patch> signature() {
//            return signature;
//        }
//    }
//
//    private int changeIndex(Change change, int pcHint) {
//        for ( ; pcHint < changes.size() ; ++pcHint)
//            if (changes.get(pcHint) == change)
//                return pcHint;
//        assert (false);
//        return -1;
//    }
//
//    /** Given a new change, a change before which it should be inserted and a hint saying that the change cannot occur
//     * before the given pc, finds the change before which the insertion should happen and performs it.
//     *
//     * If pcHint is greater than the number of changes, just adds the new change at the end.
//     *
//     * This method is used only internally. Use the modifiers for any user initiated code changes.
//     */
//    private void insertBefore(Change what, Change where, int pcHint) {
//        if (pcHint < changes.size())
//            for (; ; ++pcHint) {
//                if ( changes.get(pcHint) == where ) {
//                    changes.insertElementAt(what, pcHint);
//                    return;
//                }
//            }
//        else
//            changes.add(what);
//    }
//
//
///**
//    public class View {
//        Vector<Integer> transitions;
//        Vector<BasicBlock> basicBlocks;
//        Collection<Patch> patches;
//
//        /** When edited, the current patch. Null otherwise.
//         * /
//        Patch patch;
//
//        /** Index of a current pc's changeset before editing started so that when the editing view is closed the correct
//         * pc can be returned.
//         * /
//        int beforeChangeset;
//
//        boolean empty;
//
//        private final WeakReference<View> wref = new WeakReference<>(this);
//
//        public View(Collection<Patch> patches, Vector<Integer> transitions) {
//            this.transitions = transitions;
//            this.patches = patches;
//
//            addActiveCodeView(wref);
//        }
//
//        public Change[] activeChangesets() {
//            Change[] result = new Change[transitions.size()];
//            for (int i = 0; i < result.length; ++i)
//                result[i] = changes.get(transitions.get(i));
//            return result;
//        }
//
//        public Instruction changesetInstruction(int index) {
//            return changes.get(index).instruction;
//        }
//
//        public View(Collection<Patch> patches, Vector<Integer> transitions,  Vector<BasicBlock> blocks) {
//            this.transitions = transitions;
//            this.patches = patches;
//            this.basicBlocks = blocks;
//
//            addActiveCodeView(wref);
//        }
//
//        public void finalize() {
//            activeCodeViews.remove(wref);
//        }
//
//        @Deprecated
//        public Instruction[] insns() {
//            Instruction[] insns = new Instruction[transitions.size()];
//            for (int i = 0; i < insns.length; ++i)
//                insns[i] = changes.get(transitions.get(i)).instruction;
//            return insns;
//        }
//
//        public int numOfRegisters() {
//            return numOfRegisters;
//        }
//
//        public int getLabelAddress(int index) {
//            return basicBlocks.get(index).labelAddress();
//        }
//
//        /** Executes the code.
//         *
//         * Execution proceeds until the frame pc reaches the end of the code (or exceeds it). Note that for clarity reasons
//         * the implementation of the instruction execution is kept in each instruction's execute method rather than be used
//         * here.
//         *
//         * TODO This is a great source of inefficiency, but for now helps clarity a lot.
//         * /
//        public Value execute(Frame frame) {
//            frame.pc = 0; // to be sure
//            while (frame.pc < transitions.size()) {
//                changes.get(transitions.get(frame.pc)).instruction.execute(frame);
//                ++frame.pc;
//            }
//            return frame.result;
//        }
//
//        private void invalidateInternal(Patch patch) {
//            if (patches.contains(patch)) {
//                patches.remove(patch);
//                for ( Patch p : patch.dependents )
//                    invalidateInternal(p);
//            }
//        }
//
//        /** Invalidates the given patch in the executable view.
//         *
//         * If the patch has any dependent patches, these are invalidated too. Given the current pc before the
//         * invalidation, returns the pc corresponding to the same address after the invalidation took place. This means
//         * that the returned pc + 1 will be the address of the next instruction to execute in the invalidated code.
//         * /
//        public int invalidate(Patch patch, int pc) {
//            if (!patches.contains(patch))
//                return pc;
//            pc = transitions.get(pc);
//            invalidateInternal(patch);
//            reset(patches);
//            int result = -1; // if guard was the first instruction,
//            for (int i = 0; i < transitions.size(); ++i) {
//                if (transitions.get(i) > pc)
//                    break;
//                result = i;
//            }
//            return result;
//        }
//
//        /** Invalidates all patches dependent on the given assumption and returns the pc after the code changes.
//         * /
//        public int invalidate(Assumption assumption, int pc) {
//            Collection<Code.Patch> patches = assumption.dependents(Code.this);
//            if (patches == null)
//                return pc;
//            for (Code.Patch p : patches)
//                pc = invalidate(p, pc);
//            return pc;
//        }
//
//        @Override
//        public String toString() {
//            StringBuilder sb = new StringBuilder("Code Executable View\n");
//            sb.append("    changes:   " + changes.size()+ "\n");
//            sb.append("    signature: ");
//            for (Patch p : patches)
//                sb.append(" "+p.id);
//            sb.append("\n");
//            sb.append("pc   ch   p\n");
//            for (int i = 0; i < transitions.size(); ++i) {
//                Change c = changes.get(transitions.get(i));
//                sb.append(String.format("%-4s %-4s %-4s %s\n", i, transitions.get(i), c.inserted == null ? " " : c.inserted.id, c.instruction.toString().replace("\n", "\n    ")));
//            }
//            return sb.toString();
//        }
//
//        public int size() {
//            return transitions.size();
//        }
//
//        public Instruction get(int pc) {
//            return changes.get(transitions.get(pc)).instruction;
//        }
//
//        public Vector<Change> changes() {
//            return changes;
//        }
//
//        public boolean isPatchEmpty() {
//            assert (patch != null) : "Must be opened in editing view";
//            return empty;
//        }
//
//        /** Using the current set of applied patches, opens an editing view to it so that it can be modified. This is a
//         * faster version that creates a new patch, but otherwise picks at where the last editing view was closed.
//         * /
//        public Patch openForEditing(int pc) {
//            assert (patch == null) : "Previous code history's editing must first be closed";
//            empty = true;
//            this.beforeChangeset = transitions.get(pc);
//            patch = new Patch();
//            // no need to calculate the basic basicBlocks, they are already there
//            return patch;
//        }
//
//        public Patch openForEditing() {
//            return openForEditing(0);
//        }
//
//        /** Closes the editing view. Adds the current patch to the set of active patches.
//         * /
//        public int close() {
//            assert (patch != null) : "Must be opened in editing view";
//            patches.add(patch);
//            patch = null;
//            int result = -1; // if guard was the first instruction,
//            for (int i = 0; i < transitions.size(); ++i) {
//                if (transitions.get(i) > beforeChangeset)
//                    break;
//                result = i;
//            }
//            refactorActiveViews();
//            return result;
//        }
//
//        /** Removes the patch completely from the list of changes. Any instructions inserted by the patch will be
//         * removed forever and any instructions removed by the patch will be added.
//         *
//         * It is invalid to invalidate a patch if instruction added by that patch has already been removed by some other
//         * patch.
//         * /
//        public void discard(Patch p) {
//            assert (patch == null) : "Previous code history's editing must first be closed";
//            for (int i = 0; i < changes.size(); ) {
//                Change c = changes.get(i);
//                if (c.inserted == p) {
//                    assert (c.removed.isEmpty());
//                    changes.remove(i);
//                    continue;
//                } else if (c.removed.contains(patch)) {
//                    c.removed.remove(patch);
//                }
//                ++i;
//            }
//            if (patches.contains(p)) {
//                patches.remove(p);
//                reset(patches);
//            }
//        }
//
//        void reset(Collection<Patch> signature) {
//            Vector<Integer> result = new Vector<>();
//            for (int i = 0; i < changes.size(); ++i) {
//                Change c = changes.get(i);
//                if (c.isPresentIn(signature))
//                    result.add(i);
//            }
//            this.patches = signature;
//            this.transitions = result;
//            this.basicBlocks = BasicBlockEnumerator.getBasicBlocks(this);
//        }
//
//        private void resetForRecomputation(Collection<Patch> signature) {
//          Vector<Integer> result = new Vector<>();
//          for (int i = 0; i < changes.size(); ++i) {
//              Change c = changes.get(i);
//              if (c.isPresentIn(signature))
//                  result.add(i);
//          }
//          this.patches = signature;
//          this.transitions = result;
//      }
//
//        public Code codeHistory() {
//            return Code.this;
//        }
//
//        /** Removes the index-th instruction in the editing view as part of the current patch.
//         * /
//        public void remove(int index) {
//            assert (patch != null) : "Must be opened in editing view";
//            // TODO update patch dependencies here
//            Instruction ins = changes.get(transitions.get(index)).instruction;
//            if (ins instanceof LABEL) {
//                // we are deleting a basic block, first delete all instructions in it
//                removeInternal(index);
//                while (index < transitions.size()) {
//                    if (changes.get(transitions.get(index)).instruction instanceof LABEL)
//                        break;
//                    removeInternal(index);
//                }
//                // remove the basic code specified by the deleted label
//                BasicBlock b = basicBlocks.get(((LABEL) ins).address);
//                assert (b.previous.isEmpty());
//                // delete the links from the deleted basic code
//                for (BasicBlock bb : b.next) {
//                    assert (bb.previous.contains(b));
//                    bb.previous.remove(b);
//                }
//                // delete the basic code
//                basicBlocks.remove(b.index);
//                // offset the basic basicBlocks by the length of the removed basic code
//                offsetBasicBlocks(b.index, -(b.length() + 1)); // +1 for the label
//                // reindex the later basic basicBlocks to account for the removed one
//                reindexBasicBlocks(b.index);
//            } else {
//                // We are not changing basic block structure - shorten the current basic block
//                BasicBlock b = getBasicBlockForPC(index);
//                --b.length;
//                offsetBasicBlocks(b.index + 1, -1);
//                // if the instruction is BR or CBR, remove the basic block links
//                if (ins instanceof BR) {
//                    removeBasicBlockLink(b, ((BR) ins).address);
//                } else if (ins instanceof CBR) {
//                    removeBasicBlockLink(b, ((CBR) ins).addressTrue);
//                    removeBasicBlockLink(b, ((CBR) ins).addressFalse);
//                }
//                // mark the change as removed by the patch and remove the instruction from the transitions
//                removeInternal(index);
//            }
//        }
//
//        /** Inserts the specified instruction at the index-th position of the editing view.
//         * /
//        public void insert(int index, Instruction instruction) {
//            assert (! (instruction instanceof LABEL)) : "use insertLabel instead";
//            assert (patch != null) : "Must be opened in editing view";
//            // get the current basic block, make it longer and offset by one all following basicBlocks
//            BasicBlock b = getBasicBlockForPC(index);
//            ++b.length;
//            offsetBasicBlocks(b.index + 1, + 1);
//            // if the instruction is BR or CBR, add the basic block links
//            if (instruction instanceof BR ) {
//                insertBasicBlockLink(b, ((BR) instruction).address);
//            } else if (instruction instanceof CBR) {
//                insertBasicBlockLink(b, ((CBR) instruction).addressTrue);
//                insertBasicBlockLink(b, ((CBR) instruction).addressFalse);
//            }
//            // update the changes and the transitions vectors for the code.
//            insertInternal(index, instruction);
//        }
//
//        public int insertLabel(int pc) {
//            // check that we are adding right before another basic code, or at the very end of the code. This also
//            // prevents creating a basic code at pc 0 as there will never be a LABEL instruction (unless it would have
//            // been inserted by this method, which it cannot)
//            assert (pc == transitions.size() || get(pc) instanceof LABEL);
//            // get the previous basic code, since we do not insert to pc 0 there will always be one
//            BasicBlock previous = getBasicBlockForPC(pc);
//            // create the new basic code and insert it to the basic basicBlocks array
//            BasicBlock current = new BasicBlock(previous.index + 1, pc, 1);
//            basicBlocks.add(current.index, current);
//            // insert the label instruction for the new basic code
//            insertInternal(pc, new LABEL(current.index));
//            // offset the basic basicBlocks
//            offsetBasicBlocks(current.index + 1, 1);
//            // reindex the following basic basicBlocks
//            reindexBasicBlocks(current.index + 1);
//            // return the current basic code index
//            return current.index;
//        }
//
//
//        /** Moves instruction one instruction up. Returns true if the movement is successful, or false if the movement
//         * cannot be performed without changes to the program semantics.
//         *
//         * If the movement occurs within a single basic block, we only need to check that the instruction before is
//         * data independent.
//         *
//         * If the instruction goes from first position in a basic block to the last position (excluding the terminating
//         * instruction) in the previous basic basicBlocks, we must replicate the instruction to be in every basic block and
//         * make sure that it is data independent of the terminating instruction of the bb.
//         *
//         * Furthermore for each previous basic block we must make sure that if the basic block has also other children,
//         * the same instruction is present in them and can be moved to the new location too.
//         * /
//
//        public boolean moveUp(int pc) {
//            assert (pc != 0); // can't move first instruction
//            BasicBlock bb = getBasicBlockForPC(pc);
//            assert (pc != bb.labelAddress()); // can't move LABEL instruction
//            assert (pc != bb.end()); // can't move terminating instruction
//            Instruction ins = get(pc);
//            if (pc == bb.codeAddress()) {
//                SortedSet<Integer> toBeRemoved = new TreeSet<>();
//                HashSet<BasicBlock> notOnPath = new HashSet<>();
//                notOnPath.addAll(bb.previous);
//                toBeRemoved.add(pc);
//                for (BasicBlock prev : bb.previous)
//                    if (! canMoveToBB(prev, ins, toBeRemoved, notOnPath))
//                        return false;
//                // remove everything from toBeRemoved in reverse order
//                assert (toBeRemoved.contains(pc));
//                while (! toBeRemoved.isEmpty()) {
//                    int r = toBeRemoved.last();
//                    toBeRemoved.remove(r);
//                    remove(r);
//                }
//                // for all not in path, make sure instruction does not conflict with their terminating instruction
//                for (BasicBlock b : notOnPath)
//                    if (! ins.isDataIndependent(get(b.end())))
//                        return false;
//                // It is ok to duplicate the instruction itself as all the instructions can only contain final data and
//                // therefore having the instruction inserted multiple times in the code is not a problem
//                for (BasicBlock prev : notOnPath)
//                    insert(prev.end(), ins);
//            } else {
//                // if within a single BB, check that we can jump over the previous instruction by making sure they are
//                // independent
//                Instruction prev = get(pc - 1);
//                if (! ins.isDataIndependent(prev)) // we can't move if the instruction is not independent
//                    return false;
//                remove(pc);
//                insert(pc - 1, ins);
//            }
//            return true;
//        }
//
//        /** A version of the move up function that allows skipping multiple instructions at once.
//         *
//         * @param fromPc
//         * @param toPc
//         * @return
//         * /
//        public boolean moveUp(int fromPc, int toPc) {
//            assert (fromPc != 0); // can't move first instruction
//            BasicBlock from = getBasicBlockForPC(fromPc);
//            assert (fromPc != from.labelAddress()); // can't move LABEL instruction
//            assert (fromPc != from.end()); // can't move terminating instruction
//            BasicBlock to = getBasicBlockForPC(toPc);
//            Instruction ins = get(fromPc);
//            if (fromPc == toPc) {
//                // simple sequential movement, we only need to make sure all instructions we skip are data independent
//                for (int i = fromPc -1; i >= toPc; --i)
//                    if (! ins.isDataIndependent(get(i)))
//                        return false;
//                remove(fromPc);
//                insert(toPc, ins);
//            } else {
//                // check that once in the destination block we can move the instruction to the destination location
//                for (int i = to.end() - 1; i >= toPc; --i)
//                    if (! ins.isDataIndependent(get(i)))
//                        return false;
//                // check that an instruction can be moved to a destination basic block, this checks that the same
//                // instruction is on each path leading to destination bb in reversed CFG
//                SortedSet<Integer> toBeRemoved = new TreeSet<>();
//                HashSet<BasicBlock> notOnPath = new HashSet<>();
//                notOnPath.addAll(from.previous);
//                if (notOnPath.contains(to)) // final block can't be on the path, if the final block was the previous block of the source block
//                    notOnPath.remove(to);
//                if (! canMoveToBB(to, ins, toBeRemoved, notOnPath))
//                    return false;
//                // if neither of the instructions found is the one we want to move, it means it is always overshadowed
//                // by a different instruction that is definitely not data independent and therefore the move is not
//                // possible
//                if (! toBeRemoved.contains(fromPc))
//                    return false;
//                // remove everything from toBeRemoved in reverse order
//                while (! toBeRemoved.isEmpty()) {
//                    int r = toBeRemoved.last();
//                    toBeRemoved.remove(r);
//                    remove(r);
//                }
//                // for all not in path, make sure instruction does not conflict with their terminating instruction
//                for (BasicBlock b : notOnPath)
//                    if (! ins.isDataIndependent(get(b.end())))
//                        return false;
//                // It is ok to duplicate the instruction itself as all the instructions can only contain final data and
//                // therefore having the instruction inserted multiple times in the code is not a problem
//                for (BasicBlock prev : notOnPath)
//                    insert(prev.end(), ins);
//                insert(toPc, ins);
//            }
//            return true;
//        }
//
//        private void removeInternal(int index) {
//            empty = false;
//            changes.get(transitions.get(index)).removed.add(patch);
//            transitions.remove(index);
//        }
//
//        private void insertInternal(int index, Instruction instruction) {
//            empty = false;
//            // update the changes and the transitions vectors for the code.
//            if (index == transitions.size()) {
//                transitions.add(changes.size());
//                changes.add(new Change(instruction, patch));
//            } else {
//                int cindex = transitions.get(index);
//                if (cindex <= beforeChangeset)
//                    ++beforeChangeset;
//                changes.add(cindex, new Change(instruction, patch));
//                transitions.add(index, cindex);
//                for (++index; index < transitions.size(); ++index)
//                    transitions.set(index, transitions.get(index) + 1);
//            }
//        }
//
//        /** Returns basic code for current program counter in the editing view.
//         *
//         * The method does assume that basic basicBlocks are stored in ascending pc order and that they cover the full code.
//         * Therefore checking only for the first basic code that extends to the pc is enough.
//         * /
//         public BasicBlock getBasicBlockForPC(int pc) {
//            for (BasicBlock b : basicBlocks ) {
//                if (b.end() + 1 >= pc)
//                    return b;
//            }
//            assert (false);
//            return null;
//        }
//
//        /** Starting at given basic code, updates it and all subsequent basic code start addresses by the given
//         * delta.
//         * /
//        private void offsetBasicBlocks(int startIndex, int codeOffsetDelta) {
//            for (; startIndex < basicBlocks.size(); ++startIndex)
//                basicBlocks.get(startIndex).start += codeOffsetDelta;
//        }
//
//        private void insertBasicBlockLink(BasicBlock from, int to) {
//            BasicBlock second = basicBlocks.get(to);
//            assert (!second.previous.contains(from));
//            second.previous.add(from);
//            assert (!from.next.contains(second));
//            from.next.add(second);
//        }
//
//        private void removeBasicBlockLink(BasicBlock from, int to) {
//            BasicBlock second = basicBlocks.get(to);
//            assert (second.previous.contains(from));
//            second.previous.remove(from);
//            assert (from.next.contains(second));
//            from.next.remove(second);
//        }
//
//        /** Given the codeAddress index to the basic basicBlocks array, reindexes the code and all subsequent ones.
//         *
//         * The reindexing is automatic and matches the position of the basic code in the basic basicBlocks array. Reindexing
//         * the code means updating its LABEL instruction to contain the proper number as well as updating terminating
//         * instructions from the previous basic basicBlocks to jump to the correct new basic code id.
//         * /
//        private void reindexBasicBlocks(int startIndex) {
//            for (; startIndex < basicBlocks.size(); ++startIndex) {
//                BasicBlock b = basicBlocks.get(startIndex);
//                // update the label instruction to reference the proper code index
//                removeInternal(b.labelAddress());
//                insertInternal(b.labelAddress(), new LABEL(startIndex));
//                // for each previous code, update its terminating instruction to correctly point to the new index
//                for (BasicBlock bb : b.previous) {
//                    Instruction tins = get(bb.end());
//                    // the terminating instruction can only be BR or CBR
//                    if (tins instanceof BR) {
//                        assert ((BR) tins).address == b.index;
//                        removeInternal(bb.end());
//                        insertInternal(bb.end(), new BR(startIndex));
//                    } else {
//                        CBR cbr = (CBR) tins;
//                        assert (cbr.addressTrue == b.index) || (cbr.addressFalse == b.index);
//                        removeInternal(bb.end());
//                        insertInternal(bb.end(), new CBR(cbr.sreg,
//                                cbr.addressTrue == b.index ? startIndex : cbr.addressTrue,
//                                cbr.addressFalse == b.index ? startIndex : cbr.addressFalse));
//                    }
//                }
//                b.index = startIndex;
//            }
//        }
//
//        /** Finds given instruction in the basic block or its successors that can be moved up (ie that is independent
//         * of the code before it.
//         *
//         * Returns true and stores the list of such instructions (there must be one for each possible control path) if
//         * successful, false otherwise.
//         *
//         * TODO this can mark basic basicBlocks that have already been visited for quicker progression.
//         * /
//        private boolean findDataIndependentInstructions(BasicBlock b, Instruction ins, Collection<Integer> pcs, Collection<BasicBlock> notOnPath) {
//            if (notOnPath.contains(b))
//                notOnPath.remove(b);
//            // walk all instructions in the BB and make sure they are either identical to the instruction we are looking
//            // for, or they are data independent with it.
//            for (int i = b.codeAddress(); i <= b.end(); ++i) {
//                Instruction ii = get(i);
//                if (ins.equals(ii)) {
//                    pcs.add(i);
//                    return true;
//                }
//                if (! ins.isDataIndependent(ii))
//                    return false;
//            }
//            for (BasicBlock bb : b.next)
//                if (! findDataIndependentInstructions(bb, ins, pcs, notOnPath))
//                    return false;
//            return true;
//        }
//
//
//        private boolean canMoveToBB(BasicBlock bb, Instruction ins, Collection<Integer> pcs, Collection<BasicBlock> notOnPath) {
//            for (BasicBlock b : bb.next)
//                if (! findDataIndependentInstructions(b, ins, pcs, notOnPath))
//                    return false;
//            return true;
//        }
//
//
//        public int basicBlocksSize() {
//            return basicBlocks.size();
//        }
//
//        public Collection<Patch> signature() {
//            return patches;
//        }
//
//        public BasicBlock getBasicBlock(int bbIndex) {
//            return basicBlocks.get(bbIndex);
//        }
//
//        public int pcForChangeset(Change change) {
//            for (int i = 0; i < transitions.size(); ++i)
//                if (changes.get(transitions.get(i)) == change)
//                    return i;
//            assert (false);
//            return -1;
//        }
//    } */
//
//
//    /** A change to the code.
//     *
//     * Change is always represented by an instruction. The instruction is inserted by a single patch, but multiple
//     * patches may in time remove the instruction. The instruction, patch that has inserted it and all patches that have
//     * removed it are all part of the Change object.
//     */
//    public static class Change {
//
//        public final Instruction instruction;
//        public final Patch inserted;
//        public final HashSet<Patch> removed = new HashSet<>();
//
//        public Change(Instruction instruction, Patch patch) {
//            this.instruction = instruction;
//            this.inserted = patch;
//        }
//
//        /** Given a collection of patches applied, returns true if the change (inserted instruction) should be present in
//         * the final code.
//         *
//         * This means that either the inserting patch is in the patches, or that the instruction has been in the code
//         * from the beginning and that none of the patches that have removed the instruction is present in the patches.
//         */
//        public boolean isPresentIn(Collection<Patch> patches) {
//            if (inserted != null && ! patches.contains(inserted))
//                return false;
//            for (Patch p : removed)
//                if (patches.contains(p))
//                    return false;
//            return true;
//        }
//    }
//
//    /** Basic block information.
//     *
//     * Each basic block is a DDG object that aggregates all properties of its instructions. Basic block starts with a
//     * LABEL instruction (with the exception of the first basic block in a function that does not require the label
//     * instruction) followed by the optional non-terminating instructions and is finished by a terminating instruction,
//     * currently only RET, BR and CBR.
//     */
//    public static class BasicBlock implements DDGObject {
//        int index;
//        int start;
//        int length;
//
//        public final HashSet<BasicBlock> previous = new HashSet<>();
//        public final HashSet<BasicBlock> next = new HashSet<>();
//
//        public final HashSet<Integer> registerReads = new HashSet<>();
//        public final HashSet<Integer> registerWrites = new HashSet<>();
//
//        public final HashSet<Symbol> symbolReads = new HashSet<>();
//        public final HashSet<Symbol> symbolWrites = new HashSet<>();
//
//        public BasicBlock(int index, int start, int length) {
//            this.index = index;
//            this.start = start;
//            this.length = length;
//        }
//
//
//        @Override
//        public String toString() {
//            return "BB " + index + ", codeAddress " + start + ", length " + length;
//        }
//
//        public final int index() {
//            return index;
//        }
//
//        public final int labelAddress() {
//            return start - 1;
//        }
//
//        public final int codeAddress() {
//            return start;
//        }
//
//        public final int length() {
//            return length;
//        }
//
//        public final int end() {
//            return start + length - 1;
//        }
//
//        @Override
//        public boolean reads(int register) {
//            return registerReads.contains(register);
//        }
//
//        @Override
//        public boolean writes(int register) {
//            return registerWrites.contains(register);
//        }
//
//        @Override
//        public boolean reads(Symbol symbol) {
//            return symbolReads.contains(symbol);
//        }
//
//        @Override
//        public boolean writes(Symbol symbol) {
//            return symbolWrites.contains(symbol);
//        }
//
//        @Override
//        public void addRegisterReadsTo(Collection<Integer> registers) {
//            registers.addAll(registerReads);
//        }
//
//        @Override
//        public void addRegisterWritesTo(Collection<Integer> registers) {
//            registers.addAll(registerWrites);
//        }
//
//        @Override
//        public void addSymbolReadsTo(Collection<Symbol> symbols) {
//            symbols.addAll(symbolReads);
//        }
//
//        @Override
//        public void addSymbolWritesTo(Collection<Symbol> symbols) {
//            symbols.addAll(symbolWrites);
//        }
//    }
//
//
//
//    Vector<Change> changes = new Vector<>();
//
//    int numOfRegisters;
//
//
//    /** Creates the code history object from the initial instructions.
//     *
//     */
//    public Code2(Instruction... instructions) {
//        for (Instruction i : instructions) {
//            changes.add(new Change(i, null));
//        }
//        numOfRegisters = RegisterEnumerator.getMaxReg(instructions);
//    }
//
//    public Code2(Instruction[] instructions, int numOfRegisters) {
//        for (Instruction i : instructions) {
//            changes.add(new Change(i, null));
//        }
//        this.numOfRegisters = numOfRegisters;
//    }
//
//
//    /** Given the set of patches, returns the sequential instructions that will be produced when all these will be
//     * applied in their original order to the initial set of instructions.
//     */
//    public View getManipulator(Collection<Patch> patches) {
//        return new View(patches);
//    }
//
//    public View getManipulator() {
//        return new View(new HashSet<Patch>());
//    }
//
//    /** Enumerates the basic basicBlocks found in the code code.
//     *
//     * Each basic code is identified by its index and its length. This later allows for easier instruction to basic
//     * code matching.
//     *
//     * TODO after certain optimizations the basic code structure has holes. This is because each basic code but the
//     * first one is introduced by a label instruction with given index. When a label instruction together with its
//     * basic code is removed, the label's number will become a hole. This has to be fixed in the future.
//     *
//     * The basic basicBlocks automatically store a control flow graph amongst them.
//     */
//    static class BasicBlockEnumerator extends Visitor.Sequential {
//        Vector<BasicBlock> bbs = new Vector<>();
//
//        BasicBlock currentBlock;
//
//        static Vector<BasicBlock> getBasicBlocks(Instruction[] code) {
//            BasicBlockEnumerator e = new BasicBlockEnumerator();
//            e.visit(code);
//            e.checkStructure();
//            return e.bbs;
//        }
//
//        static Vector<BasicBlock> getBasicBlocks(View code) {
//            BasicBlockEnumerator e = new BasicBlockEnumerator();
//            e.visit(code);
//            e.checkStructure();
//            return e.bbs;
//        }
//
//        private BasicBlockEnumerator() {
//            currentBlock = new BasicBlock(0, 0, 0);
//            bbs.add(currentBlock);
//        }
//
//
//        /** Checks that the basic basicBlocks array is minimal and does not contain non-existing empty basicBlocks represented
//         * by null.
//         *
//         * This can happen after code changes deleting entire basicBlocks but not changing the code indices.
//         */
//        public void checkStructure() {
//            for (BasicBlock b : bbs)
//                if (b == null)
//                    assert (b != null);
//        }
//
//        private BasicBlock get(int index) {
//            if (index + 1 > bbs.size())
//                bbs.setSize(index + 1);
//            BasicBlock b = bbs.get(index);
//            if (b == null) {
//                b = new BasicBlock(index, -1, -1);
//                bbs.set(index, b);
//            }
//            return b;
//        }
//
//        private void addLink(int to) {
//            BasicBlock toBlock = get(to);
//            currentBlock.next.add(toBlock);
//            toBlock.previous.add(currentBlock);
//        }
//
//        private void terminateCurrentBlock() {
//            assert (currentBlock.length == pc() - currentBlock.start + 1);
//            // we don't do this for simpler testing that does not have to follow the bb structure.
//            //currentBlock = null;
//        }
//
//        @Override
//        public void visit(Instruction ins) {
//            ins.addRegisterReadsTo(currentBlock.registerReads);
//            ins.addRegisterWritesTo(currentBlock.registerWrites);
//            ins.addSymbolReadsTo(currentBlock.symbolReads);
//            ins.addSymbolWritesTo(currentBlock.symbolWrites);
//            ++currentBlock.length;
//        }
//
//        @Override
//        public void visit(LABEL2 ins) {
//            //assert (currentBlock == null) : "Malformed basic code structure. LABEL after non terminating instruction";
//            currentBlock = get(ins.address);
//            currentBlock.start = pc() + 1;
//            super.visit(ins);
//        }
//
//        @Override
//        public void visit(RET ins) {
//            assert (currentBlock != null) : "Malformed basic code structure. RET instruction outside basic code";
//            super.visit(ins);
//            terminateCurrentBlock();
//        }
//
//        @Override
//        public void visit(BR2 ins) {
//            assert (currentBlock != null) : "Malformed basic code structure. BR instruction outside basic code";
//            super.visit(ins);
//            addLink(ins.address);
//            terminateCurrentBlock();
//        }
//
//        @Override
//        public void visit(CBR2 ins) {
//            assert (currentBlock != null) : "Malformed basic code structure. CBR instruction outside basic code";
//            super.visit(ins);
//            addLink(ins.addressTrue);
//            addLink(ins.addressFalse);
//            terminateCurrentBlock();
//        }
//
//    }
//
//    /** Enumerates all registers used by the code and returns the maximum one.
//     *
//     * This is used in the debugging constructor.
//     */
//    static class RegisterEnumerator extends Visitor.Sequential {
//        int maxReg = -1;
//
//        static int getMaxReg(Instruction[] code) {
//            RegisterEnumerator e = new RegisterEnumerator();
//            e.visit(code);
//            return e.maxReg;
//        }
//
//        @Override
//        public void visit(Instruction.I ins) {
//            maxReg = Math.max(maxReg, ins.sreg);
//        }
//
//        @Override
//        public void visit(Instruction.O ins) {
//            maxReg = Math.max(maxReg, ins.dreg);
//        }
//
//        @Override
//        public void visit(Instruction.OI ins) {
//            maxReg = Math.max(maxReg, ins.sreg);
//            visit((Instruction.O) ins);
//        }
//
//        @Override
//        public void visit(Instruction.OII ins) {
//            maxReg = Math.max(maxReg, ins.sreg2);
//            visit((Instruction.OI) ins);
//        }
//
//        @Override
//        public void visit(Instruction.OR ins) {
//            visit((Instruction.O) ins);
//        }
//
//        @Override
//        public void visit(Instruction.WI ins) {
//            visit((Instruction.I) ins);
//        }
//
//        @Override
//        public void visit(Instruction.IAA ins) {
//            visit((Instruction.I) ins);
//        }
//
//        @Override
//        public void visit(Instruction.OC ins) {
//            visit((Instruction.O) ins);
//        }
//
//        @Override
//        public void visit(Instruction.OImm ins) {
//            visit((Instruction.O) ins);
//        }
//    }
//
//}
//
//
