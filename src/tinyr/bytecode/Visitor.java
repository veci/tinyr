package tinyr.bytecode;

import tinyr.analysis.UseInsertedException;
import tinyr.bytecode.instructions.*;

/** Visitor pattern for all instructions.
 *
 * The visitor is hierarchical, being first for the specific instruction, then for the encoding and finally for the
 * general Instruction.
 *
 * Several visitors that are of general use are implemented as static classes here.
 */
public class Visitor {

    public void visit(Instruction ins) {
        // pass
    }

    public void visit(Instruction.I ins) {
        visit( (Instruction) ins);
    }
    
    public void visit(Instruction.INL ins) {
        visit( (Instruction) ins);
    }

    public void visit(Instruction.O ins) {
        visit( (Instruction) ins);
    }

    public void visit(Instruction.OI ins) {
        visit( (Instruction) ins);
    }

    public void visit(Instruction.OII ins) {
        visit( (Instruction) ins);
    }

    public void visit(Instruction.OIIArray ins) {
        visit( (Instruction) ins);
    }

    public void visit(Instruction.ORIArray ins) {
        visit( (Instruction) ins);
    }

    public void visit(Instruction.OR ins) {
        visit( (Instruction) ins);
    }

    public void visit(Instruction.WI ins) {
        visit( (Instruction) ins);
    }

    public void visit(Instruction.L ins) {
        visit( (Instruction) ins);
    }

    public void visit(Instruction.ILL ins) {
        visit( (Instruction) ins);
    }

    public void visit(Instruction.OC ins) {
        visit( (Instruction) ins);
    }

    public void visit(Instruction.OImm ins) {
        visit( (Instruction) ins);
    }

    public void visit(Instruction.As ins) {
        visit( (Instruction) ins);
    }

    public void visit(ADD ins) {
        visit((Instruction.OII) ins);
    }
    public void visit(AND ins) {
        visit((Instruction.OII) ins);
    }

    public void visit(ARRAY_CREATE ins) {
        visit((Instruction.OII) ins);
    }

    public void visit(ARRAY_GET ins) {
        visit((Instruction.OII) ins);
    }

    public void visit(ARRAY_LENGTH ins) {
        visit((Instruction.OI) ins);
    }

    public void visit(ARRAY_SET ins) {
        visit((Instruction.OII) ins);
    }

    public void visit(ASM ins) {
        visit((Instruction.As) ins);
    }

    public void visit(BKPT ins) {
        visit((Instruction) ins);
    }

    public void visit(BR ins) {
        visit((Instruction.L) ins);
    }

    public void visit(CALL ins) throws UseInsertedException {
        visit((Instruction.OIIArray) ins);
    }

    public void visit(CBR ins) {
        visit((Instruction.ILL) ins);
    }

    public void visit(DIV ins) {
        visit((Instruction.OII) ins);
    }
    
    public void visit(END ins) {
	visit((Instruction.INL) ins);
    }

    public void visit(EQ ins) {
        visit((Instruction.OII) ins);
    }

    public void visit(FDEF ins) {
        visit((Instruction.OC) ins);
    }

    public void visit(GT ins) {
        visit((Instruction.OII) ins);
    }

    public void visit(GTE ins) {
        visit((Instruction.OII) ins);
    }

    public void visit(INTRINSIC ins) {
        visit((Instruction.ORIArray) ins);
    }

    public void visit(INV ins) {
        visit((Instruction.As) ins);
    }

    public void visit(IS_ARRAY ins) {
        visit((Instruction.OI) ins);
    }

    public void visit(IS_CLOSURE ins) {
        visit((Instruction.OI) ins);
    }

    public void visit(IS_DOUBLE ins) {
        visit((Instruction.OI) ins);
    }

    public void visit(LABEL ins) {
        visit((Instruction.L) ins);
    }

    public void visit(LOAD_CONST ins) {
        visit((Instruction.OImm) ins);
    }

    public void visit(LOAD_REG ins) {
        visit((Instruction.OI) ins);
    }

    public void visit(LOAD_VAR ins) {
        visit((Instruction.OR) ins);
    }

    public void visit(LT ins) {
        visit((Instruction.OII) ins);
    }

    public void visit(LTE ins) {
        visit((Instruction.OII) ins);
    }

    public void visit(MUL ins) {
        visit((Instruction.OII) ins);
    }

    public void visit(NEQ ins) {
        visit((Instruction.OII) ins);
    }

    public void visit(NOP ins) {
        visit((Instruction) ins);
    }

    public void visit(OR ins) {
        visit((Instruction.OII) ins);
    }

    public void visit(RANDOM ins) {
        visit((Instruction.OI) ins);
    }

    public void visit(RET ins) {
        visit((Instruction.I) ins);
    }
    
    public void visit(START ins) {
	visit((Instruction.INL) ins);
    }

    public void visit(STORE_VAR ins) {
        visit((Instruction.WI) ins);
    }

    public void visit(SUB ins) {
        visit((Instruction.OII) ins);
    }
    public void visit(USE ins) {
        visit((Instruction.As) ins);
    }
}
