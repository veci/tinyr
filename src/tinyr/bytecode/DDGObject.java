package tinyr.bytecode;

import tinyr.runtime.Symbol;

import java.util.Collection;

/** Data Dependency Graph Object
 *
 * TODO this is a bad name
 *
 * DDG object contains information about its changes to the state. It can answer which registers and variables the
 * object reads or modifies.
 *
 * Currently, the Instruction and Code.BasicBlock classes implement this interface for easier code movement.
 *
 */
public interface DDGObject {
    // TODO shouldn't they be named loads & stores for compatibility?
    // They are now reads / writes with accordance to the old code

    /** Returns true if the instruction reads from the given register. False otherwise.
     */
    boolean reads(int register);

    /** Returns true if the instruction writes to the given register. False otherwise.
     */
    boolean writes(int register);

    /** Returns true if the instruction reads from the given symbol (variable). False otherwise.
     */
    boolean reads(Symbol symbol);

    /** Returns true if the instruction writes to the given symbol (variable). False otherwise.
     */
    boolean writes(Symbol symbol);

    /** Adds all registers the instruction reads to the given collection.
     */
    void addRegisterReadsTo(Collection<Integer> registers);

    /** Adds all registers the instruction writes to the given collection.
     */
    void addRegisterWritesTo(Collection<Integer> registers);

    /** Adds all symbols (variables) the instruction reads to the given collection.
     */
    void addSymbolReadsTo(Collection<Symbol> symbols);

    /** Adds all symbols (variables) the instruction writes to the given collection.
     */
    void addSymbolWritesTo(Collection<Symbol> symbols);
}
