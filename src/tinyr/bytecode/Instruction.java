package tinyr.bytecode;

import tinyr.Main;
import tinyr.runtime.*;
import tinyr.runtime.Closure.AsmTypes;

import java.util.*;

import javafx.util.Pair;

/** Bytecode instruction.
 *
 * Each actual bytecode instruction must not specify new fields, but rather inherit either from Instruction (when no
 * fields are required), or from one of the subsequent encodings defined as static inner classes below.
 *
 * Each instruction has the execute() method, which given the specific frame executes the instruction, the accept()
 * method for the visitor implementation and the toString() method which returns the string representation of the
 * instruction. The string representation is often used for comparing two instructions against each other so must be
 * unique for this purpose.
 *
 * All instruction's fields must always be declared as final, otherwise the code will break as most of the versioning
 * code assumes the instructions are constant.
 */
public abstract class Instruction implements Cloneable, DDGObject {

    final HashSet<Assumption> ignoredAssumptions = new HashSet<>();

    public abstract void execute(Frame frame);

    public abstract void accept(Visitor visitor);

    /** Using the specified translator, returns a clone of the instruction with its data filled by the given translator.
     *
     * This means changing register indices and labels.
     */
    public abstract Instruction translate(Translator t);

    /** Adds the given assumption to the set of assumptions that should be ignored when optimizing the instruction.
     */
    public void ignoreAssumption(Assumption a) {
        ignoredAssumptions.add(a);
    }

    /** Returns true if the given assumption should be ignored by the optimizer for the instruction.
     */
    public boolean ignoresAssumption(Assumption a) {
        return ignoredAssumptions.contains(a);
    }

    /** Returns true if any of the given assumptions should be ignored by the optimizer of the instruction.
     */
    public boolean ignoresAssumptions(Collection<Assumption> asms) {
        if (asms == null)
            return false;
        for (Assumption a : asms)
            if (ignoredAssumptions.contains(a))
                return true;
        return false;
    }

    /** Returns the assumptions to be ignored by the optimizer for the instruction.
     */
    public Collection<Assumption> ignoredAssumptions() {
        return ignoredAssumptions;
    }

    public String ignoredAssumptionsToString() {
        if (ignoredAssumptions.isEmpty())
            return "";
        StringBuilder sb = new StringBuilder(" -- ignores");
        for (Assumption a: ignoredAssumptions)
            sb.append(" " + a.toString());
        return sb.toString();
    }

    /** Returns true if the instruction is terminating one (BR, CBR, RET).
     */
    public boolean isTerminating() {
        return false;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    @Override
    public Instruction clone() {
        try {
            return (Instruction) super.clone();
        } catch ( CloneNotSupportedException e ) {
            assert (false) : "Clone of instruction cannot fail";
            return null;
        }
    }

    @Override
    public boolean equals(Object other) {
        return this.getClass() == other.getClass();
    }

    /** Returns true if the instruction is data independent of the DDGObject specified.
     *
     * Two DDGObjects I and J are data independent iff:
     *
     * - any register or variable I writes to J does not read
     * - any register or variable I reads J does not write.
     *
     * Trivially, if I is data independent of J, then also J is data independent of I.
     */
    public boolean isDataIndependent(DDGObject other) {
        HashSet<Integer> registers = new HashSet<>();
        addRegisterReadsTo(registers);
        for (int i : registers)
            if (other.writes(i))
                return false;
        registers.clear();
        addRegisterWritesTo(registers);
        for (int i : registers)
            if (other.reads(i))
                return false;
        HashSet<Symbol> symbols = new HashSet<>();
        addSymbolReadsTo(symbols);
        for (Symbol s : symbols)
            if (other.writes(s))
                return false;
        symbols.clear();
        addSymbolWritesTo(symbols);
        for (Symbol s : symbols)
            if (other.reads(s))
                return false;
        return true;
    }

    /** Returns true if the instruction reads from the given register. False otherwise.
     */
    @Override
    public boolean reads(int register) {
        return false;
    }

    /** Returns true if the instruction writes to the given register. False otherwise.
     */
    @Override
    public boolean writes(int register) {
        return false;
    }

    /** Returns true if the instruction reads from the given symbol (variable). False otherwise.
     */
    @Override
    public boolean reads(Symbol symbol) {
        return false;
    }

    /** Returns true if the instruction writes to the given symbol (variable). False otherwise.
     */
    @Override
    public boolean writes(Symbol symbol) {
        return false;
    }

    /** Adds all registers the instruction reads to the given collection.
     */
    @Override
    public void addRegisterReadsTo(Collection<Integer> registers) {
        // pass
    }

    /** Adds all registers the instruction writes to the given collection.
     */
    @Override
    public void addRegisterWritesTo(Collection<Integer> registers) {
        // pass
    }

    /** Adds all symbols (variables) the instruction reads to the given collection.
     */
    @Override
    public void addSymbolReadsTo(Collection<Symbol> symbols) {
        // pass
    }

    /** Adds all symbols (variables) the instruction writes to the given collection.
     */
    @Override
    public void addSymbolWritesTo(Collection<Symbol> symbols) {
        // pass
    }

    /**
     * For each instruction with dreg we look for the current best possible Assumption,
     * whether it can be specific (ConstDouble,ConstClosure), general(AnyDouble, AnyClosure), null
     */
    public void computePossibleAssumptions(Frame frame, Value currentValue, int dreg, int pc) {
	if (!Main.firstTimeExecuted)
	    return;
	Touple toupleOld = frame.closure.captures.get(dreg);
	Touple toupleNew = null;
	if (!frame.closure.initializedCaptures[dreg] && toupleOld == null) { //uninitialized Value
	    //I did not try yet, just put current value as new possible Assumption
	    frame.closure.initializedCaptures[dreg] = true;
	    if (currentValue instanceof Primitive) {
		toupleNew = new Touple(pc, currentValue, AsmTypes.isConstDouble);
		frame.closure.captures.put(dreg, toupleNew);
	    } 
	    else if (currentValue instanceof Closure) {
		toupleNew = new Touple(pc, currentValue, AsmTypes.isConstClosure);
		frame.closure.captures.put(dreg, toupleNew);
	    }
	    else if (currentValue instanceof Array ){
		//From this moment there is no possible Assumption for now for this register
	    }
	    else {
		assert (false) : " Value instanceof not in {Primitive, Closure, Array}";
	    }
	} 
	else if ( frame.closure.initializedCaptures[dreg] && toupleOld == null) { // Initialized Value, no possible Assumption
	    // register already initialized && null in captures, no possible Assumption on this register
	    return;
	} 
	else if ( frame.closure.initializedCaptures[dreg] && toupleOld != null) { // adjusting stored Assumption if possible
	    Value oldValue = toupleOld.getValue();
	    Closure.AsmTypes oldAsmType = toupleOld.getAsmType();
	    if (currentValue instanceof Primitive) {
		if (oldAsmType == Closure.AsmTypes.isConstDouble) {
		    if ( ((Primitive)oldValue).value != ((Primitive)currentValue).value ) {
			toupleNew = new Touple(pc, null, AsmTypes.isDouble);
			frame.closure.captures.put(dreg, toupleNew);
		    }
		} 
		else if (oldAsmType == Closure.AsmTypes.isDouble) {
		    // stored AnyDouble, current value another Double, no change
		}
		else { // stored Closure Assumption, current value Primitive - no possible Assumption on this register
		    frame.closure.captures.put(dreg, null);
		}
	    } 
	    else if (currentValue instanceof Closure) {
		if (oldAsmType == Closure.AsmTypes.isConstClosure) {
		    if ( !((Closure)oldValue).equals((Closure)currentValue) ) {
			toupleNew = new Touple(pc, null, AsmTypes.isClosure);
			frame.closure.captures.put(dreg, toupleNew);
		    }
		}
		else if (oldAsmType == Closure.AsmTypes.isClosure) {
		    // stored AnyClosure, current value another Closure, no change
		}
		else { //stored Double Assumption, current value Closure - no possible Assumption on this register
		    frame.closure.captures.put(dreg, null);
		}
	    } else if (currentValue instanceof Array) {
		//stored Assumption, current value Array - no possible Assumption with this combination
		frame.closure.captures.put(dreg, null);
	    } else {
		assert(false) : " Value instanceof not in {Primitive, Closure, Array}";
	    }
	}
	else {
	    assert (false) : " UnExpected situation with Captures";
	}
    }

    /** Instruction encoding taking one register it reads from. An example is RET instruction.
     */
    public static abstract class I extends Instruction {
        public final int sreg;

        public I(int sreg) {
            this.sreg = sreg;
        }

        @Override
        public String toString() {
            return super.toString() + " " + sreg;
        }

        @Override
        public boolean reads(int register) {
            return register == sreg || super.reads(register);
        }

        @Override
        public void addRegisterReadsTo(Collection<Integer> registers) {
            registers.add(sreg);
            super.addRegisterReadsTo(registers);
        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((I) other).sreg == sreg;
        }

    }

    /** Instruction encoding taking one register it writes to.
     */
    public static abstract class O extends Instruction {
        public final int dreg;

        public O(int dreg) {
            this.dreg = dreg;
        }

        @Override
        public String toString() {
            return super.toString() + " " + dreg;
        }

        @Override
        public boolean writes(int register) {
            return register == dreg || super.writes(register);
        }

        @Override
        public void addRegisterWritesTo(Collection<Integer> registers) {
            registers.add(dreg);
            super.addRegisterWritesTo(registers);
        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((O) other).dreg == dreg;
        }

    }

    /** Instruction taking one input, and one output register. Unary operators, like IS_CLOSURE, or the function call
     * CALL instruction are examples.
     */
    public static abstract class OI extends O {
        public final int sreg;

        public OI(int dreg, int sreg) {
            super(dreg);
            this.sreg = sreg;
        }

        @Override
        public String toString() {
            return super.toString() + " " + sreg;
        }

        @Override
        public boolean reads(int register) {
            return register == sreg || super.reads(register);
        }

        @Override
        public void addRegisterReadsTo(Collection<Integer> registers) {
            registers.add(sreg);
            super.addRegisterReadsTo(registers);
        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((OI) other).sreg == sreg;
        }

    }

    /** Instruction which takes two input and one output registers. Binary operators like ADD, SUB, EQ, NEQ are
     * examples.
     */
    public static abstract class OII extends OI {
        public final int sreg2;

        public OII(int dreg, int sreg, int sreg2) {
            super(dreg, sreg);
            this.sreg2 = sreg2;
        }

        @Override
        public String toString() {
            return super.toString() + " " + sreg2;
        }

        @Override
        public boolean reads(int register) {
            return register == sreg2 || super.reads(register);
        }

        @Override
        public void addRegisterReadsTo(Collection<Integer> registers) {
            registers.add(sreg2);
            super.addRegisterReadsTo(registers);
        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((OII) other).sreg2 == sreg2;
        }

    }

     /** Instruction which takes as input a register and an array of registers, and one register
      * as output. CALL is an example, using the array for passing the call parameters. 
     */
    public static abstract class OIIArray extends OI {
      public final int[] sregs;

      public OIIArray(int dreg, int sreg, int[] sregs) {
          super(dreg, sreg);
          this.sregs = sregs;
      }

      @Override
      public String toString() {
          if (sregs.length != 0){
              return super.toString() + " " + Arrays.toString(sregs);
          }else {
              return super.toString();
          }
      }

      @Override
      public boolean reads(int register) {
          for (int i : sregs)
            if (register == i)
                return true;
          return super.reads(register);
      }

      @Override
      public void addRegisterReadsTo(Collection<Integer> registers) {
          for (int i : sregs)
              registers.add(i);
          super.addRegisterReadsTo(registers);
      }

      @Override
      public boolean equals(Object other) {
          return super.equals(other) && ((OIIArray) other).sregs.equals(sregs);
      }
  }

    /** Stores result to a register, calls intrinsic specified by a symbol with arguments in registers.
     *
     */
  public static abstract class ORIArray extends OR {
      public final int[] sregs;

      public ORIArray(int dreg, Symbol name, int[] sregs) {
          super(dreg, name);
          this.sregs = sregs;
      }

      @Override
      public String toString() {
          if (sregs.length != 0){
              return super.toString() + " " + Arrays.toString(sregs);
          }else {
              return super.toString();
          }
      }

      @Override
      public boolean reads(int register) {
          for (int i : sregs)
              if (register == i)
                  return true;
          return super.reads(register);
      }

      @Override
      public void addRegisterReadsTo(Collection<Integer> registers) {
          for (int i : sregs)
              registers.add(i);
          super.addRegisterReadsTo(registers);
      }

      @Override
      public boolean equals(Object other) {
          return super.equals(other) && ((ORIArray) other).sregs.equals(sregs);
      }
  }

    /** Takes only variable as an argument - OPTIMIZE instruction.
     */
    public static abstract class R extends Instruction {

        public final Symbol name;

        public R(Symbol name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return super.toString() + " " + name.getName();
        }

        @Override
        public boolean reads(Symbol variable) {
            return variable == name;
        }

        @Override
        public void addSymbolReadsTo(Collection<Symbol> symbols) {
            symbols.add(name);
        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((R) other).name == name;
        }
    }
    
    
    /** Instruction which takes an input variable and output registers. LOAD_VAR is currently using this encoding.
     */
    public static abstract class OR extends O {

        public final Symbol name;

        public OR(int dreg, Symbol name) {
            super(dreg);
            this.name = name;
        }

        @Override
        public String toString() {
            return super.toString() + " " + name.getName();
        }

        @Override
        public boolean reads(Symbol variable) {
            return variable == name;
        }

        @Override
        public void addSymbolReadsTo(Collection<Symbol> symbols) {
            symbols.add(name);
            super.addSymbolReadsTo(symbols);
        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((OR) other).name == name;
        }
    }

    /** Instruction which reads from a register and writes to a variable. STORE_VAR is an example of this setup.
     */
    public static abstract class WI extends I {

        public final Symbol name;

        public WI(Symbol name, int sreg) {
            super(sreg);
            this.name = name;
        }

        @Override
        public String toString() {
            // do not use the super functionality as we want to change the order
            return getClass().getSimpleName() + " " + name.getName() + " " + sreg;
        }

        @Override
        public boolean writes(Symbol symbol) {
            return symbol == name || super.writes(symbol);
        }

        @Override
        public void addSymbolWritesTo(Collection<Symbol> symbols) {
            symbols.add(name);
            super.addSymbolWritesTo(symbols);
        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((WI) other).name == name;
        }
    }

    /** Instruction containing only an address as an argument. This is the unconditional jump BR or LABEL instruction.
     *
     * Note that the address is not an actual pc offset, but rather a label index.
     */
    @Deprecated
    public static abstract class A extends Instruction {

        public final int address;

        public A(int address) {
            this.address = address;
        }

        @Override
        public String toString() {
            return super.toString() + " " + address;
        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((A) other).address == address;
        }
    }

    /** Instruction containing a label as an argument. This is the unconditional jump BR or a LABEL instruction.
     */
    public static abstract class L extends Instruction {

        public final Label address;

        public L(Label address) {
            this.address = address;
        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((L) other).address == address;
        }

        @Override
        public String toString() {
            return super.toString() + " " + address.hashCode();
        }
    }

    /** Instruction with two addresses and one input register. CBR is an example.
     */
    public static abstract class ILL extends I {

        public final Label addressTrue;
        public final Label addressFalse;

        public ILL(int sreg, Label addressTrue, Label addressFalse) {
            super(sreg);
            this.addressTrue = addressTrue;
            this.addressFalse = addressFalse;
        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((ILL) other).addressTrue == addressTrue && ((ILL) other).addressFalse == addressFalse;
        }

        @Override
        public String toString() {
            return super.toString() + " " + addressTrue.hashCode() + " " + addressFalse.hashCode();
        }
    }

    /** Instruction with two addresses and one input register. CBR is an example.
     *
     *
     * Note that the address is not an actual pc offset, but rather a label index.
     */
    @Deprecated
    public static abstract class IAA extends I {

        public final int addressTrue;
        public final int addressFalse;

        public IAA(int sreg, int addressTrue, int addressFalse) {
            super(sreg);
            this.addressTrue = addressTrue;
            this.addressFalse = addressFalse;
        }

        @Override
        public String toString() {
            return super.toString() + " " + addressTrue + " " + addressFalse;
        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((IAA) other).addressTrue == addressTrue && ((IAA) other).addressFalse == addressFalse;
        }
    }

    /** Instruction containing closure and output register as its arguments. FDEF is an example storing given closure
     * to a register.
     */
    public static abstract class OC extends O {
        public final Closure closure;

        public OC(int dreg, Closure closure) {
            super(dreg);
            this.closure = closure;
        }

        @Override
        public String toString() {
            return super.toString() + "\n    " + closure.body.toString().replace("\n", "\n    ");
        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((OC) other).closure == closure;
        }
    }

    /** Instruction with output register and immediate value. LOAD_CONST.
     */
    public static abstract class OImm extends O {
        public final Value value;

        public OImm(int dreg, Value value) {
            super(dreg);
            this.value = value;
        }


        @Override
        public String toString() {
            return super.toString() + " " + value;
        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((OImm) other).value == value;
        }
    }

    /** ASM, INV or USE instruction taking only the assumption as its argument.
     */
    public static abstract class As extends Instruction {

        public final Assumption assumption;

        public As(Assumption assumption) {
            this.assumption = assumption;
        }

        @Override
        public String toString() {
            return super.toString() + " " + assumption.toString();

        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((As) other).assumption == assumption;
        }
    }
    
    /** Takes only closure as an argument - for inlining purpose, to link start and the end
     *  of inlining with inlined Closure
     */
    public static abstract class INL extends Instruction {

        public final Closure closure;
        public final int index;

        public INL(Closure closure, int index) {
            this.closure = closure;
            this.index = index;
        }

        @Override
        public String toString() {
            return super.toString() + " " + index + " " + Frame.getKey(closure);
        }

        @Override
        public boolean equals(Object other) {
            return super.equals(other) && ((INL) other).closure == closure && ((INL) other).index == index;
        }
    }
}
