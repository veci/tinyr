package tinyr.bytecode;

import tinyr.runtime.*;

/** Interface for any objects that transcribe instructions.
 *
 * Used in inlining, where instructions have to be translated from calee's frame to caller's frame.
 *
 * The interface provides methods for translating registers, labels, assumptions and patches. Each instruction then
 * implements the translate method that provides its translated clone.
 */
public interface Translator {
    public int translate(int old);
    public Label translate(Label old);
    public Code.Patch translate(Code.Patch insertedBy);
    public Assumption translate(Assumption old);

    public static class SingleRegister implements Translator {

        public final int oldReg;
        public final int newReg;

        public SingleRegister(int oldReg, int newReg) {
            this.oldReg = oldReg;
            this.newReg = newReg;
        }

        @Override
        public int translate(int old) {
            return old == oldReg ? newReg : old;
        }

        @Override
        public Label translate(Label old) {
            return old;
        }

        @Override
        public Code.Patch translate(Code.Patch insertedBy) {
            return insertedBy;
        }

        @Override
        public Assumption translate(Assumption old) {
            return old;
        }
    }
}