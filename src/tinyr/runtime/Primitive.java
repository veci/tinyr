package tinyr.runtime;

/**
 */
public class Primitive extends Value {

    public double value;

    public Primitive(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "(Primitive) " + value;
    }

    @Override
    public Type type() {
        return Type.primitive;
    }

    @Override
    public int hashCode() {
        return (int)value;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Primitive) && value == (((Primitive) obj).value);

    }
}
