package tinyr.runtime;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javafx.util.Pair;
import tinyr.analysis.analysis.ConstantPropagation;
import tinyr.analysis.analysis.Liveness;
import tinyr.analysis.analysis.ReachingDefinitions;
import tinyr.analysis.optimization.CP_DCE;
import tinyr.analysis.optimization.Inlining;
import tinyr.analysis.optimization.LoopInvariant;
import tinyr.analysis.optimization.UnusedWriteRemoval;
import tinyr.analysis.optimization.peephole.StructureSimplifier;
import tinyr.bytecode.Code;
import tinyr.bytecode.Code.Patch;
import tinyr.bytecode.Instruction;
import tinyr.bytecode.instructions.ASM;
import tinyr.bytecode.instructions.CBR;
import tinyr.bytecode.instructions.END;
import tinyr.bytecode.instructions.START;
import tinyr.runtime.Assumption.Register;

/**
 * This is not that much of a closure because everything is global, so we do not capture
 * any state when the closure is created.
 */
public class Closure extends Value {

    public static enum AsmTypes {
        isDouble,
        isClosure,
        isConstDouble,
        isConstClosure
    }

    /**
     * Code of the Function
     */
    public final Code.View body;

    /**
     * stores informations about possible Assumptions for each Register - Key in Map
     * Touple - represents information for possible Assumptions
     */
    public Map<Integer, Touple> captures = new HashMap<Integer, Touple>();

    /**
     * boolean value for each register to distinguish between un/initialized
     */
    public boolean[] initializedCaptures;

    /**
     * list of PCs, where Assumptions were inserted in to the code, can not be Set
     * as more assumptions can be inserted with same pc
     */
    List<Integer> insertedPCs;

    /**
     * Number of Arguments of the Function
     */
    public final int numOfArgs;

    /**
     * For each different set of Assumptions we want to optimize function and remember optimized state
     */
    private Map<Set<Assumption>, Set<Patch>> closureHistory = new HashMap<Set<Assumption>, Set<Patch>>();

    /**
     * Set of Assumptions a function was optimized with stored already in File
     */
    private Set<Set<Assumption>> assumptionsInFile = new LinkedHashSet<Set<Assumption>>();

    /**
     * Set of already defined functions
     */
    public static Set<Closure> functions = new LinkedHashSet<Closure>();

    /**
     * Boolean value to distinguish whether function is already optimized or not,
     * so we do not do it every times, when calling the function
     */
    public boolean optimized = false;

    /**
     * true when function executed once at least
     */
    public boolean executed = false;

    /**
     * Set of validAssumptions after each code execution
     */
    public static Set<Pair<Closure, Assumption>> goodASMs = new LinkedHashSet<Pair<Closure, Assumption>>();
    
    /**
     * Boolean mark to distinguish between Main function (first running) among others
     */
    public boolean mainFunction = false;

    public Closure(Code.View body) {
        this.body = body;
        this.body.codeObject().closure = this;
        this.numOfArgs = 0;
        Closure.functions.add(this);
        initializedCaptures = new boolean[body.numOfRegisters()];
    }

    public Closure( Code.View body, int numOfArgs ) {
        this.body = body;
        this.body.codeObject().closure = this;
        this.numOfArgs = numOfArgs;
        Closure.functions.add(this);
        initializedCaptures = new boolean[body.numOfRegisters()];
    }

    private static int generatedAssumptions = 0;
    private static int executedFunctions = 0;
    
    /**
     * Inserts Assumptions for each executed function based on captured information
     * and optimizes functions
     */
    public static void generateAssumptions() {
        //generate only on already optimized functions
        for (Closure closure : Closure.functions)
            if (closure.executed)
                generateAssumptionsForFunction(closure);
        System.out.println("GENERATED ASMs : " + generatedAssumptions);
        System.out.println("Executed Functions : " + executedFunctions);
        for (Closure closure : Closure.functions)
            if (closure.executed && !closure.optimized) {
                optimizeFunction(closure);
            }
        goodASMsToFile();
        openFilesForFunctions();
        removeMarkingInstructions();
        removeUnusedAssumptions();
        countWriteMetrics(goodASMs);
    }
    
    private static void removeUnusedAssumptions() {
	for (Closure closure : Closure.functions) {
	    if (closure.executed) {
		Set<Assumption> goodOnes = getAssumptionsForFunction(closure, Closure.goodASMs);
		List<Integer> toRemove = new ArrayList<Integer>();
		for (int i = closure.body.size()-1; i >= 0; i--) {
		    Instruction ins = closure.body.get(i);
		    if (ins instanceof ASM) {
			if (! goodOnes.contains(((ASM)ins).assumption)) {
			    toRemove.add(i);
			}
		    }
		}
		Patch p = closure.body.openNewPatch();
		for (int i = 0; i < toRemove.size(); i++) {
		    closure.body.remove(toRemove.get(i), p);
		}
		closure.body.close(p);
	    }
	}
    }

    /**
     * Removes START and END instructions from all of the executed functions
     */
    private static void removeMarkingInstructions() {
	for (Closure closure : Closure.functions) {
	    if (closure.executed) {
		Patch p = closure.body.openNewPatch();
		for (int i = closure.body.size() - 1; i >= 0; i--) {
		    Instruction ins = closure.body.get(i);
		    if (ins instanceof START || ins instanceof END) {
			closure.body.remove(i, p);
		    }
		}
		closure.body.close(p);
	    }
	}
    }

    /**
     * Removes START and END instructions from the given function
     */
    private static void removeMarkingInstructions(Closure closure) {
	Patch p = closure.body.openNewPatch();
	for (int i = closure.body.size() - 1; i >= 0; i--) {
	    Instruction ins = closure.body.get(i);
	    if (ins instanceof START || ins instanceof END)
		closure.body.remove(i, p);
	}
	closure.body.close(p);
    }

    /**
     * Inserts Assumptions to the function code based on captured information
     */
    private static void generateAssumptionsForFunction(Closure closure) {
	executedFunctions++;
        for (Entry<Integer, Touple> entry : closure.captures.entrySet()) {
            int pc = entry.getValue().getPc();
            Assumption assumption = createAssumption(entry.getKey(), entry.getValue().getAsmType(), entry.getValue().getValue());
            insertAssumption(closure, assumption, pc);
            generatedAssumptions++;
        }
        if (closure.captures.size() != 0)
            closure.optimized = false;
    }

    /**
     * Creates best possible Assumption based on captured information
     */
    private static tinyr.runtime.Assumption createAssumption(int register, AsmTypes asmType, Value v) {
        Assumption assumption = null;
        if (asmType == Closure.AsmTypes.isDouble) {
            assumption = new Assumption.Register.IsDouble(register);
        }
        else if (asmType == Closure.AsmTypes.isConstDouble) {
            assumption = new Assumption.Register.IsConstDouble( register, ((Primitive)v).value );
        }
        else if (asmType == Closure.AsmTypes.isClosure) {
            assumption = new Assumption.Register.IsClosure(register);
        }
        else if (asmType == Closure.AsmTypes.isConstClosure) {
            assumption = new Assumption.Register.IsConstClosure( register, (Closure)v );
        }
        return assumption;
    }

    /**
     * Inserts Assumption to the function code
     * @param closure, where Assumption is inserted
     * @param asm - Inserted assumption
     * @param pc -  where it should be inserted
     */
    private static void insertAssumption(Closure closure, Assumption asm, int pc) {
        /*
         * Introducing set of already insertedPCs to the function
         * whenever we are inserting to the code, first we have to look, if we already
         * inserted to the lower pc and increase the current one
         */
        if (closure.insertedPCs == null) {
            closure.insertedPCs = new ArrayList<Integer>();
        }
        for (Integer pcIndex : closure.insertedPCs) {
            if (pc >= pcIndex)
                pc++;
        }
        Instruction ins = new tinyr.bytecode.instructions.ASM(asm);
        Patch p = closure.body.openNewPatch();
        closure.body.insert(ins, pc, p);
        closure.insertedPCs.add(pc);
        closure.body.close(p);
        closure.optimized = false;
    }

    /**
     * optimization order
     */
    public static int optIndex = 0;

    /**
     * Optimizes function
     */
    public static void optimizeFunction( Closure fn ) {
        boolean optimized = true;
        while ( optimized ) {
            optimized = false;
            optIndex++;
            /*
            if ( UnusedWriteRemoval.optimize( fn.body, Liveness.analyze( fn.body ) )
              || LoopInvariant.optimize( fn.body, ReachingDefinitions.analyze( fn.body ) )
              || CP_DCE.optimize( fn.body, ConstantPropagation.analyze( fn.body, fn.numOfArgs ) )
              || Inlining.optimize( fn.body, ConstantPropagation.analyze( fn.body, fn.numOfArgs ) )
              || StructureSimplifier.optimize( fn.body )
              || DuplicateUSERemoval.optimize( fn.body ) ) {
                optimized = true;
            }
            */
            System.out.println(optIndex + " LI/RD + before " + Frame.getKey(fn));
            if (LoopInvariant.optimize( fn.body, ReachingDefinitions.analyze( fn.body ))) {
                optimized = true;
                System.out.println(optIndex + " LI/RD + after " + Frame.getKey(fn));
                continue;
            }
            System.out.println(optIndex + " CP_DCE + before " + Frame.getKey(fn));
            if (CP_DCE.optimize( fn.body, ConstantPropagation.analyze( fn.body, fn.numOfArgs ))) {
                optimized = true;
                System.out.println(optIndex + " CP + after " + Frame.getKey(fn));
                continue;
            }
            System.out.println(optIndex + " UWR/Liveness + before " + Frame.getKey(fn));
            if (UnusedWriteRemoval.optimize( fn.body, Liveness.analyze( fn.body ))) {
                optimized = true;
                System.out.println(optIndex + " UWR + after " + Frame.getKey(fn));
                continue;
            }
            System.out.println(optIndex + " Inl/CP + before " + Frame.getKey(fn));
            if (Inlining.optimize( fn.body, ConstantPropagation.analyze( fn.body, fn.numOfArgs ))) {
                optimized = true;
                System.out.println(optIndex + " IN + after " + Frame.getKey(fn));
                continue;
            }
            System.out.println(optIndex + " SS + before " + Frame.getKey(fn));
            if (StructureSimplifier.optimize( fn.body )) {
                optimized = true;
                System.out.println(optIndex + " SS + after " + Frame.getKey(fn));
                continue;
            }
            /*
            System.out.println(optIndex + " DuseR + before " + Frame.getKey(fn));
            if (DuplicateUSERemoval.optimize( fn.body )) {
                optimized = true;
                System.out.println(optIndex + " DUSER + after " + Frame.getKey(fn));
                continue;
            }
            */
        }
        fn.optimized = true;
    }

    /**
     * stores number of subsets of goodASMs
     */
    private static long subsetCount = 1;

    /**
     * creates inputConfiguration from goodASMs
     */
    public static void createInputConfiguration() {
	if (goodASMs.size() == 0)
	    return;
	int arrayLength = goodASMs.size();
	inputConfiguration = new int[arrayLength];
	int index = 0;
	for (Pair<Closure, Assumption> pair : goodASMs) {
	    Assumption asm = pair.getValue();
	    if (asm instanceof Register.IsConstClosure || asm instanceof Register.IsConstDouble) {
		inputConfiguration[index] = 1;
		subsetCount = subsetCount * 3;
	    }
	    else {
		inputConfiguration[index] = 2;
		subsetCount = subsetCount * 2;
	    }
	    index++;
	}
	subsetCount --; //because of goodASMs configuration (best possible) already tried
	System.out.println("Subset Count : " + subsetCount);
        currentConfiguration = new int [arrayLength];
        for (int i = 0; i < arrayLength; i++)
            currentConfiguration[i] = inputConfiguration[i];
        generateConfigurations(arrayLength - 1);
    }
    
    /**
     * currently found configuration to be processed
     */
    private static int[] currentConfiguration;
    
    /**
     * last configuration used to simplify calculations
     */
    private static int[] lastConfiguration;

    /**
     * Configuration reflecting goodASMs set
     * {1,2,3} - possible Integers
     * 1 - represents constant (specific) type of Assumption
     * 2 - represents any (general) type of Assumption
     * 3 - represents no Assumption
     */
    private static int[] inputConfiguration;

    private static final int LATTICE_HEIGHT = 3; // {ConstantClosure, ConstantDouble} -- {AnyClosure, AnyDouble} -- {null}

    /**
     * generates configurations one by one from inputConfiguration
     */
    private static void generateConfigurations(int fromIndex) {
        if (fromIndex == -1) {
            //Current Configuration equals to the Input one
            return;
        }
        if (currentConfiguration[fromIndex] < LATTICE_HEIGHT) {
            currentConfiguration[fromIndex]++;
            subsetFromConfiguration(currentConfiguration);
            generateConfigurations(inputConfiguration.length - 1);
        }
        else {
            currentConfiguration[fromIndex] = inputConfiguration[fromIndex];
            generateConfigurations(fromIndex - 1);
        }
    }

    /**
     * In order to see current progress of computing
     */
    private static long confIndex = 1;

    private static List<Pair<Closure, Assumption>> assumptionsConfiguration = new ArrayList<Pair<Closure, Assumption>>();

    /**
     * creates Set of Assumptions reflecting currentConfiguration
     */
    private static void subsetFromConfiguration(int[] currentConfiguration) {
	if (lastConfiguration == null) {
	    int index = 0;
	    for (Pair<Closure, Assumption> pair : goodASMs) {
	        Assumption asm = weakenAssumption(pair.getValue(), currentConfiguration[index]);
	        Closure cl = pair.getKey();
		assumptionsConfiguration.add(new Pair<Closure, Assumption>(cl, asm));
	        index++;
	    }
	}
	else {
	    for (int i = 0; i < currentConfiguration.length; i++) {
		if (currentConfiguration[i] != lastConfiguration[i]) {
		    @SuppressWarnings("unchecked")
		    Pair<Closure, Assumption> pair = (Pair<Closure, Assumption>)goodASMs.toArray()[i];
		    Assumption asm = weakenAssumption(pair.getValue(), currentConfiguration[i]);
		    Closure cl = pair.getKey();
		    assumptionsConfiguration.remove(i);
		    assumptionsConfiguration.add(i, new Pair<Closure, Assumption>(cl, asm));
		}
	    }
	}
	confIndex++;
	//remove nulls
	Set<Pair<Closure, Assumption>> inputSet = new LinkedHashSet<Pair<Closure, Assumption>>();
	for (Pair<Closure, Assumption> pair : assumptionsConfiguration) {
	    if (pair.getValue() != null) {
		inputSet.add(pair);
	    }
	}
	updateAssumptions(inputSet);
	lastConfiguration = new int[currentConfiguration.length];
	for (int i = 0; i < currentConfiguration.length; i++)
	    lastConfiguration[i] = currentConfiguration[i];
    }

    /**
     * Weakens given Assumption to more general one based on assumptionType -
     * integer representing how much should I weaken it
     */
    private static Assumption weakenAssumption(Assumption asm, int assumptionType) {
        if (assumptionType == 3) {
            return null;
        }
        else if (assumptionType == 2) {
            if (asm instanceof Register.IsClosure || asm instanceof Register.IsDouble) {
        	return asm;
            }
            else if (asm instanceof Register.IsConstClosure) {
        	return new Assumption.Register.IsClosure( ((Assumption.Register)asm).regIndex );
            }
            else if (asm instanceof Register.IsConstDouble) {
        	return new Assumption.Register.IsDouble( ((Assumption.Register)asm).regIndex );
            }
        }
        return asm;//if assumptionType == 1
    }

    /**
     * stores information how many optimizations were spared with the current
     * approach - function optimized only for different set of Assumptions
     */
    public static long sparedOpt = 0;

    /**
     * stores information how many times we optimized
     */
    public static long doneOpt = 0;

    /**
     * Updates assumptions in the code, only if current inputSet different to all the previous ones
     */
    private static void updateAssumptions(Set<Pair<Closure, Assumption>> inputSet) {
	if (inputSet.size() == 0)
	    return;
	//split assumptions for each function
	Map<Closure, Set<Assumption>> subsetForFunction = new HashMap<Closure, Set<Assumption>>();
        for (Pair<Closure, Assumption> pair : inputSet) {
            Closure closure = pair.getKey();
            Assumption asm = pair.getValue();
            if (subsetForFunction.get(closure) == null) {
        	subsetForFunction.put(closure, new LinkedHashSet<Assumption>());
            }
            subsetForFunction.get(closure).add(asm);
        }
        //now look if closure history already has the same assumption set or not
        for (Entry<Closure, Set<Assumption>> entry : subsetForFunction.entrySet()) {
            Closure closure = entry.getKey();
            Set<Assumption> ASMs = entry.getValue();
            if (ASMs.size() != 0 && !closure.closureHistory.containsKey(ASMs)) {
        	restoreFunctionToTheBeginning(closure);
        	for (Assumption asm : ASMs) {
        	    int pc = closure.captures.get(((Assumption.Register)asm).regIndex).getPc();
                    insertAssumption(closure, asm, pc);
        	}
        	closure.optimized = false;
            }
            else if (ASMs.size() != 0 && closure.closureHistory.containsKey(ASMs)){
        	closure.body.setSignature(closure.closureHistory.get(ASMs));
        	sparedOpt++;//how many optimization loops are spared with this approach (storing signatures)
            }
        }
        for (Closure closure : Closure.functions) {
            if (closure.executed && !closure.optimized) {
        	if (!closure.optimized)
        	    optimizeFunction(closure);
            }
        }
        countWriteMetrics(inputSet);
    }

    /**
     * prints Set<Pair<Closure, Assumption>> to console
     */
    private static void assumptionsSetToString(Set<Pair<Closure, Assumption>> inputSet) {
        System.out.println("-----BEGIN-----");
        for (Pair<Closure, Assumption> pair : inputSet) {
            System.out.println( Frame.getKey(pair.getKey()) + " " + pair.getValue() );
        }
        System.out.println("-----END-----");
    }

    /**
     * writes set of goodASMs to File
     */
    private static void goodASMsToFile() {
        try {
            File file = new File("metrics/goodASMs");
            PrintWriter out = new PrintWriter(file);
            out.println("goodAMSs " + goodASMs.size() + " : ");
            for (Pair<Closure, Assumption> pair : goodASMs) {
                out.println("  " + Frame.getKey(pair.getKey()) + " " + pair.getValue());
            }
            out.close();
        } catch ( FileNotFoundException e ) {
            System.err.println("FILE NOT FOUND");
        }
    }

    /**
     * Set function signature to empty HashSet and restores largest register
     */
    private static void restoreFunctionToTheBeginning(Closure cl) {
        if (cl.executed) {
            cl.body.restoreLargestRegister();
            cl.body.setSignature(new HashSet<>());
            cl.insertedPCs = new ArrayList<Integer>();
            optimizeFunction(cl);
        }
    }

    /**
     * holds separate PrintWriter for each executed function (assuming that
     * each executed one has some good Assumptions)
     */
    private static Map<Closure, PrintWriter> filesForFunctions = new HashMap<Closure, PrintWriter>();

    /**
     * Opens and prepares files for executed functions
     */
    private static void openFilesForFunctions() {
        for (Closure cl : Closure.functions)
            if (cl.executed) {
                File file = new File("metrics/" + Frame.getKey(cl));
                try {
                    PrintWriter output = new PrintWriter(file);
                    filesForFunctions.put( cl, output );
                } catch ( FileNotFoundException e ) {
                    e.printStackTrace();
                }
            }
    }
    
    /**
     * Closes all used PrintWriters
     */
    public static void closeFilesForFunctions() {
        for (PrintWriter pw : filesForFunctions.values())
            if (pw != null)
                pw.close();
        filesForFunctions.clear();
    }

    /**
     * Counts and stores the current code size with and without assumptions
     * and amount of CBR instructions after optimization based on
     * inputSet of Assumptions
     * @param inputSet
     */
    private static void countWriteMetrics(Set<Pair<Closure, Assumption>> inputSet) {
        int instructions;
        int cbrCount;
        Set<Assumption> ASMs = new LinkedHashSet<>();
        for (Closure cl : Closure.functions) {
            if (cl.executed) {
        	ASMs = getAssumptionsForFunction(cl, inputSet);
        	if (!cl.closureHistory.containsKey(ASMs)) {
        	    cl.closureHistory.put(ASMs, new HashSet<Patch>(cl.body.signature()));
        	    doneOpt++;
        	}
        	if ( !cl.assumptionsInFile.contains(ASMs) ) {
        	    cl.assumptionsInFile.add(new HashSet<>(ASMs));
                    instructions = 0;
                    cbrCount = 0;
                    for (int i = 0; i< cl.body.size(); i++) {
                        if ( !(cl.body.get(i) instanceof ASM) )
                            instructions++;
                        if ( (cl.body.get(i) instanceof CBR) ) {
                            cbrCount++;
                        }
                    }
                    filesForFunctions.get(cl).println("Current weakened set : " + ASMs.size());
                    for (Assumption asm : ASMs)
                        filesForFunctions.get(cl).println(" " + asm);
                    filesForFunctions.get(cl).println("  CodeSize " + cl.body.size());
                    filesForFunctions.get(cl).println("  Without ASMs " + instructions);
                    filesForFunctions.get(cl).println("  CBR count " + cbrCount);
                    filesForFunctions.get(cl).println();
                    filesForFunctions.get(cl).flush();
                }
            }
        }
    }

    /**
     * returns set of Assumptions from inputSet for given function
     */
    private static Set<Assumption> getAssumptionsForFunction(Closure function, Set<Pair<Closure, Assumption>> inputSet) {
        Set<Assumption> result = new HashSet<Assumption>();
        for (Pair<Closure, Assumption> pair : inputSet)
            if (function.equals( pair.getKey() ))
                result.add(pair.getValue());
        return result;
    }

    @Override
    public String toString() {
        return "(Closure " + Frame.getKey(this) + ") \n" + body;
    }

    @Override
    public Type type() {
        return Type.closure;
    }
}