package tinyr.runtime;

import java.util.*;

/**
 * A Symbol is the name of a variable. All symbols are interned and can be
 * compared for equality with ==.
 */
public class Symbol implements Comparable {

    /** A symbol that means any symbol at all. */
    public static final Symbol ANY = new Symbol("");


    private final String name;
    private static HashMap<String, Symbol> allSyms = new HashMap<>(1000);

    public static HashSet<Symbol> intrinsics = new HashSet<>();

    public static final Symbol sqrt = Symbol.intrinsic("sqrt");
    public static final Symbol c = Symbol.intrinsic("c");
    public static final Symbol assertion = Symbol.intrinsic("assert");
    public static final Symbol print = Symbol.intrinsic("print");
    public static final Symbol printHex = Symbol.intrinsic("printHex");
    public static final Symbol printStr = Symbol.intrinsic("printStr");
    public static final Symbol printRes = Symbol.intrinsic("printRes");    
    public static final Symbol iDiv = Symbol.intrinsic("iDiv");
    public static final Symbol mod = Symbol.intrinsic("mod");
    public static final Symbol pow = Symbol.intrinsic("pow");
    public static final Symbol openFile = Symbol.intrinsic("openFile");
    public static final Symbol eof = Symbol.intrinsic("eof");
    public static final Symbol readLine = Symbol.intrinsic("readLine");
    public static final Symbol closeFile = Symbol.intrinsic("closeFile");
    public static final Symbol strFind = Symbol.intrinsic("strFind");
    public static final Symbol strReplace = Symbol.intrinsic("strReplace");
    public static final Symbol time = Symbol.intrinsic("time");    

    private static Symbol intrinsic(String name) {
        Symbol s = Symbol.make(name);
        intrinsics.add(s);
        return s;
    }

    /**
     * Return a new or an existing symbol.
     *
     * @param n representation string
     * @return unique symbol
     */
    public static Symbol make(String n) {
        Symbol result = allSyms.get(n);
        if (result == null) {
            result = new Symbol(n);
            allSyms.put(n, result);
        }
        return result;
    }

    private Symbol(String n) {
        name = n;
    }

    /**
     * Compare the representations of two symbols.
     *
     * @param s other symbol
     * @return negative value if this is smaller than s
     */
    public int compare(Symbol s) {
        return name.compareTo(s.name);
    }

    /**
     * @return the representation of the symbol
     */
    public String getName() {
        return name;
    }

    /**
     * compareTo() method for Arrays.sort used in State.java define() method.
     */
    @Override
    public int compareTo(Object o) {
        return this.name.compareTo(((Symbol) o).name);
    }
    
    @Override
    public String toString() {
        return name;
    }
    
}
