package tinyr.runtime;

import tinyr.analysis.Optimizer;
import tinyr.bytecode.*;
import tinyr.bytecode.instructions.*;
import tinyr.runtime.Closure.AsmTypes;

import java.util.*;

import javafx.util.Pair;

/** Assumption record.
 *
 * Assumption can be made about either a global variable, or a local register. Currently, the following assumptions are
 * supported:
 *
 * - variable/register is a specific double value (IsConstDouble)
 * - variable/register is of a double type (IsDouble)
 * - variable/register is a closure (IsClosure)
 * - variable/register is a specific closure (IsConstClosure)
 *
 * For the assumptions on global variables, the objects are guaranteed to be singletons. For assumptions on local
 * registers, the objects are singletons within single closures.
 *
 * Each assumption is capable of emiting its check & invalidate code.
 */
public abstract class Assumption {

    /** For each code executable (basically for all closures) keeps information about the patches that depend on the
     * given assumption.
     */
    HashMap<Code, HashSet<Code.Patch>> dependents = new HashMap<>();
        
    /** Checks whether an assumption holds.     
     */
    public abstract boolean check(Frame frame);

    /** Translates the assumption under the given translator.
     */
    public abstract Assumption translate(Translator t);

    /** Adds the given patch to the assumption dependents in the specified code.
     */
    public void addDependent(Code code, Code.Patch patch) {
        HashSet<Code.Patch> x = dependents.getOrDefault(code, null);
        if (x == null) {
            x = new HashSet<>();
            dependents.put(code, x);
        }
        x.add(patch);
    }

    /** Returns the hashset of all patches depending on the assumption in the given code.
     */
    public HashSet<Code.Patch> dependents(Code forCode) {
        return dependents.getOrDefault(forCode, null);
    }

    /** Outputs the code to check the whether the assumption still holds, and if it does not, an INV instruction for the
     * assumption.
     *
     * This has to be rewritten in each subclasses to emit the proper check.
     */
    //public abstract void outputInvalidationCheck(Optimizer opt, Code.Patch patch);

    /** Base class for assumptions being made about local registers.
     */
    public static abstract class Register extends Assumption {

        /** Index of register on which the assumption is being made.
         */
        public final int regIndex;
                
        protected Register (int regIndex) {
            this.regIndex = regIndex;
        }
        
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + regIndex;
            return result;
        }

        /**
         * Given the assumption return its and regIndex and AsmType
         */
        public static Pair<Integer, AsmTypes> getRegIndexAndAsmType(Assumption asm) {
            int regIndex = ((tinyr.runtime.Assumption.Register) asm).regIndex;
            Closure.AsmTypes asmType = null;
            if (asm instanceof tinyr.runtime.Assumption.Register.IsConstDouble)
                asmType = Closure.AsmTypes.isConstDouble;
            else if (asm instanceof tinyr.runtime.Assumption.Register.IsDouble)
                asmType = Closure.AsmTypes.isDouble;
            else if (asm instanceof tinyr.runtime.Assumption.Register.IsConstClosure)
                asmType = Closure.AsmTypes.isConstClosure;
            else if (asm instanceof tinyr.runtime.Assumption.Register.IsClosure)
                asmType = Closure.AsmTypes.isClosure;
            return new Pair<Integer, AsmTypes>(regIndex, asmType);
        }

        /** Assumption on the register value being a specific double number.
         */
        public static class IsConstDouble extends Register {
          
            public final double value;
                        
            public IsConstDouble(int regIndex, double value) {
                super(regIndex);
                this.value = value;
            }

            @Override
            public boolean check(Frame frame) {
                Value v = frame.get(regIndex);
                return v != null && v instanceof Primitive && (((Primitive) v).value == this.value);
            }

            @Override
            public Assumption translate(Translator t) {
                return new Assumption.Register.IsConstDouble(t.translate(regIndex), value);
            }
            /*
            @Override
            public void outputInvalidationCheck(Optimizer opt, Code.Patch patch) {
                Label lEnd = new Label();
                Label lInv = new Label();
                int r1 = opt.newRegister();
                int r2 = opt.newRegister();
                opt.insert(new LOAD_CONST(r1, value), patch);
                opt.insert(new EQ(r2, regIndex, r1), patch).ignoreAssumption(this);
                opt.insert(new CBR(r2, lEnd, lInv), patch).ignoreAssumption(this);
                opt.insert(new LABEL(lInv), patch);
                opt.insert(new INV(this), patch);
                //opt.insert(new BR(lEnd), patch);
                opt.insert( new RET(0), patch );
                opt.insert(new LABEL(lEnd), patch);                                
            }*/

            @Override
            public String toString() {
                return "register " + regIndex + " == " + value;
            }

            @Override
            public boolean equals(Object other) {
                return other instanceof IsConstDouble && ((IsConstDouble) other).regIndex == regIndex && ((IsConstDouble) other).value == value;
            }

            @Override
            public int hashCode() {
                final int prime = 31;
                int result = super.hashCode();
                long temp;
                temp = Double.doubleToLongBits( value );
                result = prime * result + (int) (temp ^ (temp >>> 32));
                return result;
            }
        }

        /** Assumption on the register value being general double number.
         */
        public static class IsDouble extends Register {                    

            public IsDouble(int regIndex) {              
                super(regIndex);
            }

            @Override
            public boolean check(Frame frame) {
                Value v = frame.get(regIndex);
                return v != null && v instanceof Primitive;
            }

            @Override
            public Assumption translate(Translator t) {
                return new Assumption.Register.IsDouble(t.translate(regIndex));
            }

            @Override
            public String toString() {
                return "register " + regIndex + " is double";
            }

            @Override
            public boolean equals(Object other) {            
                return other instanceof Assumption.Register.IsDouble && ((Assumption.Register.IsDouble) other).regIndex == regIndex;
            }
            /*
            @Override
            public void outputInvalidationCheck(Optimizer opt, Code.Patch patch) {
                Label lEnd = new Label();
                Label lInv = new Label();
                int r1 = opt.newRegister();
                opt.insert(new IS_DOUBLE(r1, regIndex), patch).ignoreAssumption(this);
                opt.insert(new CBR(r1, lEnd, lInv), patch).ignoreAssumption(this);
                opt.insert(new LABEL(lInv), patch);
                opt.insert(new INV(this), patch);
                //opt.insert(new BR(lEnd), patch);
                opt.insert( new RET(0), patch );
                opt.insert(new LABEL(lEnd), patch);
            }*/
        }

        /** Assumption on the register being general closure.
         */
        public static class IsClosure extends Register {                    

            public IsClosure(int regIndex) {
                super(regIndex);              
            }
   
            @Override
            public boolean check(Frame frame) {
                Value v = frame.get(regIndex);
                return v != null && v instanceof Closure;
            }

            @Override
            public Assumption translate(Translator t) {
                return new Assumption.Register.IsClosure(t.translate(regIndex));
            }

            @Override
            public String toString() {
                return "register " + regIndex + " is closure";
            }

            @Override
            public boolean equals(Object other) {
                return other instanceof Assumption.Register.IsClosure && ((Assumption.Register.IsClosure) other).regIndex == regIndex;
            }
            /*
            @Override
            public void outputInvalidationCheck(Optimizer opt, Code.Patch patch) {
                Label lEnd = new Label();
                Label lInv = new Label();
                int r1 = opt.newRegister();
                opt.insert(new IS_CLOSURE(r1, regIndex), patch).ignoreAssumption(this);
                opt.insert(new CBR(r1, lEnd, lInv), patch).ignoreAssumption(this);
                opt.insert(new LABEL(lInv), patch);
                opt.insert(new INV(this), patch);
                //opt.insert(new BR(lEnd), patch);
                opt.insert( new RET(0), patch );
                opt.insert(new LABEL(lEnd), patch);
            }*/
        }

        /** Assumption on the register being the given closure.
         */
        public static class IsConstClosure extends Register {

            public final Closure value;          

            public IsConstClosure(int regIndex, Closure value) {
                super(regIndex);              
                this.value = value;          
            }

            @Override
            public boolean check(Frame frame) {
                return frame.get(regIndex) == value;
            }

            @Override
            public Assumption translate(Translator t) {
                return new Assumption.Register.IsConstClosure(t.translate(regIndex), value);
            }

            @Override
            public String toString() {
                return "register " + regIndex + " is constant closure " + Frame.getKey(value);
            }
   
            @Override
            public boolean equals(Object other) {
                return other instanceof IsConstClosure && ((IsConstClosure) other).regIndex == regIndex && ((IsConstClosure) other).value == value;
            }            
            /*
            @Override
            public void outputInvalidationCheck(Optimizer opt, Code.Patch patch) {
                Label lEnd = new Label();
                Label lInv = new Label();
                int r1 = opt.newRegister();
                int r2 = opt.newRegister();
                opt.insert(new LOAD_CONST(r1, value), patch);
                opt.insert(new EQ(r2, regIndex, r1), patch).ignoreAssumption(this);
                opt.insert(new CBR(r2, lEnd, lInv), patch).ignoreAssumption(this);
                opt.insert(new LABEL(lInv), patch);
                opt.insert(new INV(this), patch);
                //opt.insert(new BR(lEnd), patch);
                opt.insert( new RET(0), patch );
                opt.insert(new LABEL(lEnd), patch);
            }*/
        }
    }

    /** Assumptions on global variables.
     */
    public static abstract class Variable extends Assumption {
        
        /** Name of the variable on which the assumption is being made.
         */
        public final Symbol name;
                       
        protected Variable(Symbol name) {
            this.name = name;
        }

        @Override
        public Assumption translate(Translator t) {
            assert (false) : "No need to translate global variable assumptions.";
            return null;
        }
        
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            return result;
        }

        /** Assumption on global variable being a constant number.
         */
        public static class IsConstDouble extends Variable {

            public final double value;

            public IsConstDouble(Symbol name, double value) {
                super(name);
                this.value = value;
            }                       

            @Override
            public boolean check(Frame frame) {
                Value v = Frame.getVariable(name);
                return v != null && v instanceof Primitive && (((Primitive) v).value == this.value);
            }

            /** Outputs the check for the assumption condition and invalidation on failure.
             *//*
            @Override
            public void outputInvalidationCheck(Optimizer opt, Code.Patch patch) {
                Label lEnd = new Label();
                Label lInv = new Label();
                int r1 = opt.newRegister();
                int r2 = opt.newRegister();
                int r3 = opt.newRegister();
                opt.insert(new LOAD_VAR(r1, name), patch).ignoreAssumption(this);
                opt.insert(new LOAD_CONST(r2, value), patch);
                opt.insert(new EQ(r3, r1, r2), patch).ignoreAssumption(this);
                opt.insert(new CBR(r3, lEnd, lInv), patch).ignoreAssumption(this);
                opt.insert(new LABEL(lInv), patch);
                opt.insert(new INV(this), patch);
                //opt.insert(new BR(lEnd), patch);
                opt.insert( new RET(0), patch );
                opt.insert(new LABEL(lEnd), patch);
            }*/

            @Override
            public String toString() {
                return name.toString() + " == " + value;
            }

            @Override
            public boolean equals(Object other) {
                return other instanceof IsConstDouble && ((IsConstDouble) other).name == name && ((IsConstDouble) other).value == value;
            }
        }

        /** Assumption on variable being of double type.
         */
        public static class IsDouble extends Variable {

            public IsDouble(Symbol name) {
                super(name);
            }

            @Override
            public boolean check(Frame frame) {
                Value v = Frame.getVariable(name);
                return v != null && v instanceof Primitive;
            }

            @Override
            public String toString() {
                return name.toString() + " is double";
            }

            @Override
            public boolean equals(Object other) {
                return other instanceof Assumption.Variable.IsDouble && ((Assumption.Variable.IsDouble) other).name == name;
            }
            /*
            @Override
            public void outputInvalidationCheck(Optimizer opt, Code.Patch patch) {
                Label lEnd = new Label();
                Label lInv = new Label();
                int r1 = opt.newRegister();
                int r2 = opt.newRegister();
                opt.insert(new LOAD_VAR(r1, name), patch).ignoreAssumption(this);
                opt.insert(new IS_DOUBLE(r2, r1), patch).ignoreAssumption(this);
                opt.insert(new CBR(r2, lEnd, lInv), patch).ignoreAssumption(this);
                opt.insert(new LABEL(lInv), patch);
                opt.insert(new INV(this), patch);
                //opt.insert(new BR(lEnd), patch);
                opt.insert( new RET(0), patch );
                opt.insert(new LABEL(lEnd), patch);
            }*/
        }

        /** Assumption on global variable being a closure.
         */
        public static class IsClosure extends Variable {

            public IsClosure(Symbol name) {
                super(name);
            }

            @Override
            public boolean check(Frame frame) {
                Value v = Frame.getVariable(name);
                return v != null && v instanceof Closure;
            }

            @Override
            public String toString() {
                return name.toString() + " is closure";
            }

            @Override
            public boolean equals(Object other) {
                return other instanceof Assumption.Variable.IsClosure && ((Assumption.Variable.IsClosure) other).name == name;
            }
            /*
            @Override
            public void outputInvalidationCheck(Optimizer opt, Code.Patch patch) {
                Label lEnd = new Label();
                Label lInv = new Label();
                int r1 = opt.newRegister();
                int r2 = opt.newRegister();
                opt.insert(new LOAD_VAR(r1, name), patch).ignoreAssumption(this);
                opt.insert(new IS_CLOSURE(r2, r1), patch).ignoreAssumption(this);
                opt.insert(new CBR(r2, lEnd, lInv), patch).ignoreAssumption(this);
                opt.insert(new LABEL(lInv), patch);
                opt.insert(new INV(this), patch);
                //opt.insert(new BR(lEnd), patch);
                opt.insert( new RET(0), patch );
                opt.insert(new LABEL(lEnd), patch);
            }*/
        }

        /** Assumption on global variable being a specific closure.
         */
        public static class IsConstClosure extends Variable {

            public final Closure value;

            public IsConstClosure(Symbol name, Closure value) {
                super(name);
                this.value = value;
            }

            @Override
            public boolean check(Frame frame) {
                return Frame.getVariable(name) == value;
            }

            @Override
            public String toString() {           
                return name.toString() + " is constant closure " + Frame.getKey(value);
            }

            @Override
            public boolean equals(Object other) {
                return other instanceof IsConstClosure && ((IsConstClosure) other).name == name && ((IsConstClosure) other).value == value;
            }
            /*
            @Override
            public void outputInvalidationCheck(Optimizer opt, Code.Patch patch) {
                Label lEnd = new Label();
                Label lInv = new Label();
                int r1 = opt.newRegister();
                int r2 = opt.newRegister();
                int r3 = opt.newRegister();
                opt.insert(new LOAD_VAR(r1, name), patch).ignoreAssumption(this);
                opt.insert(new LOAD_CONST(r2, value), patch);
                opt.insert(new EQ(r3, r1, r2), patch).ignoreAssumption(this);
                opt.insert(new CBR(r3, lEnd, lInv), patch).ignoreAssumption(this);
                opt.insert(new LABEL(lInv), patch);
                opt.insert(new INV(this), patch);
                //opt.insert(new BR(lEnd), patch);
                opt.insert( new RET(0), patch );
                opt.insert(new LABEL(lEnd), patch);
            }*/
        }
    }
}