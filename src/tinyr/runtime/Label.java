package tinyr.runtime;

/** Label object.
 *
 * The label object is only a marker object and does not have any useful properties attached to itself. It is an
 * argument to the LABEL and branch instructions.
 *
 * For debugging purposes, each label can be assigned an index, these indices are by no means consecutive, but they
 * can make the code more readable.
 *
 * See the Code.View class for more information on how the labels and basic blocks are constructed and used.
 */
public class Label {
}
