package tinyr.runtime;

/**
 */
public abstract class Value {
    public static enum Type {
        primitive,
        closure,
        array,
    }

    public abstract Type type();
}
