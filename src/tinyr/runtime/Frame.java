package tinyr.runtime;

import tinyr.Random;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 */
public class Frame {

    /** Program counter */
    public int pc;

    /** Value returned from the function, set by the RET instruction. */
    public Value result;

    // because all variables are globals, we use static hashmap for them
    static HashMap<Symbol, Value> variables = new HashMap<>();

    Value[] registers;

    public final Closure closure;

    public Frame parent;

    public Frame(Closure closure) {
        this.registers = new Value[closure.body.numOfRegisters()];
        this.pc = 0;
        this.closure = closure;
    }

    /**
     * Returns key corresponding to the given value
     */
    static public Symbol getKey(Value value) {
        Iterator<HashMap.Entry<Symbol,Value>> iter = variables.entrySet().iterator();
        while (iter.hasNext()) {
            HashMap.Entry<Symbol,Value> entry = iter.next();
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    final public Value get(int index) {
        return registers[index];
    }

    final public void set(int index, Value value) {
        registers[index] = value;
    }

    static public Value getVariable(Symbol name) {
        return variables.get(name);
    }

    final public void setVariable(Symbol name, Value value) {
        variables.put(name, value);
    }

    /** Returns name of a random global variable.
     *
     * The variable is guaranteed to be defined, but otherwise is randomly selected.
     */
    static final public Symbol getRandomVariable() {
        int idx = Random.nextInt(variables.size());
        assert (variables.size()>0): "No global variable defined yet";
        return (Symbol) variables.keySet().toArray()[idx];
    }

    final public Set<Symbol> getKeySet() {
        return variables.keySet();
    }
    
    public static final int globalVariables() {
        return variables.size();
    }
}
