package tinyr.runtime;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collection;

/**
 * This is a dirty hack for the arrays. We have a global map of double arrays and we use
 * the existing double values as indices to the global array map.
 */
public class Array extends Value {

  /**
   * Creates new array object of the given length.
   */
  public Array( int length ) {
    contents = new double[length];
  }

  public double[] contents;

  @Override
  public Type type() {
    return Value.Type.array;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder( "[" );
    for ( double d : this.contents ) {
      sb.append( new DecimalFormat( "#0.00" ).format( d ) );
      sb.append( " " );
    }
    sb.append( "]" );
    return sb.toString();
  }

  public String toCharString() {
    StringBuilder sb = new StringBuilder();
    for ( double d : this.contents )
      sb.append( Character.toChars( (int) d ) );
    return sb.toString();
  }

  public static Array fromString( String name ) {
    Array result = new Array( name.length() );
    for ( int i = 0; i < result.contents.length; ++i )
      result.contents[i] = name.charAt( i );
    return result;
  }

  public static Array fromCollection( Collection<Object> from ) {
    Array result = new Array( from.size() );
    int i = 0;
    for ( Object item : from )
      result.contents[i++] = (double) item;
    return result;
  }

}
