package tinyr.runtime;

public class Touple {

    /**
     * PC of function, where Assumption should be originally inserted
     */
    private int pc;

    /**
     * Hold value for Assumption if specific, null if general
     */
    private Value value;

    /**
     * assumption type
     */
    private Closure.AsmTypes asmType;

    public Touple(int pc, Value value, Closure.AsmTypes asmType) {
        this.asmType = asmType;
        this.pc = pc;
        this.value = value;
    }

    public int getPc() {
        return pc;
    }

    public Value getValue() {
        return value;
    }

    public Closure.AsmTypes getAsmType() {
        return asmType;
    }

    @Override
    public String toString() {
        return asmType + " " + value.toString() + " PC : " + pc;
    }
}