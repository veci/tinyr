package tinyr;

import java.io.*;

import tinyr.Lowering;
import tinyr.Parser;
import tinyr.ast.Node;
import tinyr.bytecode.*;
import tinyr.runtime.*;

import java.util.*;

public class Main {

    static Parser p = new Parser();
    static Lowering l = new Lowering();

    static void loadLibrary(String filename) {
        System.out.println("  - loading " + filename);
        File file = new File(filename);
        FileInputStream fis = null;
        Scanner in = null;
        StringBuffer library = new StringBuffer();
        try {
            fis = new FileInputStream(file);
            in = new Scanner(fis);
            while (in.hasNext()){
                library.append(in.nextLine() + "\n");
            }
            // dispose all the resources after using them.
            in.close();
            Code b = l.lower(p.parse(library.toString()));
            Code.View be = b.getView();
            be.execute(new Frame(new Closure(be)));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                in.close();
        }
    }
    
    public static boolean firstTimeExecuted = true;

    public static void main(String[] args) {
        System.out.println("tinyThing -- a supersimple interpreter to see code movements");
        Scanner in = new Scanner(System.in);
        String command = "";
        // Preloads the core functions
        loadLibrary("core.tr");
        loadLibrary("global.tr");

        loadLibrary("shootouts/binarytrees/binary2.tr");
        loadLibrary("shootouts/fannkuch-redux/fannkuch2.tr");
        loadLibrary("shootouts/fasta/fasta2.tr");
        loadLibrary("shootouts/k-nucleotide/nucleo2.tr");
        loadLibrary("shootouts/mandelbrot/mandelbrot2.tr");
        loadLibrary("shootouts/pidigits/pid2.tr");
        loadLibrary("shootouts/regexdna/regexdna.tr");
        loadLibrary("shootouts/reversecomplement/reverse2.tr");
        loadLibrary("shootouts/spectralnorm/spectralnorm2.tr");
        //loadLibrary("shootouts/nbody/nbody2.tr");

//      loadLibrary("shootouts/binarytrees/binarytrees.tr");
//      loadLibrary("shootouts/fannkuch-redux/fannkuchredux.tr");
//      loadLibrary("shootouts/fasta/fasta.tr");
//      loadLibrary("shootouts/k-nucleotide/nucleotide.tr");
//      loadLibrary("shootouts/mandelbrot/mandelbrot.tr");
//      loadLibrary("shootouts/nbody/nbody-naive.tr");
//      loadLibrary("shootouts/pidigits/pidigits.tr");
//      loadLibrary("shootouts/regexdna/regexdna.tr");
//      loadLibrary("shootouts/reversecomplement/reversecomplement.tr");
//      loadLibrary("shootouts/spectralnorm/spectralnorm-naive.tr");
        while (true) {
            System.out.print(command.isEmpty() ? "> " : "+ ");
            command += "\n" + in.nextLine();
            command = command.trim();
            if (command.equals("quit")) {
                break;
            }
             // ">" and the user has pressed just [spaces and] Enter
            if (command.isEmpty()) {
                command = "";
                continue;
            }
            try {
                Node ast = p.parse(command);
                command = "";
                try {
                    Code b = l.lower(ast);
                    Code.View be = b.getView();
                    Value v = be.execute(new Frame(new Closure(be)));
                    System.out.println(v);
                    if (firstTimeExecuted) {
                        Closure.generateAssumptions();
                        //Closure.createInputConfiguration();
                        //also after each part of execution remove START/END instructions
                        Closure.closeFilesForFunctions();
                        System.out.println("SPARED OPTIMIZATIONS " + Closure.sparedOpt);
                        System.out.println("DONE OPT " + Closure.doneOpt);
                        firstTimeExecuted = false;
                    }
                    System.out.println("DONE EXECUTION - - - - -");
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                    e.printStackTrace();
                }
            } catch (RuntimeException e) {
                if (!p.eof()) {
                    System.err.println(e.getMessage());
                    e.printStackTrace();
                } else {
                    continue;
                }
            }
            command = "";
        }
    }
}
