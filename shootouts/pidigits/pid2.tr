DIGIT_SIZE = 1000

// Prefixes the bigNumber specified with the given digit.
prefixDigit = function(bigNumber, digit) {
    register l
    l = length(bigNumber)
    register result
    result = rep(0, add(l,1))
	result = array_set(result, 1, array_get(bigNumber,1) )
	result = array_set(result, 2, digit)
    register i
    i = 2
    while (lte(i, l)) {
        result = array_set(result, add(i,1), array_get(bigNumber,i) )
        i = add(i,1)
    }
    result
}

increaseDigitSize = function(bigNumber, digits) {
    register result
    register l
    l = length(bigNumber)
    result = rep(0, add(digits,1) )
    result = array_set(result, 1, array_get(bigNumber,1) )
    register i
    register x
    i = 2
    x = sub(digits, sub(l,1)) // first position in the new number
    while (lte(i, l)) {
        result = array_set(result, add(x,i), array_get(bigNumber,i) )
        i = add(i,1)
    }
    result
}

shorten = function(bigNumber) {
    if (eq(length(bigNumber), 2)) {
        bigNumber
    } else if (eq(array_get(bigNumber, 2), 0)) {
        register result
        register l
        l = length(bigNumber)
        l = sub(l, 1)
        result = rep(0, l)
        result = array_set(result, 1, array_get(bigNumber, 1) )
        register i
        i = 2
        while (lte(i, l)) {
            result = array_set(result, i , array_get(bigNumber, add(i,1)))
            i = add(i,1)
        }
        shorten(result)
    } else {
        bigNumber
    }
}

// Returns the sign of the big number in argument.
sign = function(a) {
    array_get(a, 1)
}

// big integers are represented as an array of digits in DIGIT_SIZE base. This constructor is capable of creating a bigint
// number of up to two digits
// array
bigint = function(from) {
    register sign
    sign = 1
    if (lt(from, 0)) {
        sign = -1
        from = -from
    }
    if (gte(from, DIGIT_SIZE)) {
        c(sign, iDiv(from, DIGIT_SIZE), mod(from, DIGIT_SIZE))
    } else {
        c(sign, from)
    }
}

// Converts the given big integer to its absolute value, also a big integer
bigabs = function(bi) {
    array_set(bi, 1, 1)
}

// Returns the inversion of the given big integer. This is a number of the same magnitude, but different sign than the
// original
bigInv = function(bi) {
    array_set(bi, 1, mul(array_get(bi, 1),-1) )
}

// Compares two positive bigints and return 1 if the first one is smaller than the second
biglt = function(a, b) {
    if (lt(sign(a), sign(b))) {
        RET 1
    } else if (gt(sign(a), sign(b))) {
        RET 0
    } else if (lt(length(a), length(b))) {
        RET 1
    } else if (gt(length(a), length(b))) {
        RET 0
    } else {
        register i
        i = 2
        while (lte(i, length(a))) {
            if ( lt(array_get(a, i), array_get(b, i))) {
                RET 1
            } else if (gt(array_get(a, i), array_get(b, i))) {
                RET 0
            }
            i = add(i,1)
        }
        RET 0
    }
}

biggt = function(a, b) {
    if (gt(sign(a), sign(b))) {
        RET 1
    } else if (lt(sign(a), sign(b))) {
        RET 0
    } else if (gt(length(a), length(b))) {
        RET 1
    } else if (lt(length(a), length(b))) {
        RET 0
    } else {
        register i
        i = 2
        while (lte(i, length(a))) {
            if (gt(array_get(a, i), array_get(b, i))) {
                RET 1
            } else if (lt(array_get(a, i), array_get(b, i))) {
                RET 0
            }
            i = add(i,1)
        }
        RET 0
    }
}

bigge = function(a, b) {
    if (gt(sign(a), sign(b))) {
        RET 1
    } else if (lt(sign(a), sign(b))) {
        RET 0
    } else if (gt(length(a), length(b))) {
        RET 1
    } else if (lt(length(a), length(b))) {
        RET 0
    } else {
        register i
        i = 2
        while (lte(i, length(a))) {
            if (gt(array_get(a, i), array_get(b, i))) {
                RET 1
            } else if (lt(array_get(a, i), array_get(b, i))) {
                RET 0
            }
            i = add(i,1)
        }
        RET 1
    }
}
// Adds two positive big integers. This just uses the most trivial way - both numbers are bumped to the
bigadd_ = function(a, b) {
    register rl
    rl = max(length(a), length(b))
    a = increaseDigitSize(a, rl)
    b = increaseDigitSize(b, rl)
    rl = add(rl,1)
    register result
    result = rep(0, rl)
    result = array_set(result, 1, 1)
    register i
    register carry
    carry = 0
    i = rl
    while (gt(i, 1)) {
        register t
        t = add(add(array_get(a, i), array_get(b, i)), carry)
        carry = iDiv(t, DIGIT_SIZE)
        t = mod(t, DIGIT_SIZE)
        result = array_set(result, i, t)
        i = sub(i, 1)
    }
    shorten(result)
}

// a - b for both a and b being positive numbers
bigsub_ = function(a, b) {
    if (biglt(a, b)) {
        RET bigInv(bigsub_(b, a))
    }
    register rl
    rl = max(length(a), length(b))
    a = increaseDigitSize(a, rl)
    b = increaseDigitSize(b, rl)
    rl = add(rl, 1)
    register result
    result = rep(0, rl)
    result = array_set(result, 1, 1)
    register i
    register carry
    carry = 0
    i = rl
    while (gt(i, 1)) {
        register ta
        register tb
        ta = array_get(a, i)
        tb = add(array_get(b, i), carry)
        if (lt(ta, tb)){
            carry = 1
            ta = sub(add(ta, DIGIT_SIZE), tb)
        } else {
            carry = 0
            ta = sub(ta, tb)
        }
        result = array_set(result, i , ta)
        i = sub(i, 1)
    }
    shorten(result)
}

// Multiplication of a big number with normal number that is at most as big as DIGIT_SIZE. The shift denotes if the
// result should be shifted left by the number of digits specified
bigmul_single_ = function(a, b, shift) {
    register rl
    rl = length(a)    
    register result
    result = rep(0, add(add(rl,shift),1) )
    result = array_set(result, 1, 1)
    register i
    i = rl
    register carry
    carry = 0
    while (gt(i, 1)) {
        register t
        t = array_get(a, i)
        t = add(mul(t, b), carry)
        carry = iDiv(t, DIGIT_SIZE)
        t = mod(t, DIGIT_SIZE)
        result = array_set(result, add(i,1), t)
        i = sub(i, 1)
    }
    result = array_set(result, 2, carry)
    result // don't bother shortening
}

// Multiplication of two big integers
bigmul_ = function(a, b) {
    register result
    result = bigint(0)
    register bl
    bl = length(b)
    register shift
    shift = 0
    while (gt(bl, 1)) {
        result = bigadd_(result, bigmul_single_(a, array_get(b, bl), shift))
        bl = sub(bl, 1)
        shift = add(shift, 1)
    }
    shorten(result)
}

bigadd = function(a, b) {
    register sa
    register sb
    sa = sign(a)
    sb = sign(b)
    if (eq(sa, 1)) {
        if (eq(sb, 1)) {
            // a + b
            bigadd_(a, b)
        } else {
            // a + - b == a - b
            bigsub_(a, bigabs(b))
        }
    } else {
        if (eq(sb, 1)) {
            // -a + b == -( a - b)
            bigInv(bigsub_(bigabs(a), b))
        } else {
            // -a + - b = - (a + b)
            bigInv(bigadd_(bigabs(a), bigabs(b)))
        }
    }
}

bigsub = function(a, b) {
    register sa
    register sb
    sa = sign(a)
    sb = sign(b)
    if (eq(sa, 1)) {
        if (eq(sb, 1)) {
            // a - b
            bigsub_(a, b)
        } else {
            // a - - b == a + b
            bigadd_(a, bigabs(b))
        }
    } else {
        if (eq(sb, 1)) {
            // -a - b == -(a + b)
            bigInv(bigadd_(bigabs(a), b))
        } else {
            // -a - -b == -(a - b)
            bigInv(bigsub_(bigabs(a), bigabs(b)))
        }
    }
}

bigmul = function(a, b) {
    register sa
    register sb
    sa = sign(a)
    sb = sign(b)
    if (eq(sa, 1)) {
        if (eq(sb, 1)) {
            // a * b
            bigmul_(a, b)
        } else {
            // a * - b == a + b
            biginv(bigmul_(a, bigabs(b)))
        }
    } else {
        if (eq(sb, 1)) {
            // -a * b == -(a * b)
            bigInv(bigmul_(bigabs(a), b))
        } else {
            // -a * -b == -(a - b)
            bigmul_(bigabs(a), bigabs(b))
        }
    }
}

// this is very stupid now and uses the simple subtraction algorithm, it is slow, but we can always implement better
// version of this in the future
bigdiv = function(a, b) {
    register result
    result = bigint(0)
    while (bigge(a, b)) {
        result = bigadd(result, bigint(1))
        a = bigsub(a, b)
    }
    result
}

pidigits = function(size) {
    N = size
    register ONE
    register TWO
    register TEN
    register THREE
    ONE = bigint(1)
    TWO = bigint(2)
    TEN = bigint(10)
    THREE = bigint(3)
    register i
    register k
    register ns
    i = 0
    k = 0
    register k1
    k1 = 1
    register a
    register t
    register u
    a = bigint(0)
    t = bigint(0)
    u = bigint(0)
    register n
    register d
    n = bigint(1)
    d = bigint(1)
    while (1) {
        k = add(k, 1)
        t = bigmul(n, TWO)
        n = bigmul(n, bigint(k))
        a = bigadd(a, t)
        k1 = add(k1, 2)
        register k1_big
        k1_big = bigint(k1)
        a = bigmul(a, k1_big)
        d = bigmul(d, k1_big)
        if (bigge(a, n)) {
            register n3a
            n3a = bigadd(bigmul(n, THREE), a)
            t = bigdiv(n3a, d)
            register td
            td = bigmul(t, d)
            u = bigadd(bigsub(n3a, td), n)
            if (biggt(d, u)) {
                print(t)
                i = add(i, 1)
                if (gte(i, N))
                    break
                a = bigsub(a, td)
                a = bigmul(a, TEN)
                n = bigmul(n, TEN)
            }
        }
    }
}
